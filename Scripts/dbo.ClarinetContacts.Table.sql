USE [Clarinet_Dev]
GO
/****** Object:  Table [dbo].[ClarinetContacts]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClarinetContacts](
	[PrescientPartyRef] [int] NULL,
	[LastSync] [datetime] NULL
) ON [PRIMARY]

GO
