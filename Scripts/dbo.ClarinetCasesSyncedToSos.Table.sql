USE [Clarinet_Dev]
GO
/****** Object:  Table [dbo].[ClarinetCasesSyncedToSos]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClarinetCasesSyncedToSos](
	[PrescientCaseRef] [varchar](50) NULL,
	[CrmCaseId] [uniqueidentifier] NULL,
	[SosSyncTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
