USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcFetchCasesToProcess]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[prcFetchCasesToProcess]
as
begin

	-- TODO - expand to include previously synced cases that need re-sync
	select top 100 RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) as case_ref
	from synonymPrescientArchive a
	where RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) not in (select PrescientCaseRef from ClarinetCases)
	and RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) not in (select PrescientCaseRef from ClarinetCaseNotFoundInCrm)
	-- for testing
	and RTrim(a.client_ref) < 'A264780'
	-- exclude any failed rows from last 24 hours to avoid re-processing errors too soon
	and RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) not in (select SourceID from ClarinetHistory
							where EntityType = 'Case' and Succeeded = 0
							and [TimeStamp] > DateAdd(day, -1, GetDate()))
	order by RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) desc

end

GO
