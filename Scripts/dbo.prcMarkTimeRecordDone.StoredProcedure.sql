USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcMarkTimeRecordDone]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[prcMarkTimeRecordDone](
@PrescientCaseRef varchar(50),
@PrescientTimeKey int,
@CrmTimeID uniqueidentifier)
as
begin

	if (exists(select * from ClarinetTimeRecords where PrescientTimeKey = @PrescientTimeKey))
	begin
		update ClarinetTimeRecords set CrmTimeID = @CrmTimeID, LastSync = getdate() where PrescientTimeKey = @PrescientTimeKey
	end
	else
	begin
		insert into ClarinetTimeRecords(PrescientCaseRef, PrescientTimeKey, CrmTimeID, LastSync) 
		values (@PrescientCaseRef, @PrescientTimeKey, @CrmTimeID, getdate())
	end

end

GO
