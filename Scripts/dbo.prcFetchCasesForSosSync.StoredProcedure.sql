USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcFetchCasesForSosSync]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[prcFetchCasesForSosSync]
as
begin

	-- cases that have been transferred by Clarinet but haven't been synced to SOS already
	select top 100 RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) as case_ref, c.CrmCaseId
	from synonymPrescientArchive a
	inner join ClarinetCases c 
	on (RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10))) = c.PrescientCaseRef
	where 
	c.CrmCaseId not in (select CrmCaseId from ClarinetCasesSyncedToSos)
	-- exclude any failed rows from last 24 hours to avoid re-processing errors too soon
	and c.CrmCaseId not in (select SourceID from ClarinetHistory
							where EntityType = 'SosSync' and Succeeded = 0
							and [TimeStamp] > DateAdd(day, -1, GetDate()))
	
	order by RTrim(a.client_ref) + '.' + Cast(a.matter_suffix as varchar(10)) desc

end

GO
