USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcMarkSosSyncDone]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[prcMarkSosSyncDone](
@CaseRef varchar(50),
@CrmCaseId uniqueidentifier)
as
begin

	if (exists(select * from  ClarinetCasesSyncedToSos where PrescientCaseRef = @CaseRef))
	begin
		update ClarinetCasesSyncedToSos 
		set SosSyncTime = getdate(),
		CrmCaseId = @CrmCaseId
		where PrescientCaseRef = @CaseRef
	end
	else
	begin
		insert into ClarinetCasesSyncedToSos(PrescientCaseRef,  SosSyncTime, CrmCaseId) values (@CaseRef, getdate(), @CrmCaseId)
	end

end

GO
