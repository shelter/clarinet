USE [Clarinet_Dev]
GO
/****** Object:  Table [dbo].[ClarinetHistory]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClarinetHistory](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Direction] [varchar](10) NULL,
	[EntityType] [varchar](50) NULL,
	[OpType] [varchar](10) NULL,
	[SourceId] [varchar](50) NULL,
	[SyncId] [int] NULL,
	[TargetId] [varchar](50) NULL,
	[Succeeded] [int] NULL,
	[Message] [varchar](max) NULL,
	[MessageCount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
