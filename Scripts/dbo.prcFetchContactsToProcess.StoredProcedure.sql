USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcFetchContactsToProcess]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[prcFetchContactsToProcess]
as
begin

	-- TODO - expand to include previously synced contacts that need re-sync
	select top 100 p.party_ref 
	from synonymPrescientParty p
	where p.party_ref not in (select PrescientPartyRef from ClarinetContacts)
	and p.party_ref not in (select PrescientPartyRef from ClarinetContactNotFoundInCrm)
	-- exclude any failed rows from last 24 hours to avoid re-processing errors too soon
	and p.party_ref not in (select SourceID from ClarinetHistory
							where EntityType = 'Contact' and Succeeded = 0
							and [TimeStamp] > DateAdd(day, -1, GetDate()))
	-- this for testing
	and p.party_ref <= 679921
	order by p.party_ref desc

end

GO
