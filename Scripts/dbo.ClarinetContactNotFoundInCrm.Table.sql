USE [Clarinet_Dev]
GO
/****** Object:  Table [dbo].[ClarinetContactNotFoundInCrm]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClarinetContactNotFoundInCrm](
	[PrescientPartyRef] [int] NULL,
	[TimeStamp] [datetime] NOT NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
