USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcMarkContactDone]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[prcMarkContactDone](
@PartyRef int)
as
begin

	if (exists(select * from ClarinetContacts where PrescientPartyRef = @PartyRef))
	begin
		update ClarinetContacts set LastSync = getdate() where PrescientPartyRef = @PartyRef
	end
	else
	begin
		insert into ClarinetContacts(PrescientPartyRef, LastSync) values (@PartyRef, getdate())
	end

end
GO
