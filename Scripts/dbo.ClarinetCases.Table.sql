USE [Clarinet_Dev]
GO
/****** Object:  Table [dbo].[ClarinetCases]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClarinetCases](
	[PrescientCaseRef] [varchar](50) NULL,
	[LastSync] [datetime] NULL,
	[CrmCaseId] [uniqueidentifier] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
