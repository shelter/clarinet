USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcMarkCaseDone]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[prcMarkCaseDone](
@CaseRef varchar(50),
@CrmCaseId uniqueidentifier)
as
begin

	if (exists(select * from ClarinetCases where PrescientCaseRef = @CaseRef))
	begin
		update ClarinetCases 
		set LastSync = getdate(),
		CrmCaseID = @CrmCaseId
		where PrescientCaseRef = @CaseRef
	end
	else
	begin
		insert into ClarinetCases(PrescientCaseRef, LastSync, CrmCaseId) values (@CaseRef, getdate(), @CrmCaseId)
	end

end

GO
