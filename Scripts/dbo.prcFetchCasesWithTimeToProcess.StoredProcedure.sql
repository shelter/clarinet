USE [Clarinet_Dev]
GO
/****** Object:  StoredProcedure [dbo].[prcFetchCasesWithTimeToProcess]    Script Date: 24/01/2018 20:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[prcFetchCasesWithTimeToProcess]
as
begin

    -- first fetch prescient time records into temp table
	-- this is faster than attempting distributed query
	select tstran_key, client_ref, matter_suffix, RTrim(client_ref) + '.' + Cast(matter_suffix as varchar(10)) as prescient_caseref
	into #tmpTimeRecords
	from synonymPrescientTimeRecord

	create index idx_temp_tstran_key on #tmpTimeRecords(tstran_key)
	create index idx_prescient_caseref on #tmpTimeRecords(prescient_caseref)

	-- in test db only A168909.3 returns anything
	select distinct top 20 c.PrescientCaseRef
	from ClarinetCases c
	inner join #tmpTimeRecords t
	on t.prescient_caseref = c.PrescientCaseRef
	left outer join ClarinetTimeRecords thist
	on thist.PrescientTimeKey = t.tstran_key
	where thist.PrescientTimeKey is null
	order by c.PrescientCaseRef desc
end

GO
