﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;


namespace ClarinetTests.TestsWithRealData
{
    [TestClass]
    public class CrmPicklistTasksTests
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [TestMethod]
        public void CanGetConnectionRoles()
        {
            var picktask = new CrmPicklistTasks();

            var roles = picktask.GetConnectionRoles();

            foreach (CrmPicklistTasks.CrmConnectionRole roleLoop in roles)
            {
                logger.DebugFormat("Connection Role ID {0} Name {1} Category {2}",
                    roleLoop.ConnectionRoleId.ToString(), roleLoop.Name, roleLoop.Category);
            }

            Assert.IsTrue(roles.Count > 10);





        }

        [TestMethod]
        public void CanGetContracts()
        {
            var picktasks = new CrmPicklistTasks();

            var cts = picktasks.GetContracts();

            foreach (CrmPicklistTasks.CrmEntityForPicklist ctLoop in cts)
            {
                logger.DebugFormat("Contact ID {0} Name {1} Code {2}",
                    ctLoop.CrmId.ToString(), ctLoop.Name, ctLoop.Code);
            }

            Assert.IsTrue(cts.Count > 10);


        }

        [TestMethod]
        public void CanGetContractsAndSearch()
        {
            var picktasks = new CrmPicklistTasks();

            var cts = picktasks.GetContracts();

            var matchContract = cts.FirstOrDefault(c => c.Code == "2L965U");

            Assert.IsNotNull(matchContract);
        }

        [TestMethod]
        public void CanGetCentres()
        {
            var picktasks = new CrmPicklistTasks();

            var cts = picktasks.GetCentres();

            foreach (CrmPicklistTasks.CrmEntityForPicklist ctLoop in cts)
            {
                logger.DebugFormat("Centre ID {0} Name {1} Code {2}",
                    ctLoop.CrmId.ToString(), ctLoop.Name, ctLoop.Code);
            }

            Assert.IsTrue(cts.Count > 10);


        }

        [TestMethod]
        public void CanGetActivities()
        {
            var picktasks = new CrmPicklistTasks();

            var cts = picktasks.GetActivities();

            foreach (CrmPicklistTasks.CrmEntityForPicklist ctLoop in cts)
            {
                logger.DebugFormat("Activity ID {0} Name {1} Code {2}",
                    ctLoop.CrmId.ToString(), ctLoop.Name, ctLoop.Code);
            }

            Assert.IsTrue(cts.Count > 10);


        }

        [TestMethod]
        public void CanGetCaseTypes()
        {
            var picktasks = new CrmPicklistTasks();

            var cts = picktasks.GetCaseTypes();

            foreach (CrmPicklistTasks.CrmCaseType ctLoop in cts)
            {
                logger.DebugFormat("Case Type ID {0} Name {1} Code {2}",
                    ctLoop.CrmId.ToString(), ctLoop.Name, ctLoop.Code);
            }

            Assert.IsTrue(cts.Count > 10);





        }

        [TestMethod]
        public void CanGetTitleOptionSet()
        {
            var picktasks = new CrmPicklistTasks();

            var titles = picktasks.GetTitleOptionSet();

            foreach (int? key in titles.Keys)
            {
                logger.DebugFormat("Title key  {0} Label {1}",
                    key, titles[key]);
            }

            Assert.IsTrue(titles.Count > 10);


        }

        [TestMethod]
        public void CanGetCaseStatusReasonOptionSet()
        {
            var picktasks = new CrmPicklistTasks();

            var titles = picktasks.GetCaseStatusReasonOptionSet();

            foreach (int? key in titles.Keys)
            {
                logger.DebugFormat("CaseStatusReason key  {0} Label {1}",
                    key, titles[key]);
            }

            Assert.IsTrue(titles.Count > 2);


        }

        [TestMethod]
        public void CanGetCaseworkers()
        {
            var picktasks = new CrmPicklistTasks();

            var cws = picktasks.GetCaseworkers();

            foreach (var cwLoop in cws)
            {
                logger.DebugFormat("Caseworker ID {0} Name {1} PrescientIntialsID {2}",
                    cwLoop.CaseWorkerId, cwLoop.CaseWorkerName, cwLoop.PrescientInitialsId);

            }

            Assert.IsTrue(cws.Count > 100);

        }

        [TestMethod]
        public void CanGetConnectionRolePair_MainClient()
        {
            var picktasks = new CrmPicklistTasks();
            var ret = picktasks.GetCaseContactConnectionRolePair("PRICLI");

            Guid householdCaseRole = new Guid("fcfc036d-2b8b-e211-889a-3c4a92dbd83d");

            Assert.AreEqual(new Guid("06e32c7d-df80-e211-b5c5-d4856451cc85"), ret.ContactConnectionRole.Value);
            Assert.AreEqual(householdCaseRole, ret.CaseConnectionRole.Value);

        }

        [TestMethod]
        public void CanGetConnectionRolePair_HouseholdSon()
        {
            var picktasks = new CrmPicklistTasks();
            var ret = picktasks.GetCaseContactConnectionRolePair("REL36");

            Guid householdCaseRole = new Guid("fcfc036d-2b8b-e211-889a-3c4a92dbd83d");

            Assert.AreEqual(new Guid("8ffcd36b-e080-e211-b5c5-d4856451cc85"), ret.ContactConnectionRole.Value);
            Assert.AreEqual(householdCaseRole, ret.CaseConnectionRole.Value);

        }

        [TestMethod]
        public void CanGetConnectionRolePair_CaseLink()
        {
            var picktasks = new CrmPicklistTasks();
            var ret = picktasks.GetCaseContactConnectionRolePair("MIGCL");

            Guid caselinkRole = new Guid("6a028add-f480-e211-b5c5-d4856451cc85");

            Assert.AreEqual(caselinkRole, ret.ContactConnectionRole.Value);
            Assert.AreEqual(caselinkRole, ret.CaseConnectionRole.Value);

        }

        [TestMethod]
        public void CanGetConnectionRolePair_UnknownRole()
        {
            var picktasks = new CrmPicklistTasks();
            var ret = picktasks.GetCaseContactConnectionRolePair("madeup");



            Assert.IsFalse(ret.ContactConnectionRole.HasValue);
            Assert.IsFalse(ret.CaseConnectionRole.HasValue);

        }

        [TestMethod]
        public void CanGetEligibilityCodes()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetEligibilityCodes();

            foreach (var cLoop in codes)
            {
                logger.DebugFormat("EligibilityCode ID {0} Desc {1} Label {2}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 4);
        }

        [TestMethod]
        public void CanGetMediaCodes()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetMediaCodes();

            logger.DebugFormat("Media Codes:");
            foreach (var cLoop in codes)
            {
                logger.DebugFormat("{1}, {2}, {0}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 10);
        }

        [TestMethod]
        public void CanGetServiceAdaptations()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetServiceAdaptations();

            logger.DebugFormat("Service Adaptations:");
            foreach (var cLoop in codes)
            {
                logger.DebugFormat("{1}, {2}, {0}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 10);
        }

        [TestMethod]
        public void CanGetReferralCodes()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetReferralCodes();

            logger.DebugFormat("Referral Codes:");
            foreach (var cLoop in codes)
            {
                logger.DebugFormat("{1}, {2}, {0}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 3);
        }

        [TestMethod]
        public void CanGetHouseholdTypes()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetHouseholdTypes();

            logger.DebugFormat("Household Types:");
            foreach (var cLoop in codes)
            {
                logger.DebugFormat("{1}, {2}, {0}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 3);
        }


        [TestMethod]
        public void CanGetCLACentre()
        {
            var picktasks = new CrmPicklistTasks();
            string centreToUse = "Civil Legal Advice (Shelter)";

            var matchCentre = picktasks.GetCentres().FirstOrDefault(c => c.Name == centreToUse);

            Assert.IsNotNull(matchCentre);
            Assert.AreEqual("Civil Legal Advice (Shelter)", matchCentre.Name);

         }


        [TestMethod]
        public void CanGetDeterminationCodes()
        {
            var picktasks = new CrmPicklistTasks();
            var codes = picktasks.GetDeterminationCodes();

            foreach (var cLoop in codes)
            {
                logger.DebugFormat("DeterminationCode ID {0} Desc {1} Label {2}",
                    cLoop.CrmOptionSetValue, cLoop.Description, cLoop.Label);

            }

            Assert.IsTrue(codes.Count > 3);
        }
    }
}
