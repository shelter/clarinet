﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClarinetLib;
using ClarinetLib.SosInterface;

namespace RigatoninetTests.TestsWithRealData
{
    [TestClass]
    public class SosTests
    {
        

        [TestMethod]
        public void CanTransferCrmCaseToSos_AlreadyExists()
        {
            var tasks = new SosTasks();
            
            Guid caseID = new Guid("B73A4141-A6BE-E411-9FA7-D89D6763EF70");
            var resp = tasks.TransferCrmCaseToSos(caseID);

            
            Assert.AreEqual(true, resp.ContactAlreadyExistsInSos);
            Assert.AreEqual(true, resp.CaseAlreadyExistsInSos);
            Assert.AreEqual(true, resp.Success);
            Assert.AreEqual("foo", resp.ErrorMessage);
            Assert.AreEqual(true, resp.Success);
            
        }

        [TestMethod]
        public void CanTransferCrmCaseToSos()
        {
            var tasks = new SosTasks();

            Guid caseID = new Guid("CFE7D80F-1D9B-E211-8C09-984BE16D9E77"); // 168909.3
            var resp = tasks.TransferCrmCaseToSos(caseID);


           
            Assert.AreEqual(null, resp.ErrorMessage);
            Assert.AreEqual(false, resp.CaseAlreadyExistsInSos, "case should not already exist in SOS for this test");
            Assert.AreEqual(true, resp.Success);

        }

        [TestMethod]
        public void CanClearContactSosIdInCrm()
        {
            var tasks = new SosTasks();

            Guid contactId = new Guid("87138239-528F-E211-8ABD-78E3B517ECB7"); // 168909
            tasks.ClearContactSosIdInCrm(contactId);
        }

        [TestMethod]
        public void CanClearCaseSosIdInCrm()
        {
            var tasks = new SosTasks();

            Guid caseID = new Guid("CFE7D80F-1D9B-E211-8C09-984BE16D9E77"); // 168909.3
            tasks.ClearCaseSosIdInCrm(caseID);
        }

        [TestMethod]
        public void CanUpdateCrmContactwithSosID()
        {
            var tasks = new CrmTasksForSos();

            Guid contactID = new Guid("87138239-528F-E211-8ABD-78E3B517ECB7");
            String sosID = "168909";

            tasks.UpdateContactSosID(contactID, sosID);
         }
    }
}
