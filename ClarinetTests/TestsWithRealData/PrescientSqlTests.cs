﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClarinetLib;
using ClarinetLib.LocalModel;

namespace RigatoninetTests.TestsWithRealData
{
    [TestClass]
    public class PrescientSqlTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

       

        [TestMethod]
        public void CanFetchContact()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = new Referrals();
            tasks.FillContactDataTable(ds, new List<int>() { 683996 });




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.Contact.Count);
            Assert.AreEqual("Dear Ms Jaramillo", ds.Contact[0].Salutation);



        }

        [TestMethod]
        public void CanFetchContactIndividual()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = new Referrals();
            tasks.FillContactIndividualDataTable(ds, new List<int>() { 683996 });




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.ContactIndividual.Count);
            Assert.AreEqual("Jaramillo", ds.ContactIndividual[0].Surname);



        }

        [TestMethod]
        public void CanFetchContactOrganisation()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = new Referrals();

            tasks.FillContactOrganisationDataTable(ds, new List<int>() { 683328 });




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.ContactOrganisation.Count);
            Assert.AreEqual("Haig Housing", ds.ContactOrganisation[0].Name);



        }

        [TestMethod]
        public void CanFetchContactAddressLink()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = tasks.FillContactAddressLinkDataTable(683996, 523735);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.ContactAddressLink.Count);
            Assert.AreEqual("683996.523735", ds.ContactAddressLink[0].SourceContactAddressLinkID);



        }

        [TestMethod]
        public void CanFetchAddress()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = tasks.FillAddressDataTable( 523735);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.Address.Count);
            Assert.AreEqual("SE5 7AY",  ds.Address[0].Postcode.TrimEnd());



        }


        [TestMethod]
        public void CanFetchCase()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = new Referrals();
            List<string> caseRefList = new List<string>();
            caseRefList.Add("L011890.1");
            tasks.FillCaseDataTable(ds, caseRefList);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.Case.Count);
            Assert.AreEqual("RB20", ds.Case[0].OwnerCode);



        }

        [TestMethod]
        public void CanFetchTimeRecords()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = new Referrals();
            List<string> caseRefList = new List<string>();
            caseRefList.Add("L000606.1");
            tasks.FillTimeRecordDataTable(ds, caseRefList);




            Assert.IsNotNull(ds);
            Assert.AreEqual(5, ds.TimeRecord.Count);
            Assert.AreEqual("ML05", ds.TimeRecord[1].FeeEarnerCode);



        }

        [TestMethod]
        public void CanFetchCaseContactLink()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = tasks.FillCaseContactLinkDataTable("L011890", 1, 61694);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.CaseContactLink.Count);
            Assert.AreEqual("PRICLI", ds.CaseContactLink[0].RoleCode);



        }

        [TestMethod]
        public void CanFetchCasePresentingProblem()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = tasks.FillCasePresentingProblemDataTable("L011890", 1, 43449);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.CasePresentingProblem.Count);
            Assert.AreEqual("PP775", ds.CasePresentingProblem[0].Description.TrimEnd());



        }

        [TestMethod]
        public void CanFetchContactAlert()
        {
            var tasks = new PrescientDatabaseTasks();
            var ds = tasks.FillContactAlertDataTable(683466, 212);




            Assert.IsNotNull(ds);
            Assert.AreEqual(1, ds.ContactAlert.Count);
            Assert.AreEqual("GC06", ds.ContactAlert[0].RaisedBy);



        }

     

        [TestMethod]
        public void TestStage1_TransferContactBatch()
        {
            var tasks = new PrescientToCrmTransferTasks();
            tasks.Stage1_TransferContactBatch();

        }

        [TestMethod]
        public void TestStage2_TransferCaseBatch()
        {
            var tasks = new PrescientToCrmTransferTasks();
            tasks.Stage2_TransferCaseBatch();

        }

        [TestMethod]
        public void TestStage3_TransferTimeRecords()
        {
            var tasks = new PrescientToCrmTransferTasks();
            tasks.Stage3_TransferTimeBatch();

        }

        [TestMethod]
        public void CanFetchControlDataContacts()
        {
            var tasks = new ControlDataTasks();

            var res = tasks.FetchContactIdsToProcess();

            Assert.AreEqual(100, res.Count());
        }

        [TestMethod]
        public void CanWriteControlDataContactDone()
        {
            var tasks = new ControlDataTasks();

            tasks.WriteContactDone(33);

            
        }

        [TestMethod]
        public void CanFetchControlDataCases()
        {
            var tasks = new ControlDataTasks();

            var res = tasks.FetchCaseRefsToProcess();

            Assert.AreEqual(100, res.Count());
        }

        [TestMethod]
        public void CanWriteControlDataCaseDone()
        {
            var tasks = new ControlDataTasks();

            tasks.WriteCaseDone("K65436.5", new Guid("CE551D6B-3213-45EC-A4D5-18350BECCF29"));


        }
    }
}
