﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

using ClarinetLib.CrmConnection;


namespace ClarinetTests.TestsWithRealData
{
    [TestClass]
    public class CrmConnectionTests
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [TestMethod]
        public void CanMakeCrmConnection()
        {
            var fac = new CrmConnectionFactory();

            var serv = fac.MakeCrmConnection();

            // Now make an SDK call with the organization service proxy.
            // Display information about the logged on user.
            Guid userid = ((WhoAmIResponse)serv.Execute(
                new WhoAmIRequest())).UserId;
            SystemUser systemUser = serv.Retrieve("systemuser", userid,
                new ColumnSet(new string[] { "firstname", "lastname" })).ToEntity<SystemUser>();
            logger.DebugFormat("Logged on user is {0} {1}.",
                systemUser.FirstName, systemUser.LastName);

            

            Assert.IsNotNull(serv);
            //Assert.AreEqual("System", systemUser.FirstName);
            //Assert.AreEqual("England", systemUser.LastName);


        }

        [TestMethod]
        public void CanMakePoolConnection()
        {
            var pool = new CrmConnectionCache();

            var serv = pool.GetCrmConnectionCached();

            // Now make an SDK call with the organization service proxy.
            // Display information about the logged on user.
            Guid userid = ((WhoAmIResponse)serv.Execute(
                new WhoAmIRequest())).UserId;
            SystemUser systemUser = serv.Retrieve("systemuser", userid,
                new ColumnSet(new string[] { "firstname", "lastname" })).ToEntity<SystemUser>();
            Console.WriteLine("Logged on user is {0} {1}.",
                systemUser.FirstName, systemUser.LastName);


            Assert.IsNotNull(serv);
            Assert.AreEqual("System", systemUser.FirstName);
            Assert.AreEqual("England", systemUser.LastName);


        }
    }
}
