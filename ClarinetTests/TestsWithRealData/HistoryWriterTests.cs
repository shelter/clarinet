﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using ClarinetLib;

namespace ClarinetTests.TestsWithRealData
{
    /// <summary>
    /// Summary description for HistoryWriterTests
    /// </summary>
    [TestClass]
    public class HistoryWriterTests
    {

        private static Random _random = new Random();

        public HistoryWriterTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CanWriteMessage()
        {
            var write = new HistoryWriter();

            string randString = _random.Next(10000,99999).ToString();
            string message = "test message "+randString;
            write.WriteMessage(message, 100);

            DataTable checkTable = new DataTable();
            using (SqlConnection conn = new SqlConnection(GetHistoryWriterConnectionString()))
            {
                
                string sql = string.Format("select * from ClarinetHistory where message = '{0}' and [TimeStamp] > '{1}'",
                    message, DateTime.Now.AddSeconds(-10).ToString("dd/MMM/yyyy HH:mm:ss"));
                var adapt = new SqlDataAdapter(sql, conn);
                adapt.Fill(checkTable);
            }

            Assert.IsNotNull(checkTable);
            Assert.AreEqual(1, checkTable.Rows.Count);



        }

        [TestMethod]
        public void CanWriteRowHistory()
        {
            var write = new HistoryWriter();

            string randString = "TEST" + _random.Next(100000, 999999).ToString();

            write.WriteRowHistory(1, "TestEntity", "Insert", randString, 230, "454545", true, "test message");

            DataTable checkTable = new DataTable();
            using (SqlConnection conn = new SqlConnection(GetHistoryWriterConnectionString()))
            {

                string sql = string.Format("select * from ClarinetHistory where SourceId = '{0}' and [TimeStamp] > '{1}'",
                    randString, DateTime.Now.AddSeconds(-10).ToString("dd/MMM/yyyy HH:mm:ss"));
                var adapt = new SqlDataAdapter(sql, conn);
                adapt.Fill(checkTable);
            }

            Assert.IsNotNull(checkTable);
            Assert.AreEqual(1, checkTable.Rows.Count);


        }

        [TestMethod]
        public void CanWriteRowHistoryWithoutMessage()
        {
            var write = new HistoryWriter();

            string randString = "TEST" + _random.Next(100000, 999999).ToString();

            write.WriteRowHistory(1, "TestEntity", "Insert", randString, 230, "454545", true, null);

            DataTable checkTable = new DataTable();
            using (SqlConnection conn = new SqlConnection(GetHistoryWriterConnectionString()))
            {

                string sql = string.Format("select * from ClarinetHistory where SourceId = '{0}' and [TimeStamp] > '{1}'",
                    randString, DateTime.Now.AddSeconds(-10).ToString("dd/MMM/yyyy HH:mm:ss"));
                var adapt = new SqlDataAdapter(sql, conn);
                adapt.Fill(checkTable);
            }

            Assert.IsNotNull(checkTable);
            Assert.AreEqual(1, checkTable.Rows.Count);


        }

        private string GetHistoryWriterConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["HistoryWriter"].ConnectionString;


        }
    }
}
