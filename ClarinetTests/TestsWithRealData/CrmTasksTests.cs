﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

using ClarinetLib.LocalModel;


namespace ClarinetTests.TestsWithRealData
{
    [TestClass]
    public class CrmTasksTests
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        [TestMethod]
        public void CanSaveContact()
        {
            logger.DebugFormat("Running CanSaveContact test");
            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustContact();

            var conRow = ds.Contact[0];
            var conindRow = ds.ContactIndividual[0];

            var res = rig.SaveContact(conRow, conindRow);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

        [TestMethod]
        public void CanSaveContactWithMinimalInfo()
        {
            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustContactMinimalInfo();

            var conRow = ds.Contact[0];
            var conindRow = ds.ContactIndividual[0];

            var res = rig.SaveContact(conRow, conindRow);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);

        }

        [TestMethod]
        public void CanUpdateContact()
        {

            var rig = new CrmTasks();

            var ds = MakeReferralsDataSet();

            var conRow = ds.Contact[0];
            var conindRow = ds.ContactIndividual[0];

            var res = rig.SaveContact(conRow, conindRow);

            logger.DebugFormat("CanUpdateContact: first Save returned CrmID {0}", res.CrmId.ToString());

            // write the id back so we can do an update
            conRow.TargetContactID = res.CrmId.ToString();

            // change the forename
            conindRow.Forename = "Tony";

            var res2 = rig.SaveContact(conRow, conindRow);

            Assert.AreEqual(res.CrmId, res2.CrmId);
            Assert.AreEqual(false, res2.IsInsert);
            Assert.AreEqual(true, res2.IsUpdate);


        }


        [TestMethod]
        public void CanSaveContactAlert()
        {

            var rig = new CrmTasks();

            var ds = MakeReferralsDataSet();

            var conAlertRow = ds.ContactAlert[0];


            Guid targetContactId = TestUtils.GetExistingContactId(); 

            var res = rig.SaveContactAlert(conAlertRow, targetContactId);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

        [TestMethod]
        public void CanSaveAddress()
        {

            var rig = new CrmTasks();

            var ds = MakeReferralsDataSet();

            var addressRow = ds.Address[0];


            var res = rig.SaveAddress(addressRow);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);



        }


        [TestMethod]
        public void CanSaveOrganisation()
        {

            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustOrganisationWithAddress();

            var conRow = ds.Contact[0];
            var orgRow = ds.ContactOrganisation[0];


            var res = rig.SaveOrganisation(conRow, orgRow);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

        [TestMethod]
        public void CanSaveContactAddressLink()
        {

            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSet();

            var conRow = ds.Contact[0];
            var conindRow = ds.ContactIndividual[0];
            var addressRow = ds.Address[0];
            var linkRow = ds.ContactAddressLink[0];

            var res1 = rig.SaveContact(conRow, conindRow);
            logger.DebugFormat("Contact Save returned CrmID {0}", res1.CrmId.ToString());

            Guid targetContactId = res1.CrmId;

            var res2 = rig.SaveAddress(addressRow);
            logger.DebugFormat("Address returned CrmID {0}", res2.CrmId.ToString());
            Guid targetAddressId = res2.CrmId;

            var res3 = rig.SaveContactAddressLink(linkRow, targetContactId, targetAddressId);
            logger.DebugFormat("ContactAddressLink save returned CrmID {0}", res2.CrmId.ToString());

            Assert.AreEqual(targetContactId, res3.CrmId);
            Assert.AreEqual(false, res3.IsInsert);
            Assert.AreEqual(true, res3.IsUpdate);



        }

        [TestMethod]
        public void CanSaveOrganisationAddressLink()
        {

            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustOrganisationWithAddress();

            var conRow = ds.Contact[0];
            var conorgRow = ds.ContactOrganisation[0];
            var addressRow = ds.Address[0];
            var linkRow = ds.ContactAddressLink[0];

            var res1 = rig.SaveOrganisation(conRow, conorgRow);
            logger.DebugFormat("Org Save returned CrmID {0}", res1.CrmId.ToString());

            Guid targetContactId = res1.CrmId;

            var res2 = rig.SaveAddress(addressRow);
            logger.DebugFormat("Address returned CrmID {0}", res2.CrmId.ToString());
            Guid targetAddressId = res2.CrmId;

            var res3 = rig.SaveOrganisationAddressLink(linkRow, targetContactId, targetAddressId);
            logger.DebugFormat("ContactAddressLink save returned CrmID {0}", res2.CrmId.ToString());

            Assert.AreEqual(targetContactId, res3.CrmId);
            Assert.AreEqual(false, res3.IsInsert);
            Assert.AreEqual(true, res3.IsUpdate);



        }


        [TestMethod]
        public void CanUpdateContactAlert()
        {

            var rig = new CrmTasks();

            var ds = MakeReferralsDataSet();

            var conAlertRow = ds.ContactAlert[0];


            Guid targetContactId = TestUtils.GetExistingContactId();

            var res = rig.SaveContactAlert(conAlertRow, targetContactId);

            logger.DebugFormat("CanUpdateContactAlert: First Save returned CrmID {0}", res.CrmId.ToString());

            // write the id back so we can do an update
            conAlertRow.TargetContactAlertID = res.CrmId.ToString();
            conAlertRow.Type = "CON";

            var res2 = rig.SaveContactAlert(conAlertRow, targetContactId);

            Assert.AreEqual(res.CrmId, res2.CrmId);
            Assert.AreEqual(false, res2.IsInsert);
            Assert.AreEqual(true, res2.IsUpdate);





        }

        [TestMethod]
        public void CanSaveCase()
        {

            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustCase();

            var caseRow = ds.Case[0];


            Guid targetContactId = TestUtils.GetExistingContactId();

            var res = rig.SaveCase(caseRow, targetContactId);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

        [TestMethod]
        public void CanDeleteCase()
        {

            var rig = new CrmTasks();

            // make the case first
            var ds = TestUtils.MakeReferralsDataSetJustCase();
            var caseRow = ds.Case[0];
            Guid targetContactId = TestUtils.GetExistingContactId();
            var res = rig.SaveCase(caseRow, targetContactId);
            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Guid caseId = res.CrmId;


            rig.DeleteEntityBasedOnTable("Case", caseId);


            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);


        }

        [TestMethod]
        public void CanSaveCaseMinimal()
        {

            var rig = new CrmTasks();

            var ds = TestUtils.MakeReferralsDataSetJustCaseMinimalInfo();

            var caseRow = ds.Case[0];


            Guid targetContactId = TestUtils.GetExistingContactId();

            var res = rig.SaveCase(caseRow, targetContactId);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

        [TestMethod]
        public void CanSaveCaseContactLink()
        {

            var rig = new CrmTasks();

            var ds = MakeReferralsDataSet();

            var linkRow = ds.CaseContactLink[0];


            Guid targetContactId = TestUtils.GetExistingContactId();
            Guid targetCaseId = TestUtils.GetExistingCaseId();
            
            var res = rig.SaveCaseContactLink(linkRow, targetContactId, targetCaseId, true, false);

            logger.DebugFormat("Save returned CrmID {0}", res.CrmId.ToString());
            Assert.AreEqual(true, res.IsInsert);
            Assert.AreEqual(false, res.IsUpdate);




        }

      

        [TestMethod]
        public void CanGetContactCrmIdWhenThereIsAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetContactCrmId("261193");

            Assert.AreEqual(true, res.HasValue);
            Assert.AreEqual(new Guid("55068DBF-6C8F-E211-8ABD-78E3B517ECB7"), res.Value);
            
                

        }

        [TestMethod]
        public void CanGetContacts()
        {
            var rig = new CrmTasks();

            var res = rig.GetContacts();

            Assert.AreEqual(10, res.Count);
            


        }





        [TestMethod]
        public void CanGetCaseCrmIdForRefWithDot()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetCaseCrmId("L006818.1");

            Assert.AreEqual(true, res.HasValue);
            Assert.AreEqual(new Guid("4C17DF11-11CF-E411-9FA7-D89D6763EF70"), res.Value);



        }

        [TestMethod]
        public void CanGetCaseCrmIdForRefWithSlash()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetCaseCrmId("L006818/1");

            Assert.AreEqual(true, res.HasValue);
            Assert.AreEqual(new Guid("4C17DF11-11CF-E411-9FA7-D89D6763EF70"), res.Value);



        }


        [TestMethod]
        public void CanGetContactCrmIdWhenThereIsntAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetContactCrmId("112233");

            Assert.AreEqual(false, res.HasValue);
            



        }

        [TestMethod]
        public void CanGetContactAlertCrmIdWhenThereIsntAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetContactAlertCrmId("madeup");

            Assert.AreEqual(false, res.HasValue);




        }

      

        [TestMethod]
        public void CanGetOrganisationCrmIdWhenThereIsAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetOrganisationCrmId("628818");

            Assert.AreEqual(true, res.HasValue);
            Assert.AreEqual(new Guid("0703464d-9b8e-e211-889a-3c4a92dbd83d"), res.Value);



        }

        [TestMethod]
        public void CanGetOrganisationCrmIdWhenThereIsntAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetOrganisationCrmId("112233");

            Assert.AreEqual(false, res.HasValue);




        }

        [TestMethod]
        public void CanGetAddressCrmIdWhenThereIsAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetAddressCrmId("519886");

            Assert.AreEqual(true, res.HasValue);
            Assert.AreEqual(new Guid("1D182B7B-ABDF-E511-80FB-3863BB357F98"), res.Value);



        }

        [TestMethod]
        public void CanGetAddressCrmIdWhenThereIsntAValue()
        {
            var rig = new CrmTasks();

            Guid? res = rig.GetAddressCrmId("112233");

            Assert.AreEqual(false, res.HasValue);




        }

        [TestMethod]
        public void CanDetectContactExistence()
        {
            var rig = new CrmTasks();

            var exist = rig.DoesContactExist(TestUtils.GetExistingContactId());

            Assert.IsTrue(exist);


        }

        [TestMethod]
        public void CanDetectContactNonExistence()
        {
            var rig = new CrmTasks();
            Guid doesnt = new Guid("7c618799-2c17-4cdf-b37f-dfd7ce01bcdd");
            var exist = rig.DoesContactExist(doesnt);

            Assert.IsFalse(exist);


        }

        [TestMethod]
        public void CanCallGetAlertsToProcess()
        {
            var rig = new CrmTasks();
            var ares = rig.GetAlertsToProcess();
            var atable = ares.ContactAlertDataTable;

            Assert.IsTrue(atable.Count > 0);


        }

        private Referrals MakeReferralsDataSet()
        {
            return TestUtils.MakeReferralsDataSet();

        }
    }
}
