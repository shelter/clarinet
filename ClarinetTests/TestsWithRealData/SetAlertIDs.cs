﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;
using System.Text;

namespace ClarinetTests.TestsWithRealData
{
    /// <summary>
    /// These aren't tests, its some ad-hoc code to update some alertIDs in CRM
    /// </summary>
    [TestClass]
    public class SetAlertIDs
    {

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        //[TestMethod]
        public void SetAlertIDsInCrm()
        {
            // patching up data
            // prescient alert ids are of form 628856.111
            // 
            var processList = new List<Tuple<string,string>>();

            // these are the IDS from live database
            processList.Add(new Tuple<string,string>("fdd8f8c3-ed96-e211-8abd-78e3b517ecb7","321735.17"));
            processList.Add(new Tuple<string,string>("deba11d0-ed96-e211-8abd-78e3b517ecb7","336565.1"));
            processList.Add(new Tuple<string,string>("ffc031dc-ed96-e211-8abd-78e3b517ecb7","357488.6"));
            processList.Add(new Tuple<string,string>("00c131dc-ed96-e211-8abd-78e3b517ecb7","358507.15"));
            processList.Add(new Tuple<string,string>("16c131dc-ed96-e211-8abd-78e3b517ecb7","363030.4"));
            processList.Add(new Tuple<string,string>("17c131dc-ed96-e211-8abd-78e3b517ecb7","363030.8"));
            processList.Add(new Tuple<string,string>("19bf4eee-ed96-e211-8abd-78e3b517ecb7","379769.2"));
            processList.Add(new Tuple<string,string>("24bf4eee-ed96-e211-8abd-78e3b517ecb7","384423.3"));
            processList.Add(new Tuple<string,string>("1eb74bf4-ed96-e211-8abd-78e3b517ecb7","392315.5"));
            processList.Add(new Tuple<string,string>("f3745d00-ee96-e211-8abd-78e3b517ecb7","412180.7"));
            processList.Add(new Tuple<string,string>("67a8b41e-ee96-e211-8abd-78e3b517ecb7","449049.9"));
            processList.Add(new Tuple<string,string>("b6f9f230-ee96-e211-8abd-78e3b517ecb7","460966.2"));
            processList.Add(new Tuple<string,string>("827bf44e-ee96-e211-8abd-78e3b517ecb7","491101.1"));
            processList.Add(new Tuple<string,string>("4839065b-ee96-e211-8abd-78e3b517ecb7","500928.13"));
            processList.Add(new Tuple<string,string>("4939065b-ee96-e211-8abd-78e3b517ecb7","500928.11"));
            processList.Add(new Tuple<string,string>("8f710e67-ee96-e211-8abd-78e3b517ecb7","508890.12"));
            processList.Add(new Tuple<string,string>("8d485679-ee96-e211-8abd-78e3b517ecb7","537503.21"));
            processList.Add(new Tuple<string,string>("a64b667f-ee96-e211-8abd-78e3b517ecb7","547547.14"));
            processList.Add(new Tuple<string,string>("30c47991-ee96-e211-8abd-78e3b517ecb7","588108.16"));
            processList.Add(new Tuple<string,string>("f96094a3-ee96-e211-8abd-78e3b517ecb7","610975.19"));
            processList.Add(new Tuple<string,string>("fa6094a3-ee96-e211-8abd-78e3b517ecb7","610975.18"));
            processList.Add(new Tuple<string, string>("7adcf9c1-ee96-e211-8abd-78e3b517ecb7", "652611.22"));



            

            var tasks = new ClarinetLib.CrmTasks();
            foreach (var pair in processList)
            {
                Guid alertId = new Guid(pair.Item1);
                string prescientAlertId = pair.Item2;
                tasks.UpdateContactAlertPrescientId(alertId, prescientAlertId);



            }
          

        }

        //[TestMethod]
        public void SetAccountIDsInCrm()
        {
            // patching up data
            // prescient alert ids are of form 628856.111
            // 
            var processList = new List<Tuple<string, string>>();

      

            // these are the IDS from live database
            processList.Add(new Tuple<string, string>("22654b2a-2a8f-e211-8c29-984be16d9e77", "272452"));
            processList.Add(new Tuple<string, string>("3efa9653-268f-e211-8abd-78e3b517ecb7", "43186"));
            processList.Add(new Tuple<string, string>("428fedfe-258f-e211-8abd-78e3b517ecb7", "41010"));
            processList.Add(new Tuple<string, string>("4524471d-268f-e211-8abd-78e3b517ecb7", "41869"));
            

            var tasks = new ClarinetLib.CrmTasks();
            foreach (var pair in processList)
            {
                Guid accountId = new Guid(pair.Item1);
                string prescientContactId = pair.Item2;
                tasks.UpdateOrganisationPrescientId(accountId, prescientContactId);



            }


        }

        //[TestMethod]
        public void GetAlertsForContact()
        {
            string[] refnoList = new string[] { "321735", 
                                                "336565", 
                                                "357488", 
                                                "358507", 
                                                "363030", 
                                                "379769", 
                                                "384423", 
                                                "392315", 
                                                "412180", 
                                                "449049", 
                                                "460966", 
                                                "491101", 
                                                "500928", 
                                                "508890", 
                                                "537503", 
                                                "547547", 
                                                "588108", 
                                                "610975", 
                                                "652611"};
            
            string fetchXmlPattern = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>"
                                +"<entity name='gap_alert'>"
                                +"<attribute name='gap_alertreference' />"
                                +"<attribute name='gap_alertid' />"
                                +"<attribute name='gap_alerttype' />"
                                +"<order attribute='gap_alertreference' descending='false' />"
                                +"<link-entity name='contact' from='contactid' to='gap_person' alias='ab'>"
                                +"<filter type='and'>"
                                +"<condition attribute='gap_cicmrecordid' operator='eq' value='{0}' />"
                                +"</filter>"
                                +"</link-entity>"
                                +"</entity>"
                                +"</fetch>";
            
            var pool = new ClarinetLib.CrmConnection.CrmConnectionCache();
            var serv = pool.GetCrmConnectionCached();
            var picks = new ClarinetLib.CrmPicklistTasks();
            var alertTypeList = picks.GetAlertTypes();
            StringBuilder sb = new StringBuilder();
            foreach (string refno in refnoList)
            {

                string fetchXml = string.Format(fetchXmlPattern, refno);
                var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetchXml));

                foreach (Entity entLoop in fetchresults.Entities)
                {
                    var alertType = (OptionSetValue)entLoop["gap_alerttype"];
                    var alertTypeMatch = alertTypeList.FirstOrDefault(at => at.CrmOptionSetValue == alertType.Value);
                    var alertid = (Guid)entLoop["gap_alertid"];
                    logger.DebugFormat("RefNo {0} AlertID {1} Type {2} PType {3}", refno, alertid, alertType, alertTypeMatch.PrescientCode);
                    sb.AppendFormat("{0},{1},{2}{3}", refno, alertid, alertTypeMatch.PrescientCode, Environment.NewLine);
                }
            }
            string info = sb.ToString();
            System.IO.File.WriteAllText(@"C:\Temp\alertstuff.txt", sb.ToString());

        }

        //[TestMethod]
        public void GetAccountsForContact()
        {
            string[] refnoList = new string[] { "272452",
                                                "43186",
                                                "41010",
                                                "41869"};

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='account'>
                    <attribute name='accountid' />
                    <attribute name='gap_cicmrecordid' />
                    <filter type='and'>
                      <condition attribute='gap_cicmrecordid' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            var pool = new ClarinetLib.CrmConnection.CrmConnectionCache();
            var serv = pool.GetCrmConnectionCached();
            var picks = new ClarinetLib.CrmPicklistTasks();
           
            StringBuilder sb = new StringBuilder();
            foreach (string refno in refnoList)
            {

                string fetchXml = string.Format(fetchXmlPattern, refno);
                var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetchXml));

                foreach (Entity entLoop in fetchresults.Entities)
                {
                    var accountid = (Guid)entLoop["accountid"];
                    logger.DebugFormat("RefNo {0} AccountID {1} ", refno, accountid);
                    sb.AppendFormat("{0},{1}{2}", refno, accountid,  Environment.NewLine);
                }
            }
            string info = sb.ToString();
            System.IO.File.WriteAllText(@"C:\Temp\accountstuff.txt", sb.ToString());

        }

    }
}
