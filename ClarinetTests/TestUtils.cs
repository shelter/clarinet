﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClarinetLib.LocalModel;


namespace ClarinetTests
{
    public class TestUtils
    {


        public static Guid GetExistingContactId()
        {
            // ok this isn't great but at least its only in one place
            return new Guid("39D1A076-87B8-E511-8104-1458D043C6C0");
             
            // https://shelterukdev.crm4.dynamics.com/main.aspx?etn=contact&pagetype=entityrecord&id=3d7e431a-4d9d-e211-8a1a-3c4a92dbd83d
        }

        public static Guid GetExistingCaseId()
        {
            // ok this isn't great but at least its only in one place
            return new Guid("345C97E5-87B8-E511-8104-1458D043C6C0");
        }

        public static Referrals MakeReferralsDataSet()
        {
            var ret = new Referrals();

            var addressrow = ret.Address.NewAddressRow();
            addressrow.SourceAddressID = "765776";
            addressrow.SyncID = "123";
            addressrow.TargetAddressID = "";
            addressrow.Telephone1 = "6456463";
            addressrow.Telephone2 = "6475754";
            addressrow.Town = "somewhere";
            addressrow.Postcode = "fhdfg";
            addressrow.DeletionFlag = "N";
            addressrow.County = "londonshire";
            addressrow.Country = "UK";
            addressrow.AddressLine1 = "124 Somewhere Street";
            addressrow.AddressLine2 = "Tottenham";
            addressrow.AddressLine3 = "";
            ret.Address.AddAddressRow(addressrow);

            var cclrow = ret.CaseContactLink.NewCaseContactLinkRow();
            cclrow.SourceCaseContactLinkID = "756868";
            cclrow.SourceCaseID = "A345345/1";
            cclrow.SourceContactID = "345345";
            cclrow.SyncID = "124";
            cclrow.TargetCaseContactLinkID = "";
            cclrow.RoleCode = "PRICLI";
            cclrow.DeletionFlag = "N";
            ret.CaseContactLink.AddCaseContactLinkRow(cclrow);

            var cpprow = ret.CasePresentingProblem.NewCasePresentingProblemRow();
            cpprow.SourceCasePresentingProblemID = "864656";
            cpprow.SourceCaseID = "A345345/1";
            cpprow.SyncID = "125";
            cpprow.TargetCasePresentingProblemID = "";
            cpprow.Description = "something";
            cpprow.DeletionFlag = "N";
            ret.CasePresentingProblem.AddCasePresentingProblemRow(cpprow);


            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345345/1";
            caserow.TargetCaseID = "";
            caserow.SourceReference = "A345345/1";
            caserow.TargetReference = "";
            caserow.SourceContactID = "345345";
            caserow.CaseTypeCode = "47";
            caserow.Description = "test record";
            caserow.StartDate = "01/Jan/2010";
            caserow.EndDate = "30/Mar/2010";
            caserow.OwnerCode = "LS01";
            caserow.StatusCode = "O";
            caserow.DeletionFlag = "N";
            caserow.SyncID = "126";

            ret.Case.AddCaseRow(caserow);

            caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345346/1";
            caserow.TargetCaseID = "";
            caserow.SourceReference = "A345346/1";
            caserow.TargetReference = "";
            caserow.SourceContactID = "345346";
            caserow.CaseTypeCode = "47";
            caserow.Description = "test record again";
            caserow.StartDate = "01/Mar/2011";
            caserow.EndDate = "30/Jun/2011";
            caserow.OwnerCode = "AC01";
            caserow.StatusCode = "O";
            caserow.DeletionFlag = "N";
            caserow.SyncID = "127";

            ret.Case.AddCaseRow(caserow);

            var calrow = ret.ContactAddressLink.NewContactAddressLinkRow();
            calrow.SourceContactAddressLinkID = "234456";
            calrow.SourceContactID = "345346";
            calrow.SourceAddressID = "765776";
            calrow.SyncID = "128";
            calrow.TargetContactAddressLinkID = "";
            calrow.DeletionFlag = "N";
            ret.ContactAddressLink.AddContactAddressLinkRow(calrow);

            var alertrow = ret.ContactAlert.NewContactAlertRow();
            alertrow.SourceContactAlertID = "749634";
            alertrow.SourceContactID = "345346";
            alertrow.SyncID = "129";
            alertrow.TargetContactAlertID = "";
            alertrow.Type = "SG";
            alertrow.RaisedBy = "LC01";
            alertrow.Notes = "something something";
            alertrow.DeletionFlag = "N";
            alertrow.DateAlertAdded = "01/Jan/2010";
            alertrow.DateAlertClosed = "16/Mar/2010";
            ret.ContactAlert.AddContactAlertRow(alertrow);

            var conlinkrow = ret.ContactContactLink.NewContactContactLinkRow();
            conlinkrow.SourceContactContactLinkID = "112233";
            conlinkrow.ParentSourceContactID = "345346";
            conlinkrow.SourceContactID = "345345";
            conlinkrow.TargetContactContactLinkID = "";
            conlinkrow.SyncID = "130";
            conlinkrow.DeletionFlag = "N";
            conlinkrow.RoleCode = "MIGCL";
            ret.ContactContactLink.AddContactContactLinkRow(conlinkrow);

            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345346";
            indvrow.Surname = "testperson2";
            indvrow.Title = "Mr";
            indvrow.NINumber = "757547454674";
            indvrow.Middlenames = "UAT";
            indvrow.Forename = "Terry2";
            indvrow.Birthdate = "17/Jun/1995";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);

            var orgrow = ret.ContactOrganisation.NewContactOrganisationRow();
            orgrow.SourceContactID = "234556";
            orgrow.Name = "Some Organisation";
            orgrow.WebsiteURL = "something.com";
            ret.ContactOrganisation.AddContactOrganisationRow(orgrow);

            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345346";
            contactrow.SourceReference = "A345346";
            contactrow.TargetContactID = "";
            contactrow.TargetReference = "";
            contactrow.SyncID = "131";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "dgdsgdsfgds@sdgdsgdsf.com";
            contactrow.EmailAddress2 = "";
            contactrow.EmailAddress3 = "";
            contactrow.MobilePhone = "64566843";
            contactrow.Primary = "1";
            contactrow.Salutation = "Something";
            contactrow.Telephone1 = "7575475474";
            contactrow.Telephone2 = "";
            ret.Contact.AddContactRow(contactrow);

            return ret;

        }

        public static Referrals MakeReferralsDataSetJustContact()
        {
            var ret = new Referrals();

            
            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345346";
            indvrow.Surname = "testperson2";
            indvrow.Title = "Mr";
            indvrow.NINumber = "757547454674";
            indvrow.Middlenames = "UAT";
            indvrow.Forename = "Terry2";
            indvrow.Birthdate = "17/Jun/1995";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);

            
            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345346";
            contactrow.SourceReference = "A345346";
            contactrow.TargetContactID = "";
            contactrow.TargetReference = "";
            contactrow.SyncID = "132";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "dgdsgdsfgds@sdgdsgdsf.com";
            contactrow.EmailAddress2 = "jkhgkgjhk";
            contactrow.EmailAddress3 = "utyutrjhtj";
            contactrow.Primary = "1";
            contactrow.Salutation = "Something";
            contactrow.Telephone1 = "7575475474";
            contactrow.Telephone2 = "45645363";
            contactrow.MobilePhone = "64566843";
            ret.Contact.AddContactRow(contactrow);

            

            return ret;

        }


        public static Referrals MakeReferralsDataSetJustAddress()
        {
            var ret = new Referrals();

            var addressrow = ret.Address.NewAddressRow();
            addressrow.SourceAddressID = "765776";
            addressrow.SyncID = "123";
            addressrow.TargetAddressID = "";
            addressrow.Telephone1 = "6456463";
            addressrow.Telephone2 = "6475754";
            addressrow.Town = "somewhere";
            addressrow.Postcode = "fhdfg";
            addressrow.DeletionFlag = "N";
            addressrow.County = "londonshire";
            addressrow.Country = "UK";
            addressrow.AddressLine1 = "124 Somewhere Street";
            addressrow.AddressLine2 = "Tottenham";
            addressrow.AddressLine3 = "";
            ret.Address.AddAddressRow(addressrow);

            
            return ret;

        }

      
        public static Referrals MakeReferralsDataSetTwoContactsOneDeleted()
        {
            var ret = new Referrals();


            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345346";
            indvrow.Surname = "testperson2";
            indvrow.Title = "Mr";
            indvrow.NINumber = "757547454674";
            indvrow.Middlenames = "UAT";
            indvrow.Forename = "Terry2";
            indvrow.Birthdate = "17/Jun/1995";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);


            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345346";
            contactrow.SourceReference = "A345346";
            contactrow.TargetContactID = "";
            contactrow.TargetReference = "";
            contactrow.SyncID = "123";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "dgdsgdsfgds@sdgdsgdsf.com";
            contactrow.EmailAddress2 = "jkhgkgjhk";
            contactrow.EmailAddress3 = "utyutrjhtj";
            contactrow.Primary = "1";
            contactrow.Salutation = "Something";
            contactrow.Telephone1 = "7575475474";
            contactrow.Telephone2 = "45645363";
            contactrow.MobilePhone = "64566843";
            ret.Contact.AddContactRow(contactrow);

            contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345212";
            contactrow.SourceReference = "A345212";
            contactrow.TargetContactID = "e16b78b4-319e-446a-9c81-33756a3bb978";
            contactrow.TargetReference = "C100023";
            contactrow.SyncID = "124";
            contactrow.DeletionFlag = "Y";
            ret.Contact.AddContactRow(contactrow);


            return ret;

        }

        public static Referrals MakeReferralsDataSetJustCase()
        {
            var ret = new Referrals();

            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345582/1";
            caserow.TargetCaseID = "";
            caserow.SourceReference = "A345582/1";
            caserow.TargetReference = "";
            caserow.SourceContactID = "345582";
            caserow.CaseTypeCode = "47"; 
            caserow.Description = "test record";
            caserow.StartDate = "01/Jan/2010";
            caserow.EndDate = "30/Mar/2010";
            caserow.OwnerCode = "LS01";
            caserow.StatusCode = "O";
            caserow.DeletionFlag = "N";
            caserow.SyncID = "133";

            ret.Case.AddCaseRow(caserow);

            return ret;

        }

        public static Referrals MakeReferralsDataSetJustCaseUpdate()
        {
            var ret = new Referrals();

            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345582/1";
            caserow.TargetCaseID = "87b5d787-d549-45b2-a0ff-fe70ba32d9c3";
            caserow.SourceReference = "A345582/1";
            caserow.TargetReference = "C345581/1";
            caserow.SourceContactID = "345582";
            caserow.CaseTypeCode = "47";
            caserow.Description = "test record";
            caserow.StartDate = "01/Jan/2010";
            caserow.EndDate = "30/Mar/2010";
            caserow.OwnerCode = "LS01";
            caserow.StatusCode = "O";
            caserow.DeletionFlag = "N";
            caserow.SyncID = "133";

            ret.Case.AddCaseRow(caserow);

            return ret;

        }

        public static Referrals MakeReferralsDataSetJustCaseWithContact()
        {
            var ret = new Referrals();

            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345582/1";
            caserow.TargetCaseID = "";
            caserow.SourceReference = "A345582/1";
            caserow.TargetReference = "";
            caserow.SourceContactID = "345582";
            caserow.CaseTypeCode = "47";
            caserow.Description = "test record";
            caserow.StartDate = "01/Jan/2010";
            caserow.EndDate = "30/Mar/2010";
            caserow.OwnerCode = "LS01";
            caserow.StatusCode = "O";
            caserow.DeletionFlag = "N";
            caserow.SyncID = "133";

            ret.Case.AddCaseRow(caserow);

            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345582";
            indvrow.Surname = "testperson2";
            indvrow.Title = "Mr";
            indvrow.NINumber = "757547454674";
            indvrow.Middlenames = "UAT";
            indvrow.Forename = "Terry2";
            indvrow.Birthdate = "17/Jun/1995";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);

           

            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345582";
            contactrow.SourceReference = "A345582";
            contactrow.TargetContactID = "41a11e0c-bbdc-46d4-9c07-8883f927c9d1";
            contactrow.TargetReference = "C345582";
            contactrow.SyncID = "131";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "dgdsgdsfgds@sdgdsgdsf.com";
            contactrow.EmailAddress2 = "";
            contactrow.EmailAddress3 = "";
            contactrow.MobilePhone = "64566843";
            contactrow.Primary = "1";
            contactrow.Salutation = "Something";
            contactrow.Telephone1 = "7575475474";
            contactrow.Telephone2 = "";
            ret.Contact.AddContactRow(contactrow);


            return ret;

        }

        public static Referrals MakeReferralsDataSetJustAlertWithContact()
        {
            var ret = new Referrals();

            var alertrow = ret.ContactAlert.NewContactAlertRow();
            alertrow.SourceContactAlertID = "749634";
            alertrow.SourceContactID = "345346";
            alertrow.SyncID = "129";
            alertrow.TargetContactAlertID = "";
            alertrow.Type = "SG";
            alertrow.RaisedBy = "LC01";
            alertrow.Notes = "something something";
            alertrow.DeletionFlag = "N";
            alertrow.DateAlertAdded = "01/Jan/2010";
            alertrow.DateAlertClosed = "16/Mar/2010";
            ret.ContactAlert.AddContactAlertRow(alertrow);

            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345346";
            indvrow.Surname = "testperson2";
            indvrow.Title = "Mr";
            indvrow.NINumber = "757547454674";
            indvrow.Middlenames = "UAT";
            indvrow.Forename = "Terry2";
            indvrow.Birthdate = "17/Jun/1995";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);



            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345346";
            contactrow.SourceReference = "A345346";
            contactrow.TargetContactID = "41a11e0c-bbdc-46d4-9c07-8883f927c9d1";
            contactrow.TargetReference = "C345346";
            contactrow.SyncID = "131";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "dgdsgdsfgds@sdgdsgdsf.com";
            contactrow.EmailAddress2 = "";
            contactrow.EmailAddress3 = "";
            contactrow.MobilePhone = "64566843";
            contactrow.Primary = "1";
            contactrow.Salutation = "Something";
            contactrow.Telephone1 = "7575475474";
            contactrow.Telephone2 = "";
            ret.Contact.AddContactRow(contactrow);


            return ret;

        }

        public static Referrals MakeReferralsDataSetJustCaseDeletion()
        {
            var ret = new Referrals();

            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345211/1";
            caserow.TargetCaseID = "32a35219-7d45-4fd0-ad46-08118517f114";
            caserow.SourceReference = "A345211/1";
            caserow.TargetReference = "C100234";
            caserow.SourceContactID = "345582";
            caserow.DeletionFlag = "Y";
            caserow.SyncID = "422";

            ret.Case.AddCaseRow(caserow);

            return ret;

        }



        public static Referrals MakeReferralsDataSetJustCaseMinimalInfo()
        {
            var ret = new Referrals();

            var caserow = ret.Case.NewCaseRow();
            caserow.SourceCaseID = "A345582/1";
            caserow.TargetCaseID = "";
            caserow.SourceReference = "A345582/1";
            caserow.TargetReference = "";
            caserow.SourceContactID = "345582";
            caserow.CaseTypeCode = "47"; 
            caserow.DeletionFlag = "N";
            caserow.SyncID = "134";

            ret.Case.AddCaseRow(caserow);

            return ret;

        }

        public static Referrals MakeReferralsDataSetJustPresProbDeletion()
        {
            var ret = new Referrals();

            var cpprow = ret.CasePresentingProblem.NewCasePresentingProblemRow();
            cpprow.SourceCaseID = "";
            cpprow.SourceCasePresentingProblemID = "L345345.1.7456";
            cpprow.SyncID = "125";
            cpprow.TargetCasePresentingProblemID = "1e9691d9-71cb-49e3-8bbe-e9b4bf83450d";
            cpprow.DeletionFlag = "Y";
            ret.CasePresentingProblem.AddCasePresentingProblemRow(cpprow);

            return ret;

        }

        public static Referrals MakeReferralsDataSetJustContactMinimalInfo()
        {
            var ret = new Referrals();


            var indvrow = ret.ContactIndividual.NewContactIndividualRow();
            indvrow.SourceContactID = "345346";
            indvrow.Surname = "testperson4";
            ret.ContactIndividual.AddContactIndividualRow(indvrow);


            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345346";
            contactrow.SourceReference = "A345346";
            contactrow.TargetContactID = "";
            contactrow.TargetReference = "";
            contactrow.SyncID = "135";
            contactrow.DeletionFlag = "N";
            contactrow.Primary = "1";
            ret.Contact.AddContactRow(contactrow);

            return ret;

        }


        public static Referrals MakeReferralsDataSetJustOrganisationWithAddress()
        {
            var ret = new Referrals();

            var orgrow = ret.ContactOrganisation.NewContactOrganisationRow();
            orgrow.SourceContactID = "345128";
            orgrow.Name = "Test Organisation Biscuit Factory";
            orgrow.WebsiteURL = "mmmmmbiscuitsgdsfgdfg.com";
            ret.ContactOrganisation.AddContactOrganisationRow(orgrow);


            var contactrow = ret.Contact.NewContactRow();
            contactrow.SourceContactID = "345128";
            contactrow.SourceReference = "A345128";
            contactrow.TargetContactID = "";
            contactrow.TargetReference = "";
            contactrow.SyncID = "136";
            contactrow.DeletionFlag = "N";
            contactrow.EmailAddress1 = "fasfdas@safsdfs.com";
            contactrow.EmailAddress2 = "hfghfdgh@fdhdfhdgf.com";
            contactrow.EmailAddress3 = "gfjgj@gfjgfjf.com";
            contactrow.Primary = "0";
            contactrow.Salutation = "SomethingElse";
            contactrow.Telephone1 = "7656823423";
            contactrow.Telephone2 = "132889131";
            contactrow.MobilePhone = "3454379890";
            ret.Contact.AddContactRow(contactrow);

            var calrow = ret.ContactAddressLink.NewContactAddressLinkRow();
            calrow.SourceContactAddressLinkID = "345128,765121";
            calrow.SourceContactID = "345128";
            calrow.SourceAddressID = "765121";
            calrow.SyncID = "137";
            calrow.TargetContactAddressLinkID = "";
            calrow.DeletionFlag = "N";
            ret.ContactAddressLink.AddContactAddressLinkRow(calrow);

            var addressrow = ret.Address.NewAddressRow();
            addressrow.SourceAddressID = "765121";
            addressrow.SyncID = "138";
            addressrow.TargetAddressID = "";
            addressrow.Telephone1 = "75757543";
            addressrow.Telephone2 = "5475745";
            addressrow.Town = "somewhere";
            addressrow.Postcode = "fhdfg";
            addressrow.DeletionFlag = "N";
            addressrow.County = "londonshire";
            addressrow.Country = "UK";
            addressrow.AddressLine1 = "Test Address Something";
            addressrow.AddressLine2 = "Walthamstow";
            addressrow.AddressLine3 = "";
            ret.Address.AddAddressRow(addressrow);

            return ret;

        }
    }
}
