﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;
using Microsoft.Crm.Sdk;
using ClarinetLib.CrmConnection;
using Rhino.Mocks;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Messages;

namespace ClarinetTests.TestsWithMocks
{
    [TestClass]
    public class CrmTasksMockTests
    {
        [TestMethod]
        public void CanSaveContactMock()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            
            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Create(Arg<Entity>.Is.Anything)).Return(returnGuid);
            
            
            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

           
            var titleDict = new Dictionary<int?, string>();
            titleDict.Add(123, "mrs");
            titleDict.Add(145, "mr");
           
            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetTitleOptionSet()).Return(titleDict);

            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustContact();

            var crmres = tasks.SaveContact(ds.Contact[0], ds.ContactIndividual[0]);

            

            IList<object[]> argumentsSentToCreate =
             mockCrmApi.GetArgumentsForCallsMadeOn(x => x.Create(Arg<Entity>.Is.Anything));
            // where [0] = first call and [0] = first argument ...
            var passedEntity = (Entity)argumentsSentToCreate[0][0];

            Assert.IsNotNull(passedEntity);
            Assert.AreEqual("345346", passedEntity["gap_prescientcontactid"]);
            Assert.AreEqual("A345346", passedEntity["gap_prescientclientid"]);

            Assert.AreEqual("Terry2", passedEntity["firstname"]);
            Assert.AreEqual("testperson2", passedEntity["lastname"]);
            Assert.AreEqual(new OptionSetValue(145), passedEntity["gap_title"]);
            Assert.AreEqual("757547454674", passedEntity["gap_nationalinsuranceno"]);
            Assert.AreEqual("UAT", passedEntity["middlename"]);
            Assert.AreEqual(new DateTime(1995, 6, 17), (DateTime)passedEntity["birthdate"]);
            

            Assert.AreEqual("dgdsgdsfgds@sdgdsgdsf.com", passedEntity["emailaddress1"]);
            Assert.AreEqual("jkhgkgjhk", passedEntity["emailaddress2"]);
            Assert.AreEqual("utyutrjhtj", passedEntity["emailaddress3"]);
            Assert.AreEqual("7575475474", passedEntity["telephone1"]);
            Assert.AreEqual( "45645363", passedEntity["telephone2"]);
            Assert.AreEqual( "64566843", passedEntity["telephone3"]);

            // gap_telephone1type etc?

            
            
            mockCrmApi.VerifyAllExpectations();
            mockPool.VerifyAllExpectations();
        }


        [TestMethod]
        public void CanSaveCaseMock()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");

            Guid caseTypeId_47 = new Guid("632af875-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseTypeId_48 = new Guid("602af875-9a8e-e211-889a-3c4a92dbd83d");
            var caseTypeList = new List<CrmPicklistTasks.CrmCaseType>();
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_47,
                Name = "test",
                Code = "47"
            });
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_48,
                Name = "test2",
                Code = "48"
            });


            Guid caseWorkerId_LS01 = new Guid("04c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseWorkerId_GH04 = new Guid("02c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            var caseWorkerList = new List<CrmPicklistTasks.CrmCaseWorker>();
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_GH04,
                CaseWorkerName = "test",
                PrescientInitialsId = "GH04"
            });
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_LS01,
                CaseWorkerName = "test2",
                PrescientInitialsId = "LS01"
            });

            int statusreason_active = 1;
            //int statusreason_complete = 810340001;
            var statusCodeList = new List<CrmPicklistTasks.CrmOptionSetForPicklist>();

            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "O" });
            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "R" });
            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 810340001, PrescientCode = "C" });
            

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Create(Arg<Entity>.Is.Anything)).Return(returnGuid);


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

            
            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetCaseTypes()).Return(caseTypeList);
            mockPicks.Stub(p => p.GetCaseworkers()).Return(caseWorkerList);
            mockPicks.Stub(p => p.GetCaseStatuses()).Return(statusCodeList);
            
            var tasks = new CrmTasks(mockPool,  mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustCase();

            Guid targetContactId = new Guid("1103464d-9b8e-e211-889a-3c4a92dbd83d");

            var crmres = tasks.SaveCase(ds.Case[0], targetContactId);



            IList<object[]> argumentsSentToCreate =
             mockCrmApi.GetArgumentsForCallsMadeOn(x => x.Create(Arg<Entity>.Is.Anything));
            // where [0] = first call and [0] = first argument ...
            var passedEntity = (Entity)argumentsSentToCreate[0][0];


            Assert.IsNotNull(passedEntity);
            Assert.AreEqual(targetContactId, ((EntityReference)passedEntity["customerid"]).Id);
            Assert.AreEqual(targetContactId, ((EntityReference)passedEntity["gap_clientcontactid"]).Id);

            Assert.IsTrue(passedEntity.Attributes.Contains("gap_prescientcaseid"));
            Assert.AreEqual("A345582/1", (string)passedEntity["gap_prescientcaseid"]);
            Assert.IsTrue(passedEntity.Attributes.Contains("gap_mattertype"));
            Assert.AreEqual(caseTypeId_47, ((EntityReference)passedEntity["gap_mattertype"]).Id);
            //TODO description?
            Assert.AreEqual(new DateTime(2010, 1, 1), (DateTime)passedEntity["gap_openingdate"]);
            Assert.AreEqual(new DateTime(2010, 3, 30), (DateTime)passedEntity["gap_closuredate"]);
            Assert.AreEqual(caseWorkerId_LS01, ((EntityReference)passedEntity["gap_caseworkerid"]).Id);

            Assert.AreEqual(statusreason_active, ((OptionSetValue)passedEntity["statuscode"]).Value);
            Assert.AreEqual(true, (bool)passedEntity["gap_cicmstubrecord"]);
            
            
            
            


            mockCrmApi.VerifyAllExpectations();
            mockPool.VerifyAllExpectations();
        }


        [TestMethod]
        public void CanSaveCaseUpdateMock()
        {
            

            Guid caseTypeId_47 = new Guid("632af875-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseTypeId_48 = new Guid("602af875-9a8e-e211-889a-3c4a92dbd83d");
            var caseTypeList = new List<CrmPicklistTasks.CrmCaseType>();
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_47,
                Name = "test",
                Code = "47"
            });
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_48,
                Name = "test2",
                Code = "48"
            });


            Guid caseWorkerId_LS01 = new Guid("04c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseWorkerId_GH04 = new Guid("02c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            var caseWorkerList = new List<CrmPicklistTasks.CrmCaseWorker>();
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_GH04,
                CaseWorkerName = "test",
                PrescientInitialsId = "GH04"
            });
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_LS01,
                CaseWorkerName = "test2",
                PrescientInitialsId = "LS01"
            });

            int statusreason_active = 1;
            //int statusreason_complete = 810340001;
            var statusCodeList = new List<CrmPicklistTasks.CrmOptionSetForPicklist>();

            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "O" });
            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "R" });
            statusCodeList.Add(new CrmPicklistTasks.CrmOptionSetForPicklist() { CrmOptionSetValue = 810340001, PrescientCode = "C" });


            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Update(Arg<Entity>.Is.Anything));


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);


            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetCaseTypes()).Return(caseTypeList);
            mockPicks.Stub(p => p.GetCaseworkers()).Return(caseWorkerList);
            mockPicks.Stub(p => p.GetCaseStatuses()).Return(statusCodeList);

            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustCaseUpdate();

            Guid targetContactId = new Guid("1103464d-9b8e-e211-889a-3c4a92dbd83d");

            var crmres = tasks.SaveCase(ds.Case[0], targetContactId);



            IList<object[]> argumentsSentToCreate =
             mockCrmApi.GetArgumentsForCallsMadeOn(x => x.Update(Arg<Entity>.Is.Anything));
            // where [0] = first call and [0] = first argument ...
            var passedEntity = (Entity)argumentsSentToCreate[0][0];


            Assert.IsNotNull(passedEntity);
            Assert.IsFalse(passedEntity.Contains("customerid"),"customerid should not have been set");
            Assert.IsFalse(passedEntity.Contains("gap_clientcontactid"),"gap_clientcontactid should not have been set");
            
            Assert.IsTrue(passedEntity.Attributes.Contains("gap_prescientcaseid"));
            Assert.AreEqual("A345582/1", (string)passedEntity["gap_prescientcaseid"]);
            Assert.IsTrue(passedEntity.Attributes.Contains("gap_mattertype"));
            Assert.AreEqual(caseTypeId_47, ((EntityReference)passedEntity["gap_mattertype"]).Id);
            
            Assert.AreEqual(new DateTime(2010, 1, 1), (DateTime)passedEntity["gap_openingdate"]);
            Assert.AreEqual(new DateTime(2010, 3, 30), (DateTime)passedEntity["gap_closuredate"]);
            Assert.AreEqual(caseWorkerId_LS01, ((EntityReference)passedEntity["gap_caseworkerid"]).Id);

            Assert.AreEqual(statusreason_active, ((OptionSetValue)passedEntity["statuscode"]).Value);
            Assert.AreEqual(true, (bool)passedEntity["gap_cicmstubrecord"]);






            mockCrmApi.VerifyAllExpectations();
            mockPool.VerifyAllExpectations();
        }

        [TestMethod]
        public void CanSaveCaseMinimalInfoMock()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");

            Guid caseTypeId_47 = new Guid("632af875-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseTypeId_48 = new Guid("602af875-9a8e-e211-889a-3c4a92dbd83d");
            var caseTypeList = new List<CrmPicklistTasks.CrmCaseType>();
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_47,
                Name = "test",
                Code = "47"
            });
            caseTypeList.Add(new CrmPicklistTasks.CrmCaseType()
            {
                CrmId = caseTypeId_48,
                Name = "test2",
                Code = "48"
            });


            Guid caseWorkerId_LS01 = new Guid("04c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            Guid caseWorkerId_GH04 = new Guid("02c6e56f-9a8e-e211-889a-3c4a92dbd83d");
            var caseWorkerList = new List<CrmPicklistTasks.CrmCaseWorker>();
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_GH04,
                CaseWorkerName = "test",
                PrescientInitialsId = "GH04"
            });
            caseWorkerList.Add(new CrmPicklistTasks.CrmCaseWorker()
            {
                CaseWorkerId = caseWorkerId_LS01,
                CaseWorkerName = "test2",
                PrescientInitialsId = "LS01"
            });

          

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Create(Arg<Entity>.Is.Anything)).Return(returnGuid);


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);


            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetCaseTypes()).Return(caseTypeList);
            mockPicks.Stub(p => p.GetCaseworkers()).Return(caseWorkerList);

            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustCaseMinimalInfo();

            Guid targetContactId = new Guid("1103464d-9b8e-e211-889a-3c4a92dbd83d");

            var crmres = tasks.SaveCase(ds.Case[0], targetContactId);



            IList<object[]> argumentsSentToCreate =
             mockCrmApi.GetArgumentsForCallsMadeOn(x => x.Create(Arg<Entity>.Is.Anything));
            // where [0] = first call and [0] = first argument ...
            var passedEntity = (Entity)argumentsSentToCreate[0][0];


            Assert.IsNotNull(passedEntity);
            Assert.AreEqual(targetContactId, ((EntityReference)passedEntity["customerid"]).Id);
            Assert.AreEqual(targetContactId, ((EntityReference)passedEntity["gap_clientcontactid"]).Id);

            Assert.IsTrue(passedEntity.Attributes.Contains("gap_prescientcaseid"));
            Assert.AreEqual("A345582/1", (string)passedEntity["gap_prescientcaseid"]);
            Assert.IsTrue(passedEntity.Attributes.Contains("gap_mattertype"));
            
            Assert.AreEqual(true, (bool)passedEntity["gap_cicmstubrecord"]);






            mockCrmApi.VerifyAllExpectations();
            mockPool.VerifyAllExpectations();
        }

        [TestMethod]
        public void CanSaveContactDetectBlankNameMock()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Create(Arg<Entity>.Is.Anything)).Return(returnGuid);


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);


            var titleDict = new Dictionary<int?, string>();
            titleDict.Add(123, "mrs");
            titleDict.Add(145, "mr");

            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetTitleOptionSet()).Return(titleDict);

            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustContact();
            ds.ContactIndividual[0].Forename = "";
            ds.ContactIndividual[0].Surname = "";

            bool expectedErrorFound = false;
            try{
            var crmres = tasks.SaveContact(ds.Contact[0], ds.ContactIndividual[0]);
            }
            catch(ApplicationException ex)
            {
                expectedErrorFound = ex.Message.Contains("has no Forename or Surname");
                if (!expectedErrorFound)
                    throw ex;
            }


            Assert.IsTrue(expectedErrorFound, "Expected a specific error");
        }

        [TestMethod]
        public void CanSaveContactMapTitleCaseInsensitiveMock()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(r => r.Create(Arg<Entity>.Is.Anything)).Return(returnGuid);

            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

            var titleDict = new Dictionary<int?, string>();
            titleDict.Add(123, "mrs");
            titleDict.Add(145, "mr");

            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetTitleOptionSet()).Return(titleDict);

            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustContact();
            ds.ContactIndividual[0].Title = "MR";
            var crmres = tasks.SaveContact(ds.Contact[0], ds.ContactIndividual[0]);



            IList<object[]> argumentsSentToCreate =
             mockCrmApi.GetArgumentsForCallsMadeOn(x => x.Create(Arg<Entity>.Is.Anything));
            // where [0] = first call and [0] = first argument ...
            var passedEntity = (Entity)argumentsSentToCreate[0][0];

            Assert.IsNotNull(passedEntity);
            Assert.AreEqual("345346", passedEntity["gap_prescientcontactid"]);
            Assert.AreEqual("A345346", passedEntity["gap_prescientclientid"]);

            Assert.IsTrue(passedEntity.Contains("gap_title"), "title should be set");
            Assert.AreEqual(new OptionSetValue(145), passedEntity["gap_title"]);
            


            
        }

        [TestMethod]
        public void CanSaveCaseThrowErrorIfFieldsNullMock()
        {
            

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Stub(r => r.Create(Arg<Entity>.Is.Anything)).Return(Guid.Empty);


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Stub(r => r.GetCrmConnectionCached()).Return(mockCrmApi);


            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            
            var tasks = new CrmTasks(mockPool, mockPicks);

            var ds = TestUtils.MakeReferralsDataSetJustCase();
            ds.Case[0].CaseTypeCode = "";
            ds.Case[0].CentreOrServiceCode = "";

            Guid targetContactId = new Guid("1103464d-9b8e-e211-889a-3c4a92dbd83d");

            bool expectedErrorFound = false;
            try
            {
                var crmres = tasks.SaveCase(ds.Case[0], targetContactId);
            }
            catch (Exception ex)
            {
                expectedErrorFound = ex.Message.Contains("blank CaseType and blank CentreOrService");
                if (!expectedErrorFound)
                    throw ex;


            }

            Assert.IsTrue(expectedErrorFound, "expecting error");
        }

        [TestMethod]
        public void CanGetAlertsForProcessing()
        {
            var dummyAlert = new Entity("gap_alert");
            dummyAlert["gap_alertid"] = new Guid("771acfe0-ade8-4eba-b314-29b0ece14413");
            dummyAlert["gap_person"] = new EntityReference("contact", new Guid("bb713960-2270-48d2-a538-237e31855add"));
            dummyAlert["gap_datealertadded"] = new DateTime(2013, 03, 12);

            var entityColl = new EntityCollection();
            entityColl.Entities.Add(dummyAlert);

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(c => c.RetrieveMultiple(null)).IgnoreArguments().Return(entityColl);
            


            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            

            var tasks = new CrmTasks(mockPool, mockPicks);

            var res = tasks.GetAlertsToProcess();

            Assert.AreEqual(1, res.TotalCount);
            Assert.AreEqual(1, res.Succeeded);
            Assert.AreEqual(0, res.Failed);

            var row = res.ContactAlertDataTable[0];

            Assert.AreEqual("771acfe0-ade8-4eba-b314-29b0ece14413", row.SourceContactAlertID);
            Assert.AreEqual("bb713960-2270-48d2-a538-237e31855add", row.SourceContactID);
            Assert.AreEqual("12/Mar/2013", row.DateAlertAdded);
               

        }

        [TestMethod]
        public void CanGetAlertsForProcessingHandleError()
        {
            var dummyAlert = new Entity("gap_alert");
            dummyAlert["gap_alertid"] = new Guid("771acfe0-ade8-4eba-b314-29b0ece14413");
            dummyAlert["gap_person"] = new EntityReference("contact", new Guid("bb713960-2270-48d2-a538-237e31855add"));
            dummyAlert["gap_datealertadded"] = new DateTime(2013, 03, 12);
            dummyAlert["gap_alerttype"] = new OptionSetValue(1);

            var entityColl = new EntityCollection();
            entityColl.Entities.Add(dummyAlert);

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(c => c.RetrieveMultiple(null)).IgnoreArguments().Return(entityColl);



            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();
            mockPicks.Stub(p => p.GetAlertTypes()).Throw(new ApplicationException("test error to check handling"));

            var tasks = new CrmTasks(mockPool, mockPicks);

            var res = tasks.GetAlertsToProcess();

            Assert.AreEqual(1, res.TotalCount);
            Assert.AreEqual(0, res.Succeeded);
            Assert.AreEqual(1, res.Failed);
            Assert.AreEqual(1, res.FailedInfo.Count);

            var failinfo = res.FailedInfo[0];


            Assert.AreEqual("771acfe0-ade8-4eba-b314-29b0ece14413", failinfo.Item1);
            Assert.AreEqual("test error to check handling", failinfo.Item2);
            


        }

        [TestMethod]
        public void CanGetAlertsForProcessing_RaisedByNotNull()
        {
            var dummyAlert = new Entity("gap_alert");
            dummyAlert["gap_alertid"] = new Guid("771acfe0-ade8-4eba-b314-29b0ece14413");
            dummyAlert["gap_person"] = new EntityReference("contact", new Guid("bb713960-2270-48d2-a538-237e31855add"));
            dummyAlert["gap_datealertadded"] = new DateTime(2013, 03, 12);

            var entityColl = new EntityCollection();
            entityColl.Entities.Add(dummyAlert);

            var mockCrmApi = MockRepository.GenerateMock<IOrganizationService>();
            mockCrmApi.Expect(c => c.RetrieveMultiple(null)).IgnoreArguments().Return(entityColl);



            var mockPool = MockRepository.GenerateMock<CrmConnectionCache>();
            mockPool.Expect(r => r.GetCrmConnectionCached()).Return(mockCrmApi);

            var mockPicks = MockRepository.GenerateStub<CrmPicklistTasks>();


            var tasks = new CrmTasks(mockPool, mockPicks);

            var res = tasks.GetAlertsToProcess();

            Assert.AreEqual(1, res.TotalCount);
            Assert.AreEqual(1, res.Succeeded);
            Assert.AreEqual(0, res.Failed);

            var row = res.ContactAlertDataTable[0];

            Assert.AreEqual("771acfe0-ade8-4eba-b314-29b0ece14413", row.SourceContactAlertID);
            Assert.AreEqual("bb713960-2270-48d2-a538-237e31855add", row.SourceContactID);
            Assert.AreEqual("12/Mar/2013", row.DateAlertAdded);
            Assert.AreEqual("", row.RaisedBy);

        }

    }
}
