﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;

using Rhino.Mocks;

namespace ClarinetTests.TestsWithMocks
{
    [TestClass]
    public class IgnoreListMockTests
    {
        [TestMethod]
        public void CanLoadIgnoreList()
        {
            var il = new IgnoreList();

            bool ignore1 = il.ShouldIgnore("wrong", "nope");
            bool ignore2 = il.ShouldIgnore("case", "9999999.1");

            Assert.IsFalse(ignore1);
            Assert.IsTrue(ignore2);
        }
    }
}
