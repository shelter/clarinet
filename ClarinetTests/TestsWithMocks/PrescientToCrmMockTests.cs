﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ClarinetLib;

using Rhino.Mocks;

namespace ClarinetTests.TestsWithMocks
{
    [TestClass]
    public class PrescientToCrmMockTests
    {

        /*
        [TestMethod]
        public void CanTransferContact()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<Pres>();
            var resp = new GetDetailsResponse("", TestUtils.MakeReferralsDataSetJustContact());
            mockWebService.Expect(w => w.GetDetails(Arg<GetDetailsRequest>.Is.Anything)).Return(resp);
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));

            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            
            mockCrmTasks.Expect(c => c.SaveContact(Arg<Referrals.ContactRow>.Is.Anything, Arg<Referrals.ContactIndividualRow>.Is.Anything)).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = false });
            mockCrmTasks.Expect(c => c.GetContactRef(Arg<Guid>.Is.Anything)).Return("C100233");

            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var transReports = transferTasks.TransferData();


            var contactReport = transReports.FirstOrDefault(t => t.TableName == "Contact");
            Assert.IsNotNull(contactReport);
            Assert.AreEqual(1, contactReport.TotalRows);
            Assert.AreEqual(1, contactReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, contactReport.Failed);

            IList<object[]> argumentsSentToSetLinkID =
             mockWebService.GetArgumentsForCallsMadeOn(x => x.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything));
            Assert.IsTrue(argumentsSentToSetLinkID.Count() > 0, "expecting some calls to be made to SetLinkID...");
            // where [0] = first call and [0] = first argument ...

            var passedRequest = (SetLinkIDAndSyncStatusRequest)argumentsSentToSetLinkID[0][0];
            var passedDs = passedRequest.dsReferrals;
            
            Assert.IsNotNull(passedDs);
            Assert.AreEqual(returnGuid.ToString(), passedDs.Contact[0].TargetContactID);
            
            mockWebService.VerifyAllExpectations();
            mockCrmTasks.VerifyAllExpectations();


        }
        */

            /*
        [TestMethod]
        public void CanSkipContactDeletion()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            var resp = new GetDetailsResponse("", TestUtils.MakeReferralsDataSetTwoContactsOneDeleted());
            mockWebService.Expect(w => w.GetDetails(Arg<GetDetailsRequest>.Is.Anything)).Return(resp);
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));

            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();

            mockCrmTasks.Expect(c => c.SaveContact(Arg<Referrals.ContactRow>.Is.Anything ,
                Arg<Referrals.ContactIndividualRow>.Is.Anything)).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = false });
            mockCrmTasks.Expect(c => c.GetContactRef(Arg<Guid>.Is.Anything)).Return("C100233");

            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var transReports = transferTasks.TransferData();


            var contactReport = transReports.FirstOrDefault(t => t.TableName == "Contact");
            Assert.IsNotNull(contactReport);
            Assert.AreEqual(2, contactReport.TotalRows);
            Assert.AreEqual(1, contactReport.Succeeded, "expecting one row to succeed");
            Assert.AreEqual(0, contactReport.Failed);
            Assert.AreEqual(1, contactReport.Skipped, "expecting one skip");

            IList<object[]> argumentsSentToSaveContact =
             mockCrmTasks.GetArgumentsForCallsMadeOn(c => c.SaveContact(Arg<Referrals.ContactRow>.Is.Anything ,
                Arg<Referrals.ContactIndividualRow>.Is.Anything));
            Assert.IsTrue(argumentsSentToSaveContact.Count() == 1, "expecting one calls to be made to SaveContact...");
            // where [0] = first call and [0] = first argument ...

            var passedRow = (Referrals.ContactRow)argumentsSentToSaveContact[0][0];
            

            Assert.IsNotNull(passedRow);
            Assert.AreEqual("345346", passedRow.SourceContactID);

            mockWebService.VerifyAllExpectations();
            mockCrmTasks.VerifyAllExpectations();


        }
        */

            /*
        [TestMethod]
        public void CanDeleteCaseMock()
        {
            
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
           
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));
            
            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();

            mockCrmTasks.Expect(c => c.DeleteEntityBasedOnTable(Arg<string>.Is.Equal("Case"), Arg<Guid>.Is.Equal(new Guid("32a35219-7d45-4fd0-ad46-08118517f114"))));
            

            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustCaseDeletion();
            var deleteReport = transferTasks.ProcessDeletions(ds.Case);


            
            Assert.IsNotNull(deleteReport);
            Assert.AreEqual(1, deleteReport.TotalRows);
            Assert.AreEqual(1, deleteReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, deleteReport.Failed);

            IList<object[]> argumentsSentToSetLinkID =
             mockWebService.GetArgumentsForCallsMadeOn(x => x.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything));
            Assert.IsTrue(argumentsSentToSetLinkID.Count() > 0, "expecting some calls to be made to SetLinkID...");
            // where [0] = first call and [0] = first argument ...

            var passedRequest = (SetLinkIDAndSyncStatusRequest)argumentsSentToSetLinkID[0][0];
            var passedDs = passedRequest.dsReferrals;

            Assert.IsNotNull(passedDs);
            Assert.AreEqual("32a35219-7d45-4fd0-ad46-08118517f114", passedDs.Case[0].TargetCaseID);

            mockWebService.VerifyAllExpectations();
            mockCrmTasks.VerifyAllExpectations();


        }
        */

            /*
        [TestMethod]
        public void CanProcessPresentingProbDeletion()
        {
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
           
            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            var pretendCaseId = new Guid("d2de6e61-399d-4ccd-87c0-627cd743767a");
            mockCrmTasks.Expect(p=>p.GetCaseCrmId(Arg<string>.Is.Equal("L345345.1"))).Return( pretendCaseId);
            mockCrmTasks.Expect(p=>p.DeleteCasePresentingProblem(Arg<Guid>.Is.Equal(pretendCaseId), Arg<Guid>.Is.Equal(new Guid("1e9691d9-71cb-49e3-8bbe-e9b4bf83450d")) ));

            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();

            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustPresProbDeletion();
            var ppRow = ds.CasePresentingProblem.Rows[0];
            var idToDelete = new Guid(ppRow["TargetCasePresentingProblemID"].ToString());
            transferTasks.ProcessDeletion_PresentingProblem(ppRow["SyncID"].ToString(), ppRow, idToDelete);


            


            mockCrmTasks.VerifyAllExpectations();


        }
        */

            /*

        [TestMethod]
        public void CanTransferContactWhenPrescientRefInCrm()
        {
            // test whether Contact can be properly saved (as an update) if it has 
            // SourceContactID that is known in CRM but not TargetContactID

            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));
            

            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            mockCrmTasks.Expect(c => c.GetContactCrmId(Arg<string>.Is.Equal("345346"))).Return(returnGuid);
           

            string passedId = null;
            mockCrmTasks.Expect(c => c.SaveContact(Arg<Referrals.ContactRow>.Is.Anything, Arg<Referrals.ContactIndividualRow>.Is.Anything)).WhenCalled(
                invocation => passedId = ((Referrals.ContactRow)invocation.Arguments[0]).TargetContactID).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = true });

            mockCrmTasks.Expect(c => c.GetContactRef(Arg<Guid>.Is.Anything)).Return("C405323");

            
            


            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustContact();

            var transReport = transferTasks.Transfer_Contacts(ds.Contact, ds.ContactIndividual, ds.ContactOrganisation);


            
            Assert.IsNotNull(transReport);
            Assert.AreEqual(1, transReport.TotalRows);
            Assert.AreEqual(1, transReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, transReport.Failed);

            Assert.AreEqual(returnGuid.ToString(), passedId);

            
            mockCrmTasks.VerifyAllExpectations();


        }*/


            /*
        [TestMethod]
        public void CanTransferAddressWhenPrescientRefInCrm()
        {
            // test whether Address can be properly saved (as an update) of it has 
            // SourceAddressID but not TargetAddressODID

            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));


            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            mockCrmTasks.Expect(c => c.GetAddressCrmId(Arg<string>.Is.Equal("765776"))).Return(returnGuid);
            

            string passedId = null;
            mockCrmTasks.Expect(c => c.SaveAddress(Arg<Referrals.AddressRow>.Is.Anything)).WhenCalled(
               invocation => passedId = ((Referrals.AddressRow)invocation.Arguments[0]).TargetAddressID).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = true });
           
           


            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustAddress();

            var transReport = transferTasks.Transfer_Addresses(ds.Address);

            


            Assert.IsNotNull(transReport);
            Assert.AreEqual(1, transReport.TotalRows);
            Assert.AreEqual(1, transReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, transReport.Failed);

            Assert.AreEqual(returnGuid.ToString(), passedId);


            mockCrmTasks.VerifyAllExpectations();


        }
        */


            /*
        [TestMethod]
        public void CanTransferCaseWhenPrescientRefInCrm()
        {
            // test whether Case can be properly saved (as an update) of it has 
            // SourceCaseID but not TargeCaseID

            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));


            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            mockCrmTasks.Expect(c => c.GetCaseCrmId(Arg<string>.Is.Equal("A345582/1"))).Return(returnGuid);


            string passedId = null;
            mockCrmTasks.Expect(c => c.SaveCase(Arg<Referrals.CaseRow>.Is.Anything, Arg<Guid>.Is.Anything)).WhenCalled(
               invocation => passedId = ((Referrals.CaseRow)invocation.Arguments[0]).TargetCaseID).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = true });

            mockCrmTasks.Expect(c => c.GetCaseRef(Arg<Guid>.Is.Anything)).Return("C345582.1");


            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustCaseWithContact();

            var transReport = transferTasks.Transfer_Case(ds.Case);




            Assert.IsNotNull(transReport);
            Assert.AreEqual(1, transReport.TotalRows);
            Assert.AreEqual(1, transReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, transReport.Failed);

            Assert.AreEqual(returnGuid.ToString(), passedId);


            mockCrmTasks.VerifyAllExpectations();


        }*/


            /*
        [TestMethod]
        public void CanTransferAlertWhenPrescientRefInCrm()
        {
            
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));


            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            mockCrmTasks.Expect(c => c.GetContactAlertCrmId(Arg<string>.Is.Equal("749634"))).Return(returnGuid);


            string passedId = null;
            mockCrmTasks.Expect(c => c.SaveContactAlert(Arg<Referrals.ContactAlertRow>.Is.Anything, Arg<Guid>.Is.Anything)).WhenCalled(
               invocation => passedId = ((Referrals.ContactAlertRow)invocation.Arguments[0]).TargetContactAlertID).Return(new CrmTasks.CrmSaveResult() { CrmId = returnGuid, IsUpdate = true });

            


            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();
            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory);

            var ds = TestUtils.MakeReferralsDataSetJustAlertWithContact();

            var transReport = transferTasks.Transfer_ContactAlerts(ds.ContactAlert);




            Assert.IsNotNull(transReport);
            Assert.AreEqual(1, transReport.TotalRows);
            Assert.AreEqual(1, transReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, transReport.Failed);

            Assert.AreEqual(returnGuid.ToString(), passedId);


            mockCrmTasks.VerifyAllExpectations();


        }
        */

            /*

        [TestMethod]
        public void CanTransferContactIfOnIgnoreList()
        {
            Guid returnGuid = new Guid("b2d19099-fe8c-e211-8a6b-3c4a92dbdc39");
            var mockWebService = MockRepository.GenerateMock<CRMIntegration>();
            var resp = new GetDetailsResponse("", TestUtils.MakeReferralsDataSetJustContact());
            mockWebService.Expect(w => w.GetDetails(Arg<GetDetailsRequest>.Is.Anything)).Return(resp);
            mockWebService.Expect(w => w.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything)).Return(new SetLinkIDAndSyncStatusResponse(""));

            var mockCrmTasks = MockRepository.GenerateMock<CrmTasks>();
            mockCrmTasks.Stub(c => c.SaveContact(null, null)).IgnoreArguments().Throw(new ApplicationException("this save will fail, dont call this"));
            var mockHistory = MockRepository.GenerateMock<HistoryWriter>();

            var mockIgnoreList = MockRepository.GenerateMock<IgnoreList>();
            mockIgnoreList.Expect(r => r.ShouldIgnore("Contact", "345346")).Return(true);

            var transferTasks = new PrescientToCrmTransferTasks(mockCrmTasks, mockWebService, mockHistory, mockIgnoreList);

            var transReports = transferTasks.TransferData();


            var contactReport = transReports.FirstOrDefault(t => t.TableName == "Contact");
            Assert.IsNotNull(contactReport);
            Assert.AreEqual(1, contactReport.TotalRows);
            Assert.AreEqual(1, contactReport.Succeeded, "expecting row to succeed");
            Assert.AreEqual(0, contactReport.Failed);

            IList<object[]> argumentsSentToSetLinkID =
             mockWebService.GetArgumentsForCallsMadeOn(x => x.SetLinkIDAndSyncStatus(Arg<SetLinkIDAndSyncStatusRequest>.Is.Anything));
            Assert.IsTrue(argumentsSentToSetLinkID.Count() > 0, "expecting some calls to be made to SetLinkID...");
            // where [0] = first call and [0] = first argument ...

            var passedRequest = (SetLinkIDAndSyncStatusRequest)argumentsSentToSetLinkID[0][0];
            var passedDs = passedRequest.dsReferrals;

            Assert.IsNotNull(passedDs);
            Assert.AreEqual("", passedDs.Contact[0].TargetContactID);

            mockWebService.VerifyAllExpectations();
            mockCrmTasks.VerifyAllExpectations();
            mockIgnoreList.VerifyAllExpectations();


        }
        */



    }
}
