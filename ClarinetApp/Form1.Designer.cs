﻿namespace ClarinetApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uxPrescientDB = new System.Windows.Forms.Label();
            this.uxCrmInstance = new System.Windows.Forms.Label();
            this.uxGapSosService = new System.Windows.Forms.Label();
            this.uxClarinetDB = new System.Windows.Forms.Label();
            this.uxStage1Button = new System.Windows.Forms.Button();
            this.uxStage2Button = new System.Windows.Forms.Button();
            this.uxStage3Button = new System.Windows.Forms.Button();
            this.uxProgress = new System.Windows.Forms.TextBox();
            this.uxCancel = new System.Windows.Forms.Button();
            this.uxThrobber = new System.Windows.Forms.PictureBox();
            this.uxAllStagesButton = new System.Windows.Forms.Button();
            this.uxStage4Button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.uxThrobber)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PrescientDB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ClarinetDB";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "CRM Instance";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Gap SOS Service";
            // 
            // uxPrescientDB
            // 
            this.uxPrescientDB.AutoSize = true;
            this.uxPrescientDB.Location = new System.Drawing.Point(131, 23);
            this.uxPrescientDB.Name = "uxPrescientDB";
            this.uxPrescientDB.Size = new System.Drawing.Size(28, 13);
            this.uxPrescientDB.TabIndex = 4;
            this.uxPrescientDB.Text = "(tbc)";
            // 
            // uxCrmInstance
            // 
            this.uxCrmInstance.AutoSize = true;
            this.uxCrmInstance.Location = new System.Drawing.Point(131, 48);
            this.uxCrmInstance.Name = "uxCrmInstance";
            this.uxCrmInstance.Size = new System.Drawing.Size(28, 13);
            this.uxCrmInstance.TabIndex = 5;
            this.uxCrmInstance.Text = "(tbc)";
            // 
            // uxGapSosService
            // 
            this.uxGapSosService.AutoSize = true;
            this.uxGapSosService.Location = new System.Drawing.Point(131, 75);
            this.uxGapSosService.Name = "uxGapSosService";
            this.uxGapSosService.Size = new System.Drawing.Size(28, 13);
            this.uxGapSosService.TabIndex = 6;
            this.uxGapSosService.Text = "(tbc)";
            // 
            // uxClarinetDB
            // 
            this.uxClarinetDB.AutoSize = true;
            this.uxClarinetDB.Location = new System.Drawing.Point(131, 101);
            this.uxClarinetDB.Name = "uxClarinetDB";
            this.uxClarinetDB.Size = new System.Drawing.Size(28, 13);
            this.uxClarinetDB.TabIndex = 7;
            this.uxClarinetDB.Text = "(tbc)";
            // 
            // uxStage1Button
            // 
            this.uxStage1Button.Location = new System.Drawing.Point(16, 132);
            this.uxStage1Button.Name = "uxStage1Button";
            this.uxStage1Button.Size = new System.Drawing.Size(108, 25);
            this.uxStage1Button.TabIndex = 8;
            this.uxStage1Button.Text = "Stage1 Contacts";
            this.uxStage1Button.UseVisualStyleBackColor = true;
            this.uxStage1Button.Click += new System.EventHandler(this.uxStage1Button_Click);
            // 
            // uxStage2Button
            // 
            this.uxStage2Button.Location = new System.Drawing.Point(130, 132);
            this.uxStage2Button.Name = "uxStage2Button";
            this.uxStage2Button.Size = new System.Drawing.Size(108, 25);
            this.uxStage2Button.TabIndex = 9;
            this.uxStage2Button.Text = "Stage2 Cases";
            this.uxStage2Button.UseVisualStyleBackColor = true;
            this.uxStage2Button.Click += new System.EventHandler(this.uxStage2Button_Click);
            // 
            // uxStage3Button
            // 
            this.uxStage3Button.Location = new System.Drawing.Point(244, 132);
            this.uxStage3Button.Name = "uxStage3Button";
            this.uxStage3Button.Size = new System.Drawing.Size(108, 25);
            this.uxStage3Button.TabIndex = 10;
            this.uxStage3Button.Text = "Stage3 Time";
            this.uxStage3Button.UseVisualStyleBackColor = true;
            this.uxStage3Button.Click += new System.EventHandler(this.uxStage3Button_Click);
            // 
            // uxProgress
            // 
            this.uxProgress.Location = new System.Drawing.Point(15, 191);
            this.uxProgress.Multiline = true;
            this.uxProgress.Name = "uxProgress";
            this.uxProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.uxProgress.Size = new System.Drawing.Size(527, 225);
            this.uxProgress.TabIndex = 11;
            // 
            // uxCancel
            // 
            this.uxCancel.Location = new System.Drawing.Point(454, 162);
            this.uxCancel.Name = "uxCancel";
            this.uxCancel.Size = new System.Drawing.Size(88, 23);
            this.uxCancel.TabIndex = 12;
            this.uxCancel.Text = "Cancel";
            this.uxCancel.UseVisualStyleBackColor = true;
            this.uxCancel.Click += new System.EventHandler(this.uxCancel_Click);
            // 
            // uxThrobber
            // 
            this.uxThrobber.Image = global::ClarinetApp.Properties.Resources.loader;
            this.uxThrobber.Location = new System.Drawing.Point(422, 162);
            this.uxThrobber.Name = "uxThrobber";
            this.uxThrobber.Size = new System.Drawing.Size(26, 23);
            this.uxThrobber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.uxThrobber.TabIndex = 13;
            this.uxThrobber.TabStop = false;
            // 
            // uxAllStagesButton
            // 
            this.uxAllStagesButton.Location = new System.Drawing.Point(15, 160);
            this.uxAllStagesButton.Name = "uxAllStagesButton";
            this.uxAllStagesButton.Size = new System.Drawing.Size(108, 25);
            this.uxAllStagesButton.TabIndex = 14;
            this.uxAllStagesButton.Text = "Run Stages 1,2,3";
            this.uxAllStagesButton.UseVisualStyleBackColor = true;
            this.uxAllStagesButton.Click += new System.EventHandler(this.uxAllStagesButton_Click);
            // 
            // uxStage4Button
            // 
            this.uxStage4Button.Location = new System.Drawing.Point(358, 132);
            this.uxStage4Button.Name = "uxStage4Button";
            this.uxStage4Button.Size = new System.Drawing.Size(125, 25);
            this.uxStage4Button.TabIndex = 15;
            this.uxStage4Button.Text = "Stage4 Sync to SOS";
            this.uxStage4Button.UseVisualStyleBackColor = true;
            this.uxStage4Button.Click += new System.EventHandler(this.uxStage4Button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 438);
            this.Controls.Add(this.uxStage4Button);
            this.Controls.Add(this.uxAllStagesButton);
            this.Controls.Add(this.uxThrobber);
            this.Controls.Add(this.uxCancel);
            this.Controls.Add(this.uxProgress);
            this.Controls.Add(this.uxStage3Button);
            this.Controls.Add(this.uxStage2Button);
            this.Controls.Add(this.uxStage1Button);
            this.Controls.Add(this.uxClarinetDB);
            this.Controls.Add(this.uxGapSosService);
            this.Controls.Add(this.uxCrmInstance);
            this.Controls.Add(this.uxPrescientDB);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Clarinet (CLA Migration)";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uxThrobber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label uxPrescientDB;
        private System.Windows.Forms.Label uxCrmInstance;
        private System.Windows.Forms.Label uxGapSosService;
        private System.Windows.Forms.Label uxClarinetDB;
        private System.Windows.Forms.Button uxStage1Button;
        private System.Windows.Forms.Button uxStage2Button;
        private System.Windows.Forms.Button uxStage3Button;
        private System.Windows.Forms.TextBox uxProgress;
        private System.Windows.Forms.Button uxCancel;
        private System.Windows.Forms.PictureBox uxThrobber;
        private System.Windows.Forms.Button uxAllStagesButton;
        private System.Windows.Forms.Button uxStage4Button;
    }
}

