﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClarinetLib;

namespace ClarinetApp
{
    public partial class Form1 : Form
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private BackgroundWorker bWorker;
        
        // controls what background worker will do
        private int runMode = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            logger.DebugFormat("Form_Load called");
            // show config
            ConfigFormatter cf = new ConfigFormatter();
            uxPrescientDB.Text = cf.GetFriendlyPrescientConfig();
            uxCrmInstance.Text = cf.GetFriendlyCrmConfig();
            uxGapSosService.Text = cf.GetFriendlyGapSosConfig();
            uxClarinetDB.Text = cf.GetFriendlyClarinetDbConfig();

            // setup background worker
            bWorker = new BackgroundWorker();
            bWorker.DoWork += BWorker_DoWork;
            bWorker.ProgressChanged += BWorker_ProgressChanged;
            bWorker.RunWorkerCompleted += BWorker_RunWorkerCompleted;
            bWorker.WorkerReportsProgress = true;
            bWorker.WorkerSupportsCancellation = true;

            ScreenNotRunning();
        }

        private void BWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                LogMessage("Process cancelled.");
            }
            else if (e.Error != null)
            {
                logger.Error("Error caught in RunWorkerCompleted", e.Error);
                LogMessage("Error: " + e.Error.Message);

            }
            else
            {
                LogMessage("Process completed");
            }
            ScreenNotRunning();
        }

        private void BWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            String message = (String)e.UserState;
            if (message != null)
                LogMessage(message);
        }

        private void BWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.DebugFormat("BWorker_DoWork running with runMode {0}", runMode);
            bWorker.ReportProgress(50, "Starting (see Log.txt for further details) ...");
            var tasks = new PrescientToCrmTransferTasks();

            if (runMode == 1)
                Stage1Loop(e, tasks);
            else if (runMode == 2)
                Stage2Loop(e, tasks);
            else if (runMode == 3)
                Stage3Loop(e, tasks);
            else if (runMode == 4)
                Stage4Loop(e, tasks);
            // mode 10 does stage 1,2,3 but not 4
            else if (runMode == 10)
            {
                Stage1Loop(e, tasks);
                if (!e.Cancel)
                    Stage2Loop(e, tasks);
                if (!e.Cancel)
                    Stage3Loop(e, tasks);
                
            }
        }

        private void Stage1Loop(DoWorkEventArgs e, PrescientToCrmTransferTasks tasks)
        {
            bWorker.ReportProgress(50, "Running Stage 1 (Contacts) in batches ...");
            bool allDone = false;
            int grandTotal = 0;
            int batchCount = 0;
            while (!allDone)
            {
                var res = tasks.Stage1_TransferContactBatch();
                String resSummary = String.Format("Table {0} Total {1} Succeeded {2} Failed {3} NotFoundInDestination {4} Skipped {5}",
                    res.TableName, res.TotalRows, res.Succeeded, res.Failed, res.NotFoundInDestination, res.Skipped);
                bWorker.ReportProgress(50, resSummary);

                batchCount += 1;
                grandTotal += res.TotalRows;
                if (batchCount % 5 == 0)
                    bWorker.ReportProgress(50, string.Format("Contact Batches so far {0} Total Contact rows fetched so far {1}",
                        batchCount, grandTotal));

                if (res.TotalRows == 0)
                    allDone = true;

                if (bWorker.CancellationPending)
                {
                    bWorker.ReportProgress(50, "Cancelling...");
                    e.Cancel = true;
                    allDone = true;
                }
            }
        }

        private void Stage2Loop(DoWorkEventArgs e, PrescientToCrmTransferTasks tasks)
        {
            bWorker.ReportProgress(50, "Running Stage 2 (Cases) in batches ...");
            bool allDone = false;
            int grandTotal = 0;
            int batchCount = 0;
            while (!allDone)
            {
                var res = tasks.Stage2_TransferCaseBatch();
                String resSummary = String.Format("Table {0} Total {1} Succeeded {2} Failed {3} NotFoundInDestination {4} Skipped {5}",
                    res.TableName, res.TotalRows, res.Succeeded, res.Failed, res.NotFoundInDestination, res.Skipped);
                bWorker.ReportProgress(50, resSummary);

                batchCount += 1;
                grandTotal += res.TotalRows;
                if (batchCount % 5 == 0)
                    bWorker.ReportProgress(50, string.Format("Case Batches so far {0} Total Case rows fetched so far {1}",
                        batchCount, grandTotal));

                if (res.TotalRows == 0)
                    allDone = true;

                if (bWorker.CancellationPending)
                {
                    bWorker.ReportProgress(50, "Cancelling...");
                    e.Cancel = true;
                    allDone = true;
                }
            }
        }

        private void Stage3Loop(DoWorkEventArgs e, PrescientToCrmTransferTasks tasks)
        {
            bWorker.ReportProgress(50, "Running Stage 3 (Time) in batches ...");
            bool allDone = false;
            int grandTotal = 0;
            int batchCount = 0;
            while (!allDone)
            {
                var res = tasks.Stage3_TransferTimeBatch();
                String resSummary = String.Format("Table {0} Total {1} Succeeded {2} Failed {3} NotFoundInDestination {4} Skipped {5}",
                    res.TableName, res.TotalRows, res.Succeeded, res.Failed, res.NotFoundInDestination, res.Skipped);
                bWorker.ReportProgress(50, resSummary);

                batchCount += 1;
                grandTotal += res.TotalRows;
                if (batchCount % 5 == 0)
                    bWorker.ReportProgress(50, string.Format("Time Batches so far {0} Total Time rows fetched so far {1}",
                        batchCount, grandTotal));


                if (res.TotalRows == 0)
                    allDone = true;

                if (bWorker.CancellationPending)
                {
                    bWorker.ReportProgress(50, "Cancelling...");
                    e.Cancel = true;
                    allDone = true;
                }
            }
        }

        private void Stage4Loop(DoWorkEventArgs e, PrescientToCrmTransferTasks tasks)
        {
            bWorker.ReportProgress(50, "Running Stage 4 (Sync to SOS) in batches ...");
            bool allDone = false;
            int grandTotal = 0;
            int batchCount = 0;
            while (!allDone)
            {
                var res = tasks.Stage4_SyncToSosBatch();
                String resSummary = String.Format("Table {0} Total {1} Succeeded {2} Failed {3} NotFoundInDestination {4} Skipped {5}",
                    res.TableName, res.TotalRows, res.Succeeded, res.Failed, res.NotFoundInDestination, res.Skipped);
                bWorker.ReportProgress(50, resSummary);

                batchCount += 1;
                grandTotal += res.TotalRows;
                if (batchCount % 5 == 0)
                    bWorker.ReportProgress(50, string.Format("SOS Batches so far {0} Total rows fetched so far {1}",
                        batchCount, grandTotal));


                if (res.TotalRows == 0)
                    allDone = true;

                if (bWorker.CancellationPending)
                {
                    bWorker.ReportProgress(50, "Cancelling...");
                    e.Cancel = true;
                    allDone = true;
                }
            }
        }


        public void LogMessage(String m)
        {
            String time = DateTime.Now.ToString("HH:mm:ss");
            uxProgress.AppendText(time + " " + m + Environment.NewLine);
            logger.DebugFormat("Writing message to UI: {0}", m);
        }

        private void uxStage1Button_Click(object sender, EventArgs e)
        {
            ScreenRunning();
            runMode = 1;
            bWorker.RunWorkerAsync();
        }

        public void ScreenRunning()
        {
            uxStage1Button.Enabled = false;
            uxStage2Button.Enabled = false;
            uxStage3Button.Enabled = false;
            uxStage4Button.Enabled = false;
            uxAllStagesButton.Enabled = false;
            uxCancel.Enabled = true;
            uxThrobber.Visible = true;
        }

        public void ScreenNotRunning()
        {
            uxStage1Button.Enabled = true;
            uxStage2Button.Enabled = true;
            uxStage3Button.Enabled = true;
            uxStage4Button.Enabled = true;
            uxAllStagesButton.Enabled = true;
            uxCancel.Enabled = false;
            uxThrobber.Visible = false;
        }

        private void uxCancel_Click(object sender, EventArgs e)
        {
            if (bWorker.IsBusy)
            {
                LogMessage("Sent cancel message, waiting for batch to finish ...");
                uxCancel.Enabled = false;
                bWorker.CancelAsync();
            }
        }

        private void uxStage2Button_Click(object sender, EventArgs e)
        {
            ScreenRunning();
            runMode = 2;
            bWorker.RunWorkerAsync();
        }

        private void uxStage3Button_Click(object sender, EventArgs e)
        {
            ScreenRunning();
            runMode = 3;
            bWorker.RunWorkerAsync();
        }

        private void uxAllStagesButton_Click(object sender, EventArgs e)
        {
            ScreenRunning();
            runMode = 10;
            bWorker.RunWorkerAsync();
        }

        private void uxStage4Button_Click(object sender, EventArgs e)
        {
            ScreenRunning();
            runMode = 4;
            bWorker.RunWorkerAsync();
        }
    }
}
