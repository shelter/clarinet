﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using ClarinetLib;
//using Shelter.Util.Concurrency;
using System.Reflection;
using System.Configuration;

namespace ClarinetConsole
{
    public class Program
    {

        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static int Main(string[] args)
        {
            logger.DebugFormat("ClarinetConsole starting");
            Console.WriteLine("ClarinetConsole starting");
            Console.WriteLine("See Log.txt or Clarinet database for progress info");

            int returnCode;

            try
            {
                bool wasAlreadyRegistered = false;
                if (wasAlreadyRegistered)
                {
                    logger.Debug("The application is already running. Exiting now.");
                    returnCode = 1;
                }
                else
                {
                    var tasks = new PrescientToCrmTransferTasks();
                    // get max records
                    int maxRecords = int.Parse(ConfigurationManager.AppSettings["GetDetails.MaxRecords"]);

                    // TODO : these methods do batches of 100 so we need to
                    //        keep calling each of these until the res object has TotalRows  0
                    var res1 = tasks.Stage1_TransferContactBatch();
                    var res2 = tasks.Stage2_TransferCaseBatch();
                    var res3 = tasks.Stage3_TransferTimeBatch();

                    logger.DebugFormat("ClarinetConsole ending");
                    Console.WriteLine("ClarinetConsole ending");

                    returnCode = 0;
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unhandled exception caught by Main()", ex);
                Console.WriteLine(string.Format("Unhandled exception caught by Main(): {0}",ex.Message));
                returnCode = 1;
            }
          

            return returnCode;
        }
    }
}
