﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ClarinetLib.CrmConnection;

namespace ClarinetLib
{
    public class CrmPicklistTasks
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CrmConnectionCache _connectionPool;
        private CacheWrapper _cache;

        public CrmPicklistTasks() : this(new CrmConnectionCache(), new CacheWrapper()) { }

        public CrmPicklistTasks(CrmConnectionCache connPool, CacheWrapper cache)
        {
            _connectionPool = connPool;
            _cache = cache;
        }

        public virtual List<CrmConnectionRole> GetConnectionRoles()
        {
            if (!_cache.Contains("connectionroles"))
            {
                logger.DebugFormat("GetConnectionRoles: not in cache so fetching from crm");
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = @"<fetch mapping='logical' >
                 <entity name='connectionrole'>
                    <attribute name='connectionroleid' />
                    <attribute name='name' />
                    <attribute name='category' />
                  </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));

                var ret = new List<CrmConnectionRole>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmConnectionRole()
                    {
                        ConnectionRoleId = (Guid)crLoop["connectionroleid"],
                        Name = (string)crLoop["name"],
                        Category = ((OptionSetValue)crLoop["category"]).Value
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} connection roles converted to list", ret.Count());
                _cache.Add("connectionroles", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetConnectionRoles: returning from cache");
                return (List<CrmConnectionRole>)_cache.Get("connectionroles");
            }

        }

        /// <summary>
        /// returns dictionary mapping Prescient RoleCodes to CRM ConnectionRole Guids
        /// </summary>
        /// <returns></returns>
        public virtual Dictionary<string, Guid> GetConnectionRolePrescientMapping()
        {
            // this isn't stored anywhere currently so have to hard code
            // guids are same in live and dev
            // TODO: store this mapping properly in CRM or Prescient somewhere

            var ret = new Dictionary<string, Guid>();

            ret.Add("PRICLI", new Guid("06e32c7d-df80-e211-b5c5-d4856451cc85")); // main client
            ret.Add("REL24", new Guid("e10e5235-e080-e211-b5c5-d4856451cc85")); // other relative
            ret.Add("REL4", new Guid("9ce42084-e080-e211-b5c5-d4856451cc85")); // brother
            ret.Add("REL8", new Guid("20a906ec-e080-e211-b5c5-d4856451cc85")); // carer
            ret.Add("REL14", new Guid("cb2e90f1-df80-e211-b5c5-d4856451cc85")); // daughter
            ret.Add("REL18", new Guid("dd540605-e080-e211-b5c5-d4856451cc85")); // friend
            ret.Add("REL40", new Guid("a28c798d-e080-e211-b5c5-d4856451cc85")); // grandchild
            ret.Add("REL46", new Guid("48633cb6-e080-e211-b5c5-d4856451cc85")); // grandparent
            ret.Add("REL12", new Guid("a348bed3-df80-e211-b5c5-d4856451cc85")); // landlord
            ret.Add("REL28", new Guid("0c104560-e080-e211-b5c5-d4856451cc85")); // sister
            ret.Add("REL36", new Guid("8ffcd36b-e080-e211-b5c5-d4856451cc85")); // son
            ret.Add("REL26", new Guid("d6103d3c-e080-e211-b5c5-d4856451cc85")); // partner
            ret.Add("REL38", new Guid("89522ce1-e080-e211-b5c5-d4856451cc85")); // spouse/civil partner (remapped from civil partner)
            ret.Add("REL52", new Guid("43e0d6cb-e080-e211-b5c5-d4856451cc85")); // former partner (renamed)
            ret.Add("REL16", new Guid("27a0a0fe-df80-e211-b5c5-d4856451cc85")); // parent (renaned from father)
            ret.Add("REL20", new Guid("27a0a0fe-df80-e211-b5c5-d4856451cc85")); // parent (remapped from mother)
            ret.Add("REL50", new Guid("5da2b7be-e080-e211-b5c5-d4856451cc85")); // former spouse/civil partner (renamed)
            ret.Add("REL60", new Guid("41a662d6-e080-e211-b5c5-d4856451cc85")); // house/flatmate (renamed)
            ret.Add("REL44", new Guid("e10e5235-e080-e211-b5c5-d4856451cc85")); // other relative (remapped from nephew)
            ret.Add("REL42", new Guid("e10e5235-e080-e211-b5c5-d4856451cc85")); // other relative (remapped from neice)
            ret.Add("REL70", new Guid("89522ce1-e080-e211-b5c5-d4856451cc85")); // spouse/civil partner (renamed)


            ret.Add("MIGCL", new Guid("6a028add-f480-e211-b5c5-d4856451cc85")); // caselink




            return ret;
        }

        public virtual CrmCaseContactConnectionRolePair GetCaseContactConnectionRolePair(string prescientRoleCode)
        {
            var ret = new CrmCaseContactConnectionRolePair();
            ret.ContactConnectionRole = null;
            ret.CaseConnectionRole = null;

            var mapping = GetConnectionRolePrescientMapping();

            if (mapping.ContainsKey(prescientRoleCode))
            {
                ret.ContactConnectionRole = mapping[prescientRoleCode];

                // check thats a valid connection role
                var roleMatch = GetConnectionRoles().FirstOrDefault(r => r.ConnectionRoleId == ret.ContactConnectionRole);
                if (roleMatch == null)
                    throw new ApplicationException(string.Format("GetCaseContactConnectionRolePair: RoleCode {0} mapped to ConnectionRole {1} but that connectionrole does not exist in CRM",
                        prescientRoleCode, ret.ContactConnectionRole));
            }


            if (ret.ContactConnectionRole.HasValue)
            {
                // work out the corresponding role for other side
                if (prescientRoleCode == "MIGCL")
                    // if its a generic case link then its the same on both sides
                    ret.CaseConnectionRole = ret.ContactConnectionRole.Value;
                else
                {
                    // otherwise its the connectionrole called "Household:Case"
                    var householdCaseRole = GetConnectionRoles().FirstOrDefault(r => r.Name == "Household: Case");
                    if (householdCaseRole != null)
                        ret.CaseConnectionRole = householdCaseRole.ConnectionRoleId;
                    else
                        throw new ApplicationException(string.Format("SaveContactCaseLink: could not find ConnectionRole 'Household: Case'"));

                }

            }

            return ret;


        }

        public virtual List<CrmCaseType> GetCaseTypes()
        {
            if (!_cache.Contains("casetypes"))
            {
                logger.DebugFormat("GetCaseTypes: not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                // gap_fixedfeecodesos added for Clarinet
                string fetch = @"<fetch mapping='logical' >
                 <entity name='gap_mattertype'>
                    <attribute name='gap_mattertypeid' />
                    <attribute name='gap_mattertypename' />
                    <attribute name='gap_code' />
                    <attribute name='gap_fixedfeecodesos' />
                  </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));



                var ret = new List<CrmCaseType>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmCaseType
                    {
                        CrmId = (Guid)crLoop["gap_mattertypeid"],
                        Name = (string)crLoop["gap_mattertypename"],
                        Code = (crLoop.Attributes.Contains("gap_code")) ? (string)crLoop["gap_code"] : "",
                        FixedFeeCodeSos = (crLoop.Attributes.Contains("gap_fixedfeecodesos")) ? (string)crLoop["gap_fixedfeecodesos"] : "",
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} case types converted to list", ret.Count());
                _cache.Add("casetypes", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetCaseTypes: returning from cache");
                return (List<CrmCaseType>)_cache.Get("casetypes");
            }
        }

        public virtual List<CrmEntityForPicklist> GetContracts()
        {
            if (!_cache.Contains("contracts"))
            {
                logger.DebugFormat("GetContracts: not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = @"<fetch mapping='logical' >
                 <entity name='gap_sheltercontract'>
                    <attribute name='gap_sheltercontractid' />
                    <attribute name='gap_contractname' />
                    <attribute name='gap_code' />
                  </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));



                var ret = new List<CrmEntityForPicklist>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmEntityForPicklist
                    {
                        CrmId = (Guid)crLoop["gap_sheltercontractid"],
                        Name = (string)crLoop["gap_contractname"],
                        Code = (crLoop.Attributes.Contains("gap_code")) ? (string)crLoop["gap_code"] : ""
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} contracts converted to list", ret.Count());
                _cache.Add("contracts", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetContracts: returning from cache");
                return (List<CrmEntityForPicklist>)_cache.Get("contracts");
            }
        }


        public virtual List<CrmEntityForPicklist> GetCentres()
        {
            if (!_cache.Contains("centres"))
            {
                logger.DebugFormat("GetCentres: not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = @"<fetch mapping='logical' >
                  <entity name='account'>
                    <attribute name='name' />
                    <attribute name='accountnumber' />
                    <attribute name='accountid' />
                    <filter type='and'>
                        <condition attribute='gap_organisationtype1' operator='eq' value='810340005' />
                    </filter>
                    </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));



                var ret = new List<CrmEntityForPicklist>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmEntityForPicklist
                    {
                        CrmId = (Guid)crLoop["accountid"],
                        Name = (string)crLoop["name"],
                        Code = (crLoop.Attributes.Contains("accountnumber")) ? (string)crLoop["accountnumber"] : ""
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} centres converted to list", ret.Count());
                _cache.Add("centres", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetCentres: returning from cache");
                return (List<CrmEntityForPicklist>)_cache.Get("centres");
            }
        }

        // activity codes for time records
        public virtual List<CrmEntityForPicklist> GetActivities()
        {
            if (!_cache.Contains("activities"))
            {
                logger.DebugFormat("GetActivities: not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                // may aslo need: gap_laacode, gap_sosid ?
                string fetch = @"<fetch mapping='logical' >
                 <entity name='gap_activitytype'>
                    <attribute name='gap_activitytypeid' />
                    <attribute name='gap_name' />
                    <attribute name='gap_cicmcode' />
                  </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));



                var ret = new List<CrmEntityForPicklist>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmEntityForPicklist
                    {
                        CrmId = (Guid)crLoop["gap_activitytypeid"],
                        Name = (string)crLoop["gap_name"],
                        Code = (crLoop.Attributes.Contains("gap_cicmcode")) ? (string)crLoop["gap_cicmcode"] : ""
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} activities converted to list", ret.Count());
                _cache.Add("activities", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetActivities: returning from cache");
                return (List<CrmEntityForPicklist>)_cache.Get("activities");
            }
        }

        public virtual List<CrmEntityForPicklist> GetLAAStageReached()
        {
            return GetEntityPicklistGeneral("gap_laastagereached",
                "gap_laastagereachedid", "gap_name", "gap_code");
        }


        public virtual List<CrmEntityForPicklist> GetLAAOutcomes()
        {
            return GetEntityPicklistGeneral("gap_laaoutcome",
                "gap_laaoutcomeid", "gap_name", "gap_code");
        }

        public virtual List<CrmEntityForPicklist> GetLAAGenders()
        {
            return GetEntityPicklistGeneral("gap_laagender",
                "gap_laagenderid", "gap_name", "gap_code");
        }

        public virtual List<CrmEntityForPicklist> GetLAAEthnicities()
        {
            return GetEntityPicklistGeneral("gap_laaethnicity",
                "gap_laaethnicityid", "gap_name", "gap_code");
        }

        public virtual List<CrmEntityForPicklist> GetLAADisabilities()
        {
            return GetEntityPicklistGeneral("gap_disability",
                "gap_disabilityid", "gap_disability", "gap_code");
        }

        // match to Description field on eligibility codes
        public virtual List<CrmOptionSetWithDescription> GetEligibilityCodes()
        {
            if (!_cache.Contains("eligibilitycodes"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "bst_eligibilitycode", false);
                _cache.Add("eligibilitycodes", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("eligibilitycodes");
            }
        }



        // match to desscription field on determination
        public virtual List<CrmOptionSetWithDescription> GetDeterminationCodes()
        {
            if (!_cache.Contains("determinationcodes"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "bst_determination", false);
                _cache.Add("determinationcodes", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("determinationcodes");
            }
        }

        // match to Description field on mediacodes
        public virtual List<CrmOptionSetWithDescription> GetMediaCodes()
        {
            if (!_cache.Contains("mediacodes"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "bst_mediacode", false);

                // make all the codes lower case
                foreach (var opt in ret)
                {
                    if (opt.Description != null)
                        opt.Description = opt.Description.ToLower();
                }
                _cache.Add("mediacodes", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("mediacodes");
            }
        }

        // match to description field
        public virtual List<CrmOptionSetWithDescription> GetServiceAdaptations()
        {
            if (!_cache.Contains("serviceadaptations"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "bst_serviceadaptations", false);
                _cache.Add("serviceadaptations", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("serviceadaptations");
            }
        }

        // referral codes
        public virtual List<CrmOptionSetWithDescription> GetReferralCodes()
        {
            if (!_cache.Contains("referralcodes"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "bst_referralcodes", false);
                _cache.Add("referralcodes", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("referralcodes");
            }
        }


        public virtual List<CrmEntityForPicklist> GetChargeBands()
        {
            return GetEntityPicklistGeneral("gap_chargeband",
                "gap_chargebandid", "gap_chargebandname", "gap_code");
        }

        // household type
        public virtual List<CrmOptionSetWithDescription> GetHouseholdTypes()
        {
            if (!_cache.Contains("householdtypes"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "gap_householdtype", false);
                _cache.Add("householdtypes", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("householdtypes");
            }
        }

        public virtual List<CrmOptionSetWithDescription> GetFormOfContact()
        {
            if (!_cache.Contains("formofcontact"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "gap_formofcontact", false);
                _cache.Add("formofcontact", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("formofcontact");
            }
        }

        public virtual List<CrmOptionSetWithDescription> GetHomelessnessStatus()
        {
            if (!_cache.Contains("homelessstatus"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "gap_homelessnessstatus", false);
                _cache.Add("homelessstatus", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("homelessstatus");
            }
        }

        public virtual List<CrmOptionSetWithDescription> GetTenure()
        {
            if (!_cache.Contains("tenure"))
            {
                var ret = GetOptionSetWithDescriptionAsCode("incident", "gap_tenure", false);
                _cache.Add("tenure", ret);
                return ret;
            }
            else
            {
                return (List<CrmOptionSetWithDescription>)_cache.Get("tenure");
            }
        }



        // LAA Matter Types in CRM
        // matter types in Prescient
        // span codes in SOS
        // this maps Prescient codes to CRM codes
        public virtual Dictionary<string, string> GetLaaMatterTypeMappingLevel1()
        {

            // SOS has the mapping between these two codes but its not easy to access

            var ret = new Dictionary<string, string>();
            // level 1
            ret.Add("DIVB", "DEBT405");
            ret.Add("DMAP", "DEBT400");
            ret.Add("DMCA", "DEBT395");
            ret.Add("DMDE", "DEBT390");
            ret.Add("DNPD", "DEBT385");
            ret.Add("DORH", "DEBT410");
            ret.Add("DPDE", "DEBT380");
            ret.Add("DTOT", "DEBT415");
            ret.Add("HANT", "HOUSING0615");
            ret.Add("HBFT", "HOUSING0640");
            ret.Add("HDIS", "HOUSING0620");
            ret.Add("HHOM", "HOUSING0635");
            ret.Add("HLAN", "HOUSING165");   // awaiting confirmation
            ret.Add("HMOR", "DEBT400");  // probably
            ret.Add("HOOT", "HOUSING0655");
            ret.Add("HPOT", "HOUSING0610");
            ret.Add("HREH", "HOUSING0630");
            ret.Add("HREP", "HOUSING0625");
            ret.Add("HRNT", "HOUSING0605");
            ret.Add("HULE", "HOUSING0645");



            // level 2

            ret.Add("DCRE", "DEBT435");
            ret.Add("DIBP", "DEBT440");
            ret.Add("DMIX", "DEBT430");
            ret.Add("DORD", "DEBT445");
            ret.Add("DOTH", "DEBT450");
            ret.Add("DSCH", "DEBT420");
            ret.Add("DVAL", "DEBT425");
            ret.Add("HHAC", "HOUSING0685");
            ret.Add("HHLS", "HOUSING0700");
            //ret.Add("HLAN", "HOUSING0705"); // conflicts
            ret.Add("HNAS", "HOUSING0690");
            ret.Add("HOTH", "HOUSING0710");
            ret.Add("HOWN", "HOUSING0695"); // probably
            ret.Add("HPRI", "HOUSING0680");// probably
            ret.Add("HPUB", "HOUSING0675");// probably



            return ret;

        }

        public virtual Dictionary<string, string> GetLaaMatterTypeMappingLevel2()
        {
            var ret = GetLaaMatterTypeMappingLevel1();
            // just one thing is different
            ret["HLAN"] = "HOUSING0705";
            return ret;
        }

        // laa matter types/span codes
        // also need GetLaaMatterTypeMapping()
        public virtual List<CrmEntityForPicklist> GetLaaMatterTypes()
        {
            return GetEntityPicklistGeneral("gap_spancode1",
                "gap_spancode1id", "gap_name", "gap_code");
        }

        public virtual Dictionary<int?, string> GetTimeStatusReasonOptionSet()
        {
            if (!_cache.Contains("timestatusreasonoptionset"))
            {
                logger.DebugFormat(" GetTimeStatusReasonOptionSet: OptionSet not in cache so getting from crm...");
                int state_active = 0;
                var dict = GetOptionSetStatus("gap_timerecord", "statuscode", state_active);

                _cache.Add("timestatusreasonoptionset", dict);
                return dict;
            }
            else
            {
                logger.DebugFormat("GetTimeStatusReasonOptionSet: returning cached options");
                return (Dictionary<int?, string>)_cache.Get("timestatusreasonoptionset");
            }
        }

        // only use two time statuses so lookup every time is overkill
        // hence this returns the two used ones
        // [0] = New, [1] = Billed
        public int[] GetCommonTimeStatusReasons()
        {
            if (!_cache.Contains("timestatusreasoncommonarray"))
            {
                String unbilledStatusReason = "New";
                String billedStatusReason = "Submitted";
                int[] ret = new int[2];
                var dict = GetTimeStatusReasonOptionSet();
                var matchOpen = dict.Where(p => p.Value == unbilledStatusReason);
                if (!matchOpen.Any())
                {
                    throw new ApplicationException(string.Format("GetCommonTimeStatusReasons: Could not find {0} in Crm Time Status Reasons",
                            unbilledStatusReason));

                }
                else
                {
                    // Get the first match
                    ret[0] = matchOpen.First().Key.Value;
                }
                var matchClosed = dict.Where(p => p.Value == billedStatusReason);
                if (!matchClosed.Any())
                {
                    throw new ApplicationException(string.Format("GetCommonTimeStatusReasons: Could not find {0} in Crm Time Status Reasons",
                            billedStatusReason));

                }
                else
                {
                    // Get the first match
                    ret[1] = matchClosed.First().Key.Value;
                }
                _cache.Add("timestatusreasoncommonarray", ret);
                return ret;
            }
            else
            {
                return (int[])_cache.Get("timestatusreasoncommonarray");
            }
        }


        // general method for fetching entity-as-picklist       
        // entityName - name of the entity in CRM eg gap_mattertype
        // idField - name of the id field in CRM (a Guid) eg gap_mattertypeid
        // nameField - name of the name/desc field in CRM eg gap_mattertypename
        // codefield - name of the code field in CRM eg gap_code 
        // code can handle the values of codeField being null in the data returned by CRM
        private List<CrmEntityForPicklist> GetEntityPicklistGeneral(string entityName, string idField, string nameField, string codeField)
        {
            string cacheName = String.Format("cached_{0}", entityName);
            if (!_cache.Contains(cacheName))
            {
                logger.DebugFormat("GetEntityPicklistGeneral: {0} not in cache so fetching from CRM", entityName);
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = String.Format(@"<fetch mapping='logical' >
                 <entity name='{0}'>
                    <attribute name='{1}' />
                    <attribute name='{2}' />
                    <attribute name='{3}' />
                  </entity></fetch>  ",
                    entityName, idField, nameField, codeField);

                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));

                var ret = new List<CrmEntityForPicklist>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmEntityForPicklist
                    {
                        CrmId = (Guid)crLoop[idField],
                        Name = (string)crLoop[nameField],
                        Code = (crLoop.Attributes.Contains(codeField)) ? (string)crLoop[codeField] : ""
                    };
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} rows of {1} data converted to list", ret.Count(), entityName);
                _cache.Add(cacheName, ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetEntityPicklistGeneral: returning {0} from cache", entityName);
                return (List<CrmEntityForPicklist>)_cache.Get(cacheName);
            }
        }


        public virtual List<CrmCaseWorker> GetCaseworkers()
        {
            if (!_cache.Contains("caseworkers"))
            {
                logger.DebugFormat("GetCaseWorkers: data not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = @"<fetch mapping='logical' >
                 <entity name='gap_caseworker'>
                    <attribute name='gap_caseworkerid' />
                    <attribute name='gap_caseworkername' />
                    <attribute name='gap_prescientinitialsid' />
                  </entity></fetch>  ";


                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));



                var ret = new List<CrmCaseWorker>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmCaseWorker
                    {
                        CaseWorkerId = (Guid)crLoop["gap_caseworkerid"]
                    };
                    if (crLoop.Attributes.Contains("gap_caseworkername"))
                        ccr.CaseWorkerName = (string)crLoop["gap_caseworkername"];
                    else
                        logger.WarnFormat("CaseworkerID {0} has empty name", ccr.CaseWorkerId);
                    if (crLoop.Attributes.Contains("gap_prescientinitialsid"))
                        ccr.PrescientInitialsId = (string)crLoop["gap_prescientinitialsid"];
                    

                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} case workers converted to list", ret.Count());
                _cache.Add("caseworkers", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetCaseworkers: returning from cache");
                return (List<CrmCaseWorker>)_cache.Get("caseworkers");
            }

        }

        public virtual List<CrmOptionSetForPicklist> GetAlertTypes()
        {
            // this currently hard coded because Crm/Prescient dont have any keys in common for this
            var ret = new List<CrmOptionSetForPicklist>();

            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "SG" });
            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 3, PrescientCode = "CON" });
            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 2, PrescientCode = "WC" });
            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 4, PrescientCode = "DD" });


            return ret;

        }

        public virtual List<CrmOptionSetForPicklist> GetCaseStatuses()
        {
            // this currently hard coded because Crm/Prescient dont have any keys in common for this
            var ret = new List<CrmOptionSetForPicklist>();

            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "O" });
            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 1, PrescientCode = "R" });
            ret.Add(new CrmOptionSetForPicklist() { CrmOptionSetValue = 810340001, PrescientCode = "C" });
            
            return ret;

        }

        public virtual List<CrmPresentingProblem> GetPresentingProblems()
        {
            if (!_cache.Contains("presenting_problems"))
            {
                logger.DebugFormat("GetPresentingProblems: data not in cache so fetching from CRM");
                var serv = _connectionPool.GetCrmConnectionCached();

                string fetch = @"<fetch mapping='logical' >
                 <entity name='gap_presentingproblems'>
                    <attribute name='gap_presentingproblemsid' />
                    <attribute name='gap_problem' />
                    <attribute name='gap_code' />
                  </entity></fetch>";

                EntityCollection crmresults = serv.RetrieveMultiple(new FetchExpression(fetch));

                var ret = new List<CrmPresentingProblem>();
                foreach (Entity crLoop in crmresults.Entities)
                {
                    var ccr = new CrmPresentingProblem
                    {
                        PresentingProblemId = (Guid)crLoop["gap_presentingproblemsid"]
                    };

                    ccr.Name = (string)crLoop["gap_problem"];
                    ccr.Code = (crLoop.Attributes.Contains("gap_code")) ? (string)crLoop["gap_code"] : "";
                    
                    ret.Add(ccr);
                }
                logger.DebugFormat("{0} presenting problems converted to list", ret.Count());
                _cache.Add("presenting_problems", ret);
                return ret;
            }
            else
            {
                logger.DebugFormat("GetPresentingProblems: returning from cache");
                return (List<CrmPresentingProblem>)_cache.Get("presenting_problems");
            }

        }

        /// <summary>
        /// returns title optionset from CRM (all titles in lower case)
        /// </summary>
        public virtual Dictionary<int?, string> GetTitleOptionSet()
        {
            if (!_cache.Contains("titleoptionset"))
            {
                logger.DebugFormat("GetTitleOptionSet: Title OptionSet not in cache so getting from crm...");
                var dict = GetOptionSet("contact", "gap_title", true);
               
                _cache.Add("titleoptionset", dict);
                return dict;
            }
            else
            {
                logger.DebugFormat("GetTitleOptionSet: returning cached titles");
                return (Dictionary<int?, string>)_cache.Get("titleoptionset");
            }
        }

        public virtual Dictionary<int?, string> GetCaseStatusReasonOptionSet()
        {
            if (!_cache.Contains("casestatusreasonoptionset"))
            {
                logger.DebugFormat(" GetCaseStatusReasonOptionSet: OptionSet not in cache so getting from crm...");
                int state_active = 0;
                var dict = GetOptionSetStatus("incident", "statuscode", state_active);
               
                _cache.Add("casestatusreasonoptionset", dict);
                return dict;
            }
            else
            {
                logger.DebugFormat("GetCaseStatusReasonOptionSet: returning cached options");
                return (Dictionary<int?, string>)_cache.Get("casestatusreasonoptionset");
            }
        }

        

        private Dictionary<int?, string> GetOptionSet(string entityName, string setName, bool convertToLower)
        {
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = setName,
                RetrieveAsIfPublished = true
            };
            var serv = _connectionPool.GetCrmConnectionCached();
            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)serv.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            PicklistAttributeMetadata picklistMetadata = (PicklistAttributeMetadata)attrMetadata;

            var dict = new Dictionary<int?, string>();
            foreach (OptionMetadata optionMeta in picklistMetadata.OptionSet.Options)
            {
                string lab = optionMeta.Label.UserLocalizedLabel.Label;
                if (convertToLower)
                    lab = lab.ToLower();
                dict.Add(optionMeta.Value, lab);
            }
            logger.DebugFormat("GetOptionSet: Got {0} options for {1} {2} optionset",
                dict.Count, entityName, setName);
            
            return dict;
        }

        private List<CrmOptionSetWithDescription> GetOptionSetWithDescriptionAsCode(string entityName, string setName, bool convertToLower)
        {
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = setName,
                RetrieveAsIfPublished = true
            };
            var serv = _connectionPool.GetCrmConnectionCached();
            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)serv.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            PicklistAttributeMetadata picklistMetadata = (PicklistAttributeMetadata)attrMetadata;

            var ret = new List<CrmOptionSetWithDescription>();
            foreach (OptionMetadata optionMeta in picklistMetadata.OptionSet.Options)
            {
                string lab = optionMeta.Label.UserLocalizedLabel.Label;
                if (convertToLower)
                    lab = lab.ToLower();
                string desc = null;
                if (optionMeta.Description.UserLocalizedLabel != null) 
                    desc = optionMeta.Description.UserLocalizedLabel.Label;

                var osp = new CrmOptionSetWithDescription()
                {
                    CrmOptionSetValue = optionMeta.Value.Value,
                    Label = lab,
                    Description = desc
                };
                ret.Add(osp);
            }
            logger.DebugFormat("GetOptionSet: Got {0} options for {1} {2} optionset",
                ret.Count, entityName, setName);

            return ret;
        }

        private Dictionary<int?, string> GetOptionSetStatus(string entityName, string setName, int forRelatedState)
        {
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = setName,
                RetrieveAsIfPublished = true
            };
            var serv = _connectionPool.GetCrmConnectionCached();
            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)serv.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            StatusAttributeMetadata statusMetadata = (StatusAttributeMetadata)attrMetadata;

            var dict = new Dictionary<int?, string>();
            foreach (StatusOptionMetadata optionMeta in statusMetadata.OptionSet.Options)
            {
                logger.DebugFormat("val {0} label {1} State {2}",
                    optionMeta.Value, optionMeta.Label.UserLocalizedLabel.Label, optionMeta.State);
                if (optionMeta.State.HasValue && optionMeta.State.Value == forRelatedState)
                    dict.Add(optionMeta.Value, optionMeta.Label.UserLocalizedLabel.Label);
            }
            logger.DebugFormat("GetOptionSetStatus: Got {0} options for {1} {2} optionset filtered for state {3}",
                dict.Count, entityName, setName, forRelatedState);

            return dict;
        }

        public void Reconnect()
        {
            logger.DebugFormat("Reconnect (crmPicklistTasks) called, flushing connections caches ...");
            this._connectionPool = new CrmConnectionCache();
            
        }


        public class CrmConnectionRole
        {
            public Guid ConnectionRoleId { get; set; }
            public string Name { get; set; }
            public int Category { get; set; }
            public string PrescientRoleCode { get; set; }

        }

        public class CrmCaseContactConnectionRolePair
        {
            public Guid? ContactConnectionRole { get; set;}
            public Guid? CaseConnectionRole { get; set;}
        }

        /// <summary>
        /// Crm entity with Guid CrmID, Name and Code
        /// Used for Case Types, Centres, Contracts etc to translate from Code to CrmId
        /// </summary>
        public class CrmEntityForPicklist
        {
            public Guid CrmId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }

        }

        // added in Clarinet for CaseTypes
        public class CrmCaseType
        {
            public Guid CrmId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public string FixedFeeCodeSos { get; set; }

        }


        public class CrmCaseWorker
        {
            public Guid CaseWorkerId { get; set;}
            public string CaseWorkerName { get; set;}
            public string PrescientInitialsId { get; set;}
        }

        public class CrmPresentingProblem
        {
            public Guid PresentingProblemId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
        }

        public class CrmOptionSetForPicklist
        {
            public int CrmOptionSetValue { get; set; }
            public string PrescientCode { get; set; }

        }

        public class CrmOptionSetWithDescription
        {
            public int CrmOptionSetValue { get; set; }
            public string Label { get; set; }
            public string Description { get; set; }

        }
    }
}
