﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml.Linq;

namespace ClarinetLib
{
    public class IgnoreList
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<string, bool> m_ignoreDictionary = null;


        public virtual bool ShouldIgnore(string entityType, string sourceId)
        {
            if (m_ignoreDictionary == null)
                PopulateDictionary();

            string lookForKey = MakeDictionaryKey(entityType, sourceId);
            bool ret = m_ignoreDictionary.ContainsKey(lookForKey);
            if (ret)
                logger.DebugFormat("ShouldIgnore: returning true for {0} {1}", entityType, sourceId);

            return ret;
        }

        public virtual void PopulateDictionary()
        {
            logger.DebugFormat("PopulateDictionary called");
            string pathToIgnoreXml = ConfigurationManager.AppSettings["IgnoreList.XmlPath"];
            if (pathToIgnoreXml == null)
            {
                logger.DebugFormat("config file does not specify IgnoreList.XmlPath therefore assuming no records to be ignored");
                m_ignoreDictionary = new Dictionary<string, bool>();
                return;
            }


            XDocument ignoreXmlDoc = XDocument.Load(pathToIgnoreXml);
            logger.DebugFormat("Read ignore xml file from {0}", pathToIgnoreXml);
            List<XElement> ignoreElements = ignoreXmlDoc.Root.Elements("Ignore").ToList();
            m_ignoreDictionary = new Dictionary<string, bool>();
            foreach (XElement ie in ignoreElements)
            {
                string key = MakeDictionaryKey(ie.Attribute("EntityType").Value, ie.Attribute("SourceId").Value);
                m_ignoreDictionary.Add(key, true);
            }
            logger.DebugFormat("Added {0} items to in-memory ignore list", m_ignoreDictionary.Count());
        }

        private string MakeDictionaryKey(string table_name, string key_field_values)
        {
            return string.Format("{0}---{1}", table_name.Trim().ToLower(), key_field_values.Trim().ToLower());
        }

    }
}
