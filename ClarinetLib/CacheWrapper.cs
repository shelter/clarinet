﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Caching;

namespace ClarinetLib
{
    /// <summary>
    /// Clarinet will need to cache various things for efficiency
    /// This is a wrapper for the cache so that it can be mocked during testing
    /// </summary>
    public class CacheWrapper
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static MemoryCache _memoryCache = null; 

        static CacheWrapper()
        {
            logger.DebugFormat("Initialising MemoryCache..");
            _memoryCache = new MemoryCache("clarinetCache");

           
        }

        private CacheItemPolicy GetStandardPolicy()
        {
            var pol = new CacheItemPolicy();
            pol.AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(5);
            return pol;
        }

        public virtual void Add(string key, object toCache)
        {
            _memoryCache.Add(key, toCache, GetStandardPolicy());
        }

        public virtual object Get(string key)
        {
            return _memoryCache.Get(key);
        }

        public virtual bool Contains(string key)
        {
            return _memoryCache.Contains(key);
        }

    }
}
