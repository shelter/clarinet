﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Crm.Sdk.Messages;

namespace ClarinetLib.CrmConnection
{
    /// <summary>
    /// Class to connect to CRM and cache the results.
    /// Uses CrmConnectionFactory to actually connect
    /// call GetCrmConnectionForUsing() within a using {} block
    /// alternatively call GetCrmConnectionCached() (but this will waste resources as connections never closed)
    /// if neither of those work, call GetCrmConnectionOldStyle() (which is slow)
    /// </summary>
    public class CrmConnectionCache
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(CrmConnectionCache));
        

        private CrmConnectionFactory _crmConnectionFactory;

        // this article, "Improve Service Channel Allocation Performance" section
        // http://msdn.microsoft.com/en-us/library/gg509027.aspx#Performance
        // suggests that OrganisationServiceProxy should be cached but is not thread safe
        // hence this object uses a [threadstatic]
        [ThreadStatic]
        private static IOrganizationService s_cachedService;

        // this article, "Improve Service Channel Allocation Performance" section
        // http://msdn.microsoft.com/en-us/library/gg509027.aspx#Performance
        // suggests that connection kit components are thread safe so can be standard static
        private static CrmConnectionFactory.ConnectionKit s_connectionKit;

        // constructor for general use
        public CrmConnectionCache()
            : this(new CrmConnectionFactory())
        { }

        // constructur for unit testing
        public CrmConnectionCache(CrmConnectionFactory theFactory)
        {
            _crmConnectionFactory = theFactory;
        }

        /// <summary>
        /// Returns a CrmConnection that should be disposed of after use
        /// i.e. call this with a using { } block
        /// </summary>
        /// <returns></returns>
        public virtual IOrganizationService GetCrmConnectionForUsing()
        {
            // first, do we have the kit? (only need to get it once) 
            EnsureConnectionKitIsCached();

            logger.DebugFormat("GetCrmConnectionForUsing: making new connection from kit ...");
            return _crmConnectionFactory.MakeCrmConnectionFromKit(s_connectionKit);
        }

        /// <summary>
        /// Returns a cached CrmConnection for this particular thread
        /// This is (probably) going to waste system resources due to connections not getting closed
        /// </summary>
        /// <returns></returns>
        public virtual IOrganizationService GetCrmConnectionCached()
        {
            // first, do we have the kit? (only need to get it once) 
            EnsureConnectionKitIsCached();

            // next, does this thread have the service? (need to do this once for each thread)
            if (s_cachedService == null)
            {
                logger.DebugFormat("GetCrmConnection: cached service not created for this thread, need to make one ...");
                s_cachedService = _crmConnectionFactory.MakeCrmConnectionFromKit(s_connectionKit);
                logger.DebugFormat("GetCrmConnectionL cached service made");
            }
            else
                logger.DebugFormat("GetCrmConnection: returning cached connection");

            return s_cachedService;
        }

        private void EnsureConnectionKitIsCached()
        {
            // connection kit is thread safe so only needs to be created once
            if (s_connectionKit == null)
            {
                logger.DebugFormat("EnsureConnectionKitIsCached: Connection Kit not created yet, need to make one...");
                s_connectionKit = _crmConnectionFactory.MakeCrmConnectionThatIsRepeatable();
                logger.DebugFormat("EnsureConnectionKitIsCached: Connection Kit made");
            }
            else
                logger.DebugFormat("EnsureConnectionKitIsCached: connectionkit is already cached");
        }


        /// <summary>
        /// Returns a brand new CrmConnection which is a slow process
        /// Connection should ideally be disposed of after use
        /// i.e. use a using {} block
        /// </summary>
        /// <returns></returns>
        public virtual IOrganizationService GetCrmConnectionOldStyle()
        {
            if (s_cachedService == null)
            {
                s_cachedService = _crmConnectionFactory.MakeCrmConnection();
            }
            else
                logger.DebugFormat("GetCrmConnection: returning cached connection");

            return s_cachedService;
        }

        public static void FlushCachedConnections()
        {
            logger.DebugFormat("FlushCachedConnections called, flushing connections ...");
            s_cachedService = null;
            s_connectionKit = null;
        }

    }
}
