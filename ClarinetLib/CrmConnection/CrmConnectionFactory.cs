﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel;
using System.ServiceModel.Description;
using System.Reflection;

using System.Configuration;

using log4net;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Crm.Sdk.Messages;

namespace ClarinetLib.CrmConnection
{
    /// <summary>
    /// Class to create connections to CRM Late-Bound API
    /// based on this msdn example http://msdn.microsoft.com/en-gb/library/hh675404.aspx 
    /// but modified based on performance advice at http://msdn.microsoft.com/en-us/library/gg509027.aspx#Performance
    /// Reads connection from from web.config
    /// See CrmConnectionCache for caching of connections to improve performance.
    /// </summary>
    public class CrmConnectionFactory
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(CrmConnectionFactory));

        // To get discovery service address and organization unique name, 
        // Sign in to your CRM org and click Settings, Customization, Developer Resources.
        // On Developer Resource page, find the discovery service address under Service Endpoints and organization unique name under Your Organization Information.

        private String _discoveryServiceAddress;
        private String _organizationUniqueName;
        // Provide your user name and password.
        private String _userName;
        private String _password;

       
        // Provide domain name for the On-Premises org.
        private String _domain;

        public CrmConnectionFactory()
        {
            _discoveryServiceAddress = ConfigurationManager.AppSettings["CrmConnectionFactory.DiscoveryServiceAddress"];
            _organizationUniqueName = ConfigurationManager.AppSettings["CrmConnectionFactory.OrganizationUniqueName"];
            _userName = ConfigurationManager.AppSettings["CrmConnectionFactory.UserName"];
            _password = ConfigurationManager.AppSettings["CrmConnectionFactory.Password"];
            _domain = ConfigurationManager.AppSettings["CrmConnectionFactory.Domain"];


        }
     

        /// <summary>
        /// Returns a connection but this has to do authentication each time so it is slow
        /// </summary>
        public IOrganizationService MakeCrmConnection()
        {
            logger.DebugFormat("MakeCrmConnection called");

            OrganizationServiceProxy organizationProxy = null;

            IServiceManagement<IDiscoveryService> serviceManagement =
                        ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(
                        new Uri(_discoveryServiceAddress));
            AuthenticationProviderType endpointType = serviceManagement.AuthenticationType;

            // Set the credentials.
            AuthenticationCredentials authCredentials = GetCredentials(endpointType);


            String organizationUri = String.Empty;
            // Get the discovery service proxy.
            using (DiscoveryServiceProxy discoveryProxy =
                GetProxy<IDiscoveryService, DiscoveryServiceProxy>(serviceManagement, authCredentials))
            {
                // Obtain organization information from the Discovery service. 
                if (discoveryProxy != null)
                {
                    // Obtain information about the organizations that the system user belongs to.
                    OrganizationDetailCollection orgs = DiscoverOrganizations(discoveryProxy);

                    // find target org
                    OrganizationDetail namedOrg = FindOrganizationByUrlName(_organizationUniqueName,
                        orgs.ToArray());

                    if (namedOrg == null)
                    {
                        string orgList = string.Join(", ",orgs.Select(o=>o.UrlName).ToList());
                        throw new ApplicationException(string.Format("Could not find UrlName {0} in list {1}",
                            _organizationUniqueName, orgList));
                    }

                    // Obtains the Web address (Uri) of the target organization.
                    organizationUri = namedOrg.Endpoints[EndpointType.OrganizationService];

                }
            }


            if (!String.IsNullOrWhiteSpace(organizationUri))
            {
                IServiceManagement<IOrganizationService> orgServiceManagement =
                    ServiceConfigurationFactory.CreateManagement<IOrganizationService>(
                    new Uri(organizationUri));

                // Set the credentials.
                AuthenticationCredentials credentials = GetCredentials(endpointType);

                // Get the organization service proxy.
                organizationProxy =
                    GetProxy<IOrganizationService, OrganizationServiceProxy>(orgServiceManagement, credentials);
                
                    // This statement is required to enable early-bound type support.
                    //organizationProxy.EnableProxyTypes();
                organizationProxy.Timeout =  new TimeSpan(0, 6, 0);
                logger.DebugFormat("MakeCrmConnection created organizationProxy");
                string endpointUrl = orgServiceManagement.CurrentServiceEndpoint.ListenUri.GetLeftPart(UriPartial.Path);
                logger.DebugFormat("MakeCrmConnection: Service Endpoint {0}", endpointUrl);

            }

            return organizationProxy;

        }

        /// <summary>
        /// This returns the two objects than can be used to make connections repeatedly
        /// Not sure if it will work with AD authentication though
        /// </summary>
        /// <returns></returns>
        public ConnectionKit MakeCrmConnectionThatIsRepeatable()
        {
            logger.DebugFormat("MakeCrmConnectionThatIsRepeatable called");

            var kit = new ConnectionKit();
            

            IServiceManagement<IDiscoveryService> serviceManagement =
                        ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(
                        new Uri(_discoveryServiceAddress));
            AuthenticationProviderType endpointType = serviceManagement.AuthenticationType;

            // Set the credentials.
            AuthenticationCredentials authCredentials = GetCredentials(endpointType);


            String organizationUri = String.Empty;
            // Get the discovery service proxy.
            using (DiscoveryServiceProxy discoveryProxy =
                GetProxy<IDiscoveryService, DiscoveryServiceProxy>(serviceManagement, authCredentials))
            {
                // Obtain organization information from the Discovery service. 
                if (discoveryProxy != null)
                {
                    // Obtain information about the organizations that the system user belongs to.
                    OrganizationDetailCollection orgs = DiscoverOrganizations(discoveryProxy);
                    // Obtains the Web address (Uri) of the target organization.
                    organizationUri = FindOrganizationByUrlName(_organizationUniqueName,
                        orgs.ToArray()).Endpoints[EndpointType.OrganizationService];

                }
            }


            if (!String.IsNullOrWhiteSpace(organizationUri))
            {
                IServiceManagement<IOrganizationService> orgServiceManagement =
                    ServiceConfigurationFactory.CreateManagement<IOrganizationService>(
                    new Uri(organizationUri));

                // Set the credentials.
                AuthenticationCredentials credentials = GetCredentials(endpointType);

                kit.TokenCredentials = serviceManagement.Authenticate(credentials);
                kit.OrgServiceManagement = orgServiceManagement;

                

                // This statement is required to enable early-bound type support.
                //organizationProxy.EnableProxyTypes();
               
                logger.DebugFormat("MakeCrmConnectionThatIsRepeatable created connectionkit");
            }

            return kit;

        }

        /// <summary>
        /// Makes a connection from the ConnectionKit returned by MakeCrmConnectionThatIsRepeatable()
        /// You might have problems using this with AD, if so try the normal MakeCrmConnection instead
        /// </summary>
        /// <param name="kit"></param>
        /// <returns></returns>
        public IOrganizationService MakeCrmConnectionFromKit(ConnectionKit kit)
        {
            logger.DebugFormat("MakeCrmConnectionFromKit called");

            OrganizationServiceProxy organizationProxy = null;

            organizationProxy = new OrganizationServiceProxy(kit.OrgServiceManagement, kit.TokenCredentials.SecurityTokenResponse);
            organizationProxy.Timeout = new TimeSpan(0, 6, 0);

            string endpointUrl = kit.OrgServiceManagement.CurrentServiceEndpoint.ListenUri.GetLeftPart(UriPartial.Path);
            logger.DebugFormat("MakeCrmConnectionFromKit: Service Endpoint {0}", endpointUrl);

            var s = organizationProxy.HomeRealmUri;

            return organizationProxy;
        }

        public class ConnectionKit
        {
            public IServiceManagement<IOrganizationService> OrgServiceManagement { get; set; }
            public AuthenticationCredentials TokenCredentials { get; set; }
        }

        /// <summary>
        /// Obtain the AuthenticationCredentials based on AuthenticationProviderType.
        /// </summary>
        /// <param name="endpointType">An AuthenticationProviderType of the CRM environment.</param>
        /// <returns>Get filled credentials.</returns>
        private AuthenticationCredentials GetCredentials(AuthenticationProviderType endpointType)
        {
            logger.DebugFormat("GetCredentials called, endpointType {0}", endpointType.ToString());
            AuthenticationCredentials authCredentials = new AuthenticationCredentials();
            switch (endpointType)
            {
                case AuthenticationProviderType.ActiveDirectory:
                    authCredentials.ClientCredentials.Windows.ClientCredential =
                        new System.Net.NetworkCredential(_userName,
                            _password,
                            _domain);
                    break;
                case AuthenticationProviderType.LiveId:
                    authCredentials.ClientCredentials.UserName.UserName = _userName;
                    authCredentials.ClientCredentials.UserName.Password = _password;
                    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                    authCredentials.SupportingCredentials.ClientCredentials =
                        Microsoft.Crm.Services.Utility.DeviceIdManager.LoadOrRegisterDevice();
                    break;
                default: // For Federated and OnlineFederated environments.                    
                    authCredentials.ClientCredentials.UserName.UserName = _userName;
                    authCredentials.ClientCredentials.UserName.Password = _password;
                    // For OnlineFederated single-sign on, you could just use current UserPrincipalName instead of passing user name and password.
                    // authCredentials.UserPrincipalName = UserPrincipal.Current.UserPrincipalName;  //Windows/Kerberos
                    break;
            }

            return authCredentials;
        }

        /// <summary>
        /// Discovers the organizations that the calling user belongs to.
        /// </summary>
        /// <param name="service">A Discovery service proxy instance.</param>
        /// <returns>Array containing detailed information on each organization that 
        /// the user belongs to.</returns>
        public OrganizationDetailCollection DiscoverOrganizations(
            IDiscoveryService service)
        {
            if (service == null) throw new ArgumentNullException("service");
            RetrieveOrganizationsRequest orgRequest = new RetrieveOrganizationsRequest();
            RetrieveOrganizationsResponse orgResponse =
                (RetrieveOrganizationsResponse)service.Execute(orgRequest);

            return orgResponse.Details;
        }

        /// <summary>
        /// Finds a specific organization detail in the array of organization details
        /// returned from the Discovery service.
        /// </summary>
        /// <param name="orgUniqueName">The unique name of the organization to find.</param>
        /// <param name="orgDetails">Array of organization detail object returned from the discovery service.</param>
        /// <returns>Organization details or null if the organization was not found.</returns>
        /// <seealso cref="DiscoveryOrganizations"/>
        public OrganizationDetail FindOrganization(string orgUniqueName,
            OrganizationDetail[] orgDetails)
        {
            if (String.IsNullOrWhiteSpace(orgUniqueName))
                throw new ArgumentNullException("orgUniqueName");
            if (orgDetails == null)
                throw new ArgumentNullException("orgDetails");
            OrganizationDetail orgDetail = null;

            foreach (OrganizationDetail detail in orgDetails)
            {
                if (String.Compare(detail.UniqueName, orgUniqueName,
                    StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }
            return orgDetail;
        }

        /// <summary>
        /// Finds a specific organization detail in the array of organization details
        /// returned from the Discovery service.
        /// </summary>
        /// <param name="orgUrlName">The unique name of the organization to find.</param>
        /// <param name="orgDetails">Array of organization detail object returned from the discovery service.</param>
        /// <returns>Organization details or null if the organization was not found.</returns>
        /// <seealso cref="DiscoveryOrganizations"/>
        public OrganizationDetail FindOrganizationByUrlName(string orgUrlName,
            OrganizationDetail[] orgDetails)
        {
            if (String.IsNullOrWhiteSpace(orgUrlName))
                throw new ArgumentNullException("orgUrlName");
            if (orgDetails == null)
                throw new ArgumentNullException("orgDetails");
            OrganizationDetail orgDetail = null;

            foreach (OrganizationDetail detail in orgDetails)
            {
                if (String.Compare(detail.UrlName, orgUrlName,
                    StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }
            return orgDetail;
        }


        /// <summary>
        /// Generic method to obtain discovery/organization service proxy instance.
        /// </summary>
        /// <typeparam name="TService">
        /// Set IDiscoveryService or IOrganizationService type to request respective service proxy instance.
        /// </typeparam>
        /// <typeparam name="TProxy">
        /// Set the return type to either DiscoveryServiceProxy or OrganizationServiceProxy type based on TService type.
        /// </typeparam>
        /// <param name="serviceManagement">An instance of IServiceManagement</param>
        /// <param name="authCredentials">The user's Microsoft Dynamics CRM logon credentials.</param>
        /// <returns></returns>
        private TProxy GetProxy<TService, TProxy>(
            IServiceManagement<TService> serviceManagement,
            AuthenticationCredentials authCredentials)
            where TService : class
            where TProxy : ServiceProxy<TService>
        {
            logger.DebugFormat("GetProxy called with serviceManagement {0} authCredentuals {1}", serviceManagement.ToString(), authCredentials.UserPrincipalName);
            Type classType = typeof(TProxy);

            if (serviceManagement.AuthenticationType !=
                AuthenticationProviderType.ActiveDirectory)
            {
                AuthenticationCredentials tokenCredentials =
                    serviceManagement.Authenticate(authCredentials);
                // Obtain discovery/organization service proxy for Federated, LiveId and OnlineFederated environments. 
                // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and SecurityTokenResponse.
                return (TProxy)classType
                    .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(SecurityTokenResponse) })
                    .Invoke(new object[] { serviceManagement, tokenCredentials.SecurityTokenResponse });
            }

            // Obtain discovery/organization service proxy for ActiveDirectory environment.
            // Instantiate a new class of type using the 2 parameter constructor of type IServiceManagement and ClientCredentials.
            return (TProxy)classType
                .GetConstructor(new Type[] { typeof(IServiceManagement<TService>), typeof(ClientCredentials) })
                .Invoke(new object[] { serviceManagement, authCredentials.ClientCredentials });
        }

    }
}
