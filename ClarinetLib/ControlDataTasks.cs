﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using ClarinetLib.LocalModel;

namespace ClarinetLib
{
    public class ControlDataTasks
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public List<int> FetchContactIdsToProcess()
        {


            string sql = "exec prcFetchContactsToProcess";
            var adapt = new SqlDataAdapter(sql, GetConnection());
            adapt.SelectCommand.CommandTimeout = 200;
            DataTable tab = new DataTable();
            adapt.Fill(tab);

            return tab.AsEnumerable().Select(s => (int)s["party_ref"]).ToList<int>();

        }

        public void WriteContactDone(int prescientPartyRef)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "exec prcMarkContactDone @partyref";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@partyref", System.Data.SqlDbType.Int).Value = prescientPartyRef;

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetContacts insert with id {0} failed", prescientPartyRef));



            }

        }

        public void MarkContactNotFoundInCrm(int prescientPartyRef)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "insert into ClarinetContactNotFoundInCrm(PrescientPartyRef) values(@partyref)";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@partyref", System.Data.SqlDbType.Int).Value = prescientPartyRef;

                conn.Open();
                int rows = comm.ExecuteNonQuery();



            }

        }

        public List<String> FetchCaseRefsToProcess()
        {


            string sql = "exec prcFetchCasesToProcess";
            var adapt = new SqlDataAdapter(sql, GetConnection());
            DataTable tab = new DataTable();
            adapt.Fill(tab);

            return tab.AsEnumerable().Select(s => (String)s["case_ref"]).ToList<String>();

        }

        public List<Guid> FetchCaseIdsForSosSync()
        {


            string sql = "exec prcFetchCasesForSosSync";
            var adapt = new SqlDataAdapter(sql, GetConnection());
            DataTable tab = new DataTable();
            adapt.Fill(tab);

            return tab.AsEnumerable().Select(s => (Guid)s["CrmCaseId"]).ToList<Guid>();

        }

        public List<String> FetchCaseRefsWithTimeRecordsToProcess()
        {


            string sql = "set transaction isolation level read uncommitted exec prcFetchCasesWithTimeToProcess";
            var adapt = new SqlDataAdapter(sql, GetConnection());
            adapt.SelectCommand.CommandTimeout = 1200;
            DataTable tab = new DataTable();
            adapt.Fill(tab);

            return tab.AsEnumerable().Select(s => (String)s["PrescientCaseRef"]).ToList<String>();

        }

        public void FetchTargetIDsForTimeRecords(Referrals ds)
        {
            List<int> timeIds = ds.TimeRecord.AsEnumerable().Select(d => int.Parse(d.SourceTimeRecordID)).ToList<int>();
            if (timeIds.Count == 0)
                return;

            string sqlPattern = @"
select *
from ClarinetTimeRecords
where PrescientTimeKey In ({0})";
            string sql = String.Format(sqlPattern, ListToIn(timeIds));
            var adapt = new SqlDataAdapter(sql, GetConnection());
            DataTable tab = new DataTable();
            adapt.Fill(tab);
            logger.DebugFormat("FetchTargetIDsForTimeRecords fetched {0} ClarinetTimeRecords based on {1} time Ids ", tab.Rows.Count, timeIds.Count);

            // loop through table and put the target ids back in
            int updateCount = 0;
            foreach(DataRow rowLoop in tab.Rows)
            {
                int prescientTimeId = (int)rowLoop["PrescientTimeKey"];
                Guid targetTimeId = Guid.Empty;
                if (rowLoop["CrmTimeID"] != DBNull.Value)
                    targetTimeId = (Guid)rowLoop["CrmTimeID"];

                if (targetTimeId != Guid.Empty)
                {
                    var matchRow = ds.TimeRecord.FindBySourceTimeRecordID(prescientTimeId.ToString());
                    if (matchRow == null)
                        throw new ApplicationException(string.Format("Expected to find Time Record {0} in prescient data (crm id {1} )",
                            prescientTimeId, targetTimeId));
                    matchRow.TargetTimeRecordID = targetTimeId.ToString();
                    updateCount += 1;
                }
            }
            logger.DebugFormat("FetchTargetIDsForTimeRecords updated {0} TargetTimeRecordIDs on dataset", updateCount);
        }

        private String ListToIn(List<int> ids)
        {
            StringBuilder sb = new StringBuilder();

            foreach (int i in ids)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(i.ToString());
            }

            return sb.ToString();
        }

        public void WriteCaseDone(String prescientCaseRef, Guid crmCaseId)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "exec prcMarkCaseDone @caseref, @crmcaseid";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@caseref", System.Data.SqlDbType.VarChar, 50).Value = prescientCaseRef;
                comm.Parameters.Add("@crmcaseid", System.Data.SqlDbType.UniqueIdentifier).Value = crmCaseId;

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetCases insert with id {0} failed", prescientCaseRef));



            }

        }

        public void WriteSosSyncDone(String prescientCaseRef, Guid crmCaseId)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "exec prcMarkSosSyncDone @caseref, @crmcaseid";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@caseref", System.Data.SqlDbType.VarChar, 50).Value = prescientCaseRef;
                comm.Parameters.Add("@crmcaseid", System.Data.SqlDbType.UniqueIdentifier).Value = crmCaseId;

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetCasesSyncedToSos insert with id {0} failed", prescientCaseRef));



            }

        }


        public void WriteTimeRecordDone(String prescientCaseRef, int prescientTimeKey, Guid crmTimeId)
        {
            
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "exec prcMarkTimeRecordDone @PrescientCaseRef, @PrescientTimeKey, @CrmTimeID";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@PrescientCaseRef", System.Data.SqlDbType.VarChar, 50).Value = prescientCaseRef;
                comm.Parameters.Add("@PrescientTimeKey", System.Data.SqlDbType.Int).Value = prescientTimeKey;
                comm.Parameters.Add("@CrmTimeID", System.Data.SqlDbType.UniqueIdentifier).Value = crmTimeId;

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetTimeRecord insert with id {0} failed", prescientTimeKey));



            }

        }

        public void MarkCaseNotFoundInCrm(String prescientCaseRef)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "insert into ClarinetCaseNotFoundInCrm(PrescientCaseRef) values(@caseref)";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@caseref", System.Data.SqlDbType.VarChar, 50).Value = prescientCaseRef;

                conn.Open();
                int rows = comm.ExecuteNonQuery();



            }

        }

        public SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(GetConnectionString());
            return conn;
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ClarinetDb"].ConnectionString;


        }
    }
}
