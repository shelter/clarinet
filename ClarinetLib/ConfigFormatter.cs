﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel.Configuration;

namespace ClarinetLib
{
    // this class returns key config settings in a nice format
    // for displaying on forms, logs etc
    public class ConfigFormatter
    {

        public String GetFriendlyPrescientConfig()
        {
            String rawConnectionString = ConfigurationManager.ConnectionStrings["Prescient"].ConnectionString;
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(rawConnectionString);

            return String.Format("SQL Server: {0}  Database: {1}  SqlUser: {2}",
                builder.DataSource, builder.InitialCatalog, builder.UserID);

        }

        public String GetFriendlyClarinetDbConfig()
        {
            String rawConnectionString = ConfigurationManager.ConnectionStrings["ClarinetDb"].ConnectionString;
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder(rawConnectionString);

            return String.Format("SQL Server: {0}  Database: {1}  SqlUser: {2}",
                builder.DataSource, builder.InitialCatalog, builder.UserID);

        }

        public String GetFriendlyCrmConfig()
        {
            String crmOrgName = ConfigurationManager.AppSettings["CrmConnectionFactory.OrganizationUniqueName"];
            String crmUser = ConfigurationManager.AppSettings["CrmConnectionFactory.UserName"];

            return String.Format("Crm OrgName: {0}  Crm User: {1}",
                crmOrgName, crmUser);
        }

        public String GetFriendlyGapSosConfig()
        {
            // read from config
            var client = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var sosEndpoint = client.Endpoints.Cast<ChannelEndpointElement>().SingleOrDefault(endpoint => endpoint.Name == "BasicHttpsBinding_ISOSService");
            if (sosEndpoint != null)
                return sosEndpoint.Address.AbsoluteUri;
            return "";
        }

    }
}
