﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ClarinetLib.LocalModel;
using ClarinetLib.CrmConnection;
using Microsoft.Crm.Sdk.Messages;

namespace ClarinetLib
{
    public class CrmTasks
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private CrmConnectionCache _connectionPool;
        
        private CrmPicklistTasks _picklistTasks;


        public CrmTasks() : this(new CrmConnectionCache(), new CrmPicklistTasks()) { }

        // constrcutor for unit testing
        public CrmTasks(CrmConnectionCache connPool, CrmPicklistTasks picktasks)
        {
            _connectionPool = connPool;
        
            _picklistTasks = picktasks;
        }


        public virtual CrmSaveResult SaveContact(Referrals.ContactRow conRow, Referrals.ContactIndividualRow conindRow)
        {
            logger.DebugFormat("Save contact called, SourceContactID {0}", conRow.SourceContactID);
            var res = new CrmSaveResult();


            Entity contact = new Entity("contact");


            if (!string.IsNullOrEmpty(conRow.TargetContactID))
            {
                res.IsUpdate = true;
                contact["contactid"] = new Guid(conRow.TargetContactID);
                res.CrmId = (Guid)contact["contactid"];
            }
            else
            { 
                throw new ApplicationException(string.Format("expected contact {0} to exist in CRM but it didnt", conRow.SourceContactID));
            }
            contact["gap_prescientcontactid"] = conRow.SourceContactID;
            contact["gap_prescientclientid"] = conRow.SourceReference;

            contact["emailaddress1"] = SafeString(conRow["EmailAddress1"]);
            contact["emailaddress2"] = SafeString(conRow["EmailAddress2"]);
            contact["emailaddress3"] = SafeString(conRow["EmailAddress3"]);
            contact["telephone1"] = SafeString(conRow["Telephone1"]);
            contact["telephone2"] = SafeString(conRow["Telephone2"]);
            contact["telephone3"] = SafeString(conRow["MobilePhone"]); 
            // TODO  gap_telephone1type etc?

            // check for null names, due to bug in prescient web service that sends companies as individuals
            if (string.IsNullOrEmpty(SafeString(conindRow["Forename"])) && string.IsNullOrEmpty(SafeString(conindRow["Surname"])))
                throw new ApplicationException(string.Format("Contact SourceContactID {0} has no Forename or Surname - possibly an Organisation sent wrongly?",
                    conindRow.SourceContactID));

            contact["firstname"] = SafeString(conindRow["Forename"]);
            contact["lastname"] = SafeString(conindRow["Surname"]); 
            var titleLabel = SafeString(conindRow["Title"]);
            if (titleLabel != null)
                titleLabel = titleLabel.ToLower().Trim();
            if (string.IsNullOrEmpty(titleLabel))
                titleLabel = ExtractTitleFromMailingName(conindRow);
            var titleDictionary = _picklistTasks.GetTitleOptionSet();
            if (titleDictionary.ContainsValue(titleLabel))
            {
                var titleValue = titleDictionary.First(t => t.Value == titleLabel).Key.Value;
                contact["gap_title"] = new OptionSetValue(titleValue);
            }
            else
                logger.WarnFormat("Warning: SaveContact for SourceContactID {0} could not map Title {1} because no matching title exists in CRM",
                    conRow.SourceContactID, titleLabel);
            
            
            contact["middlename"] = SafeString(conindRow["Middlenames"]);
            string ni = SafeString(conindRow["NINumber"]);
            if (!string.IsNullOrEmpty(ni))
            {
                if (ni.Length > 12)
                    ni = ni.Substring(0, 12);
                contact["gap_nationalinsuranceno"] = ni;
            }
            
            contact["birthdate"] = SafeStringToDate(conindRow["Birthdate"]);

            // new fields for clarinet
            // gender
            if (!conindRow.IsGenderNull() && !string.IsNullOrEmpty(conindRow.Gender))
            {
                var matchGender = _picklistTasks.GetLAAGenders().FirstOrDefault(p => p.Code == conindRow.Gender);
                if (matchGender == null)
                    logger.WarnFormat("Contact SourceContactID {0} has Gender code {1} but could not find a match for that in CRM LAA Gender list",
                    conindRow.SourceContactID, conindRow.Gender);
                else
                    contact["gap_laagender"] = new EntityReference("gap_laagender", matchGender.CrmId);
            }
            // ethnicity
            if (!conindRow.IsEthnicityNull() && !string.IsNullOrEmpty(conindRow.Ethnicity))
            {
                var ethValue = conindRow.Ethnicity;
                if (ethValue == "REF" || ethValue == "NA" || ethValue == "n/a")
                    ethValue = "99";
                var matchEthnicity = _picklistTasks.GetLAAEthnicities().FirstOrDefault(p => p.Code == ethValue);
                if (matchEthnicity == null)
                    throw new ApplicationException(string.Format("Contact SourceContactID {0} has Ethnicity code {1} but could not find a match for that in CRM LAA Ethnicity list",
                    conindRow.SourceContactID, conindRow.Ethnicity));
                contact["gap_laaethnicity"] = new EntityReference("gap_laaethnicity", matchEthnicity.CrmId);
            }
            // disability
            if (!conindRow.IsDisabilityNull() && !string.IsNullOrEmpty(conindRow.Disability))
            {
                var disValue = conindRow.Disability;
                if (disValue == "NA" || disValue == "PHY" || disValue == "SEN" || disValue == "n/a" || disValue == "PNS")
                    disValue = "UKN";
                var matchDisability = _picklistTasks.GetLAADisabilities().FirstOrDefault(p => p.Code == disValue);
                if (matchDisability == null)
                    throw new ApplicationException(string.Format("Contact SourceContactID {0} has Disability code {1} but could not find a match for that in CRM Disabilities list",
                    conindRow.SourceContactID, conindRow.Disability));
                contact["gap_laadisability"] = new EntityReference("gap_disability", matchDisability.CrmId);
            }

            var serv = _connectionPool.GetCrmConnectionCached();
            if (res.IsInsert)
            {
                res.CrmId = serv.Create(contact);
                logger.DebugFormat("Contact save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(contact);
                logger.DebugFormat("Contact save (update) updated record {0}", res.CrmId.ToString());
            }
            

            return res;

        }



        private string ExtractTitleFromMailingName(Referrals.ContactIndividualRow conindRow)
        {
            if (conindRow.IsMailingNameNull())
                return null;
            if (string.IsNullOrEmpty(conindRow.MailingName))
                return null;

            int firstSpace = conindRow.MailingName.IndexOf(" ");
            if (firstSpace > 0)
            {
                string maybeTitle = conindRow.MailingName.Substring(0, firstSpace);
                logger.DebugFormat("ExtractTitleFromMailingName: Contact {0} got title {1} from mailing name",
                    conindRow.SourceContactID, maybeTitle);
                return maybeTitle.ToLower();
;            }
            return null;
        }

        public virtual string GetContactRef(Guid contactId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity contact = serv.Retrieve("contact", contactId, new ColumnSet(new string[] {
                    "contactid", "gap_contactref"}));
            if (contact.Attributes.Contains("gap_contactref"))
            {
                var cref = (string)contact["gap_contactref"];
                logger.DebugFormat("Returning ContactRef {0} for contact {1}",
                    cref, contactId);
                return cref;
            }
            else
            {
                logger.DebugFormat("No contactRef found for contact {0}",
                    contactId);
                return "";
            }
        }

        public virtual Guid? GetContactCrmId(string contactPrescientId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='contact'>
                    <attribute name='contactid' />
                    <attribute name='gap_prescientcontactid' />
                    <filter type='and'>
                      <condition attribute='gap_prescientcontactid' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape( contactPrescientId));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity contactLoop in fetchresults.Entities)
            {
                res = (Guid)contactLoop["contactid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetContactCrmId: found {0} contacts with prescient ID {1} - there should not be more than 1",
                    count, contactPrescientId));

            logger.DebugFormat("GetContactCrmId returning {0} for prescientContactId {1}",
                (res.HasValue) ? res.Value.ToString() : "null", contactPrescientId);
            return res;
        }

        public virtual List<Tuple<String,String>> GetContacts()
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='contact'>
                    <attribute name='contactid' />
                    <attribute name='gap_prescientcontactid' />
                        <filter type='and'>
                      <condition attribute='gap_prescientcontactid' operator='not-null' />
                    </filter>
                  </entity></fetch>";

            string fetch = fetchXmlPattern;
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            List<Tuple<String, String>> ret = new List<Tuple<string, string>>();
            int count = 0;
            foreach (Entity contactLoop in fetchresults.Entities)
            {
                
                Guid g = (Guid)contactLoop["contactid"];
                String s = (String)contactLoop["gap_prescientcontactid"];
                logger.DebugFormat("GetContacts returned {0} {1}",
                g.ToString(), s);
                var t = new Tuple<String, String>(g.ToString(), s);
                ret.Add(t);
                count += 1;
            }
          

            logger.DebugFormat("GetContacts returning {0} records",
                count);
            return ret;
        }


        public virtual Guid? GetContactAlertCrmId(string alertPrescientId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='gap_alert'>
                    <attribute name='gap_alertid' />
                    <attribute name='gap_prescientalertid' />
                    <filter type='and'>
                      <condition attribute='gap_prescientalertid' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(alertPrescientId));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity contactLoop in fetchresults.Entities)
            {
                res = (Guid)contactLoop["gap_alertid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetContactAlertCrmId: found {0} alerts with prescient ID {1} - there should not be more than 1",
                    count, alertPrescientId));

            logger.DebugFormat("GetContactAlertCrmId returning {0} for prescientAlertId {1}",
                (res.HasValue) ? res.Value.ToString() : "null", alertPrescientId);
            return res;
        }

       

        public virtual Guid? GetCaseCrmId(string casePrescientId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            // whatever is passed in, we want to search for both   123456/1 and 123456.1
            string casePrescientIdWithDot = null;
            string casePrescientIdWithSlash = null;
            if (casePrescientId.Contains("."))
            {
                casePrescientIdWithDot = casePrescientId;
                casePrescientIdWithSlash = casePrescientId.Replace(".", "/");
            }
            else
            {
                casePrescientIdWithSlash = casePrescientId;
                casePrescientIdWithDot = casePrescientId.Replace("/",".");
            }


            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='incident'>
                    <attribute name='incidentid' />
                    <attribute name='gap_prescientcaseid' />
                    <filter type='and'>
                        <filter type='or'>
                            <condition attribute='gap_prescientcaseid' value='{0}' operator='eq'/>
                            <condition attribute='gap_prescientcaseid' value='{1}' operator='eq'/> 
                        </filter> </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(casePrescientIdWithDot),
                System.Security.SecurityElement.Escape(casePrescientIdWithSlash));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity caseLoop in fetchresults.Entities)
            {
                res = (Guid)caseLoop["incidentid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetCaseCrmId: found {0} cases with prescient ID {1} or {2} - there should not be more than 1",
                    count, casePrescientIdWithDot, casePrescientIdWithSlash));

            logger.DebugFormat("GetCaseCrmId returning {0} for casePrescientId {1} or {2}",
                (res.HasValue) ? res.Value.ToString() : "null", casePrescientIdWithDot, casePrescientIdWithSlash);
            return res;
        }

        public virtual Guid? GetPresentingProblemCrmId(string code)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='gap_presentingproblems'>
                    <attribute name='gap_presentingproblemsid' />
                    <filter type='and'>
                      <condition attribute='gap_code' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(code));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity presentingProblem in fetchresults.Entities)
            {
                res = (Guid)presentingProblem["gap_presentingproblemsid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetPresentingProblemCrmId: found {0} presenting problems with code {1} - there should not be more than 1",
                    count, code));

            logger.DebugFormat("GetPresentingProblemCrmId returning {0} for code {1}",
                (res.HasValue) ? res.Value.ToString() : "null", code);
            return res;
        }

        public virtual Guid? GetOrganisationCrmId(string organisationPrescientId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='account'>
                    <attribute name='accountid' />
                    <attribute name='gap_prescientid' />
                    <filter type='and'>
                      <condition attribute='gap_prescientid' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(organisationPrescientId));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity accountLoop in fetchresults.Entities)
            {
                res = (Guid)accountLoop["accountid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetOrganisationCrmId: found {0} accounts with prescient ID {1} - there should not be more than 1",
                    count, organisationPrescientId));

            logger.DebugFormat("GetOrganisationCrmId returning {0} for organisationPrescientId {1}",
                (res.HasValue) ? res.Value.ToString() : "null", organisationPrescientId);
            return res;
        }

        public virtual Guid? GetAddressCrmId(string addressPrescientId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='gap_address'>
                    <attribute name='gap_addressid' />
                    <attribute name='gap_prescientaddressid' />
                    <filter type='and'>
                      <condition attribute='gap_prescientaddressid' operator='eq' value='{0}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(addressPrescientId));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            Guid? res = null;
            int count = 0;
            foreach (Entity addressLoop in fetchresults.Entities)
            {
                res = (Guid)addressLoop["gap_addressid"];
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("GetAddressCrmId: found {0} addresses with prescient ID {1} - there should not be more than 1",
                    count, addressPrescientId));

            logger.DebugFormat("GetAddressCrmId returning {0} for addressPrescientId {1}",
                (res.HasValue) ? res.Value.ToString() : "null", addressPrescientId);
            return res;
        }

        public virtual string GetOrganisationRef(Guid organisationId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity account = serv.Retrieve("account", organisationId, new ColumnSet(new string[] {
                    "accountid", "accountnumber"}));
            if (account.Attributes.Contains("accountnumber"))
            {
                var oref = (string)account["accountnumber"];
                logger.DebugFormat("Returning OrgRef {0} for organisation {1}",
                    oref, organisationId);
                return oref;
            }
            else
            {
                logger.DebugFormat("No OrgRef found for organisation {0}",
                    organisationId);
                return "";
            }
        }

        public virtual string GetCaseRef(Guid caseId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity account = serv.Retrieve("incident", caseId, new ColumnSet(new string[] {
                    "incidentid", "ticketnumber"}));
            if (account.Attributes.Contains("ticketnumber"))
            {
                var cref = (string)account["ticketnumber"];
                logger.DebugFormat("Returning CaseRef {0} for case {1}",
                    cref, caseId);
                return cref;
            }
            else
            {
                logger.DebugFormat("No CaseRef found for case {0}",
                    caseId);
                return "";
            }
        }

        public virtual bool DoesContactExist(Guid contactId)
        {
            return DoesEntityExist("contact", contactId);
        }

        public virtual bool DoesOrganisationExist(Guid organisationId)
        {
            return DoesEntityExist("account", organisationId);
        }

        private bool DoesEntityExist(string entityName, Guid crmId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            string fetchXmlPattern = @"<fetch mapping='logical' count='10'>
                 <entity name='{0}'>
                    <attribute name='{0}id' />
                    <filter type='and'>
                      <condition attribute='{0}id' operator='eq' value='{1}' />
                    </filter>
                  </entity></fetch>";

            string fetch = string.Format(fetchXmlPattern, System.Security.SecurityElement.Escape(entityName), System.Security.SecurityElement.Escape(crmId.ToString()));
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));

            return (fetchresults.Entities.FirstOrDefault() != null);

        }

        public virtual Guid GetPresentingProblemRef(string description)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            var presentingProblems = _picklistTasks.GetPresentingProblems();
            var matching = presentingProblems.FirstOrDefault(p => p.Code == description) ?? presentingProblems.Single(p => p.Name == "Other");

            return matching.PresentingProblemId;
        }


        public virtual CrmSaveResult SaveOrganisation(Referrals.ContactRow conRow, Referrals.ContactOrganisationRow conorgRow)
        {
            logger.DebugFormat("SaveOrganisation called, SourceContactID {0}", conRow.SourceContactID);
            var res = new CrmSaveResult();

            Entity account = new Entity("account");


            if (!string.IsNullOrEmpty(conRow.TargetContactID))
            {
                res.IsUpdate = true;
                account["accountid"] = new Guid(conRow.TargetContactID);
                res.CrmId = (Guid)account["accountid"];
            }
            else
            {
                throw new ApplicationException(string.Format("expected org {0} to exist in CRM but it didnt", conRow.SourceContactID));
            }
            account["gap_prescientid"] = conRow.SourceContactID;
            

            account["emailaddress1"] = SafeString(conRow["EmailAddress1"]);
            account["emailaddress2"] = SafeString(conRow["EmailAddress2"]);
            account["emailaddress3"] = SafeString(conRow["EmailAddress3"]);
            account["telephone1"] = SafeString(conRow["Telephone1"]);
            account["telephone2"] = SafeString(conRow["Telephone2"]);
            account["telephone3"] = SafeString(conRow["MobilePhone"]);
            // TODO  gap_telephone1type etc?

            account["name"] = SafeString(conorgRow["Name"]);
            account["websiteurl"] = SafeString(conorgRow["WebsiteURL"]);

            
            var serv = _connectionPool.GetCrmConnectionCached();
            if (res.IsInsert)
            {
                res.CrmId = serv.Create(account);
                logger.DebugFormat("Organisation save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(account);
                logger.DebugFormat("Organisation save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;

        }



        public virtual CrmSaveResult SaveAddress(Referrals.AddressRow addressRow)
        {
            logger.DebugFormat("Save Address called, SourceAddressID {0}", addressRow.SourceAddressID);
            var res = new CrmSaveResult();


            Entity address = new Entity("gap_address");


            if (!string.IsNullOrEmpty(addressRow.TargetAddressID))
            {
                res.IsUpdate = true;
                address["gap_addressid"] = new Guid(addressRow.TargetAddressID);
                res.CrmId = (Guid)address["gap_addressid"];
            }
            address["gap_prescientaddressid"] = addressRow.SourceAddressID;

            address["gap_line1"] = SafeString(addressRow["AddressLine1"]);
            address["gap_line2"] = SafeString(addressRow["AddressLine2"]);
            address["gap_line3"] = SafeString(addressRow["AddressLine3"]);
            address["gap_citytown"] = SafeString(addressRow["Town"]);
            address["gap_postcode"] = SafeString(addressRow["Postcode"]);
            address["gap_county"] = SafeString(addressRow["County"]);
            address["gap_country"] = SafeString(addressRow["Country"]);

            //TODO Telephone1 and Telephone2 on address - ignore?
           
            var serv = _connectionPool.GetCrmConnectionCached();
            if (res.IsInsert)
            {
                res.CrmId = serv.Create(address);
                logger.DebugFormat("Address save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(address);
                logger.DebugFormat("Address save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;

        }

        public virtual CrmSaveResult SaveContactAddressLink(Referrals.ContactAddressLinkRow linkRow, Guid targetContactId, Guid targetAddressId)
        {
            logger.DebugFormat("Save ContactAddressLink called, SourceContactAddressLinkID {0}", linkRow.SourceContactAddressLinkID);
            var res = new CrmSaveResult();
            res.CrmId = targetContactId;

            // equivalent in CRM is just the FK from Contact to Address
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity contact = serv.Retrieve("contact", targetContactId, new ColumnSet(new string[] {
                    "contactid", "gap_currentaddress"}));

            if (contact.Attributes.Contains("gap_currentaddress"))
            {
                Guid currentAddressId = ((EntityReference)contact["gap_currentaddress"]).Id;

                if (currentAddressId == targetAddressId)
                {
                    logger.DebugFormat("SaveContactAddressLink: contact {0} already linked to address {1} so leaving as it is",
                        targetContactId, targetAddressId);
                    return res;
                }
            }
            else
            {
                logger.DebugFormat("SaveContactAddressLink: contact {0} not linked to any address yet", targetContactId);
            }

            contact["gap_currentaddress"] = new EntityReference("gap_address", targetAddressId);
            CopyAddressFields(contact, targetAddressId);
            


            res.IsUpdate = true;
            serv.Update(contact);
            logger.DebugFormat("Contact save (address link update) updated record {0}", res.CrmId);

          


            return res;

        }

        public virtual void CopyAddressFields(Entity contact, Guid gapaddressid)
        {
            logger.DebugFormat("CopyAddressFields called with addressid {0}", gapaddressid);
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity address = serv.Retrieve("gap_address", gapaddressid, new ColumnSet(new string[] {
                    "gap_line1", "gap_line2", "gap_line3", "gap_citytown",
                    "gap_postcode", "gap_county", "gap_country"}));

            contact["address1_line1"] = SafeEntityString( address, "gap_line1");
            contact["address1_line2"] = SafeEntityString( address, "gap_line2");
            contact["address1_line3"] = SafeEntityString( address, "gap_line3");
            contact["address1_city"] = SafeEntityString( address, "gap_citytown");
            contact["address1_postalcode"] = SafeEntityString( address, "gap_postcode");
            contact["address1_county"] = SafeEntityString( address, "gap_county");
            contact["address1_country"] = SafeEntityString( address, "gap_country");

           
        }

        public virtual CrmSaveResult SaveOrganisationAddressLink(Referrals.ContactAddressLinkRow linkRow, Guid targetOrganisationId, Guid targetAddressId)
        {
            logger.DebugFormat("Save OrganisationAddressLink called, SourceContactAddressLinkID {0}", linkRow.SourceContactAddressLinkID);
            var res = new CrmSaveResult();
            res.CrmId = targetOrganisationId;

            // equivalent in CRM is just the FK from Contact to Address
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity account = serv.Retrieve("account", targetOrganisationId, new ColumnSet(new string[] {
                    "accountid", "gap_currentaddress"}));

            if (account.Attributes.Contains("gap_currentaddress"))
            {
                Guid currentAddressId = ((EntityReference)account["gap_currentaddress"]).Id;

                if (currentAddressId == targetAddressId)
                {
                    logger.DebugFormat("SaveOrganisationAddressLink: org {0} already linked to address {1} so leaving as it is",
                        targetOrganisationId, targetAddressId);
                    return res;
                }
            }
            else
            {
                logger.DebugFormat("SaveOrganisationAddressLink: org {0} not linked to any address yet", targetOrganisationId);
            }

            account["gap_currentaddress"] = new EntityReference("gap_address", targetAddressId);
            // TODO might need to copy address into matching fields on org entity


            res.IsUpdate = true;
            serv.Update(account);
            logger.DebugFormat("Organisation save (address link update) updated record {0}", res.CrmId);

         
            return res;

        }

        private string SafeString(object input)
        {
            if (input == DBNull.Value)
                return null;
            if (input == null)
                return null;
            return input.ToString();            
        }

        private string SafeEntityString(Entity e, string attributename)
        {
            if (e.Contains(attributename))
                return e[attributename].ToString();
            else
                return null;

        }

        private object SafeStringToDate(object dateinput)
        {
            if (dateinput == DBNull.Value)
                return null;
            if (dateinput == null)
                return null;
            string inputAsString = (string)dateinput;
            if (string.IsNullOrEmpty(inputAsString))
                return null;
            return DateTime.Parse(inputAsString);
        }

        public virtual CrmSaveResult SaveContactAlert(Referrals.ContactAlertRow alertRow, Guid targetContactId)
        {
            
            logger.DebugFormat("Save contact called");
            var res = new CrmSaveResult();
            var serv = _connectionPool.GetCrmConnectionCached();

            Entity alert = new Entity("gap_alert");


            if (!string.IsNullOrEmpty(alertRow.TargetContactAlertID))
            {
                res.IsUpdate = true;
                alert["gap_alertid"] = new Guid(alertRow.TargetContactAlertID);
                res.CrmId = (Guid)alert["gap_alertid"];
            }
            alert["gap_person"] = new EntityReference("contact", targetContactId);

            var alertTypeMap = _picklistTasks.GetAlertTypes();
            var alertMatch = alertTypeMap.FirstOrDefault(a => a.PrescientCode == alertRow.Type);
            if (alertMatch == null)
                throw new ApplicationException(string.Format("SaveContactAlert: alert SourceContactAlertID {0} SyncID {1} has AlertType {2} but could not map that to a CRM alert type",
                    alertRow.SourceContactAlertID, alertRow.SyncID, alertRow.Type));

            alert["gap_alerttype"] = new OptionSetValue(alertMatch.CrmOptionSetValue);

            alert["gap_prescientalertid"] = SafeString(alertRow["SourceContactAlertID"]);
            alert["gap_datealertadded"] =SafeStringToDate(alertRow["DateAlertAdded"]);
            alert["gap_alertdescription"] = SafeString(alertRow.Notes);
            alert["gap_datealertclosed"] = SafeStringToDate(alertRow["DateAlertClosed"]);

            var caseWorkerList = _picklistTasks.GetCaseworkers();
            var matchCaseWorker = caseWorkerList.FirstOrDefault(c => c.PrescientInitialsId == alertRow.RaisedBy);
            if (matchCaseWorker == null)
            {
                // todo make this throw an error
                // cant do that yet because data coming from presscient doesnt match
                logger.WarnFormat("SaveContactAlert: alert SourceContactAlertID {0} SyncID {1} has RaisedBy {2} which could not be found in CRM CaseWorker list",
                    alertRow.SourceContactAlertID, alertRow.SyncID, alertRow.RaisedBy);
            }
            else
                alert["gap_caseworker"] = new EntityReference("gap_caseworker", matchCaseWorker.CaseWorkerId);
                        
            if (res.IsInsert)
            {
                res.CrmId = serv.Create(alert);
                logger.DebugFormat("Alert save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(alert);
                logger.DebugFormat("Alert save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;




        }

        public virtual CrmSaveResult UpdateContactAlertPrescientId(Guid alertId, string prescientId)
        {
            logger.DebugFormat("UpdateContactAlertPrescientId called with alertId {0} prescientId {1}",
                alertId, prescientId);
            var res = new CrmSaveResult();
            
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity alert = serv.Retrieve("gap_alert", alertId, new ColumnSet(new string[] {
                    "gap_alertid", "gap_prescientalertid"}));

            alert["gap_prescientalertid"] = prescientId;

            serv.Update(alert);
            res.CrmId = alertId;
            res.IsUpdate = true;

            return res;
        }

        public virtual CrmSaveResult UpdateOrganisationPrescientId(Guid accountId, string prescientId)
        {
            logger.DebugFormat("UpdateOrganisationPrescientId called with accountId {0} prescientId {1}",
                accountId, prescientId);
            var res = new CrmSaveResult();

            var serv = _connectionPool.GetCrmConnectionCached();
            Entity account = serv.Retrieve("account", accountId, new ColumnSet(new string[] {
                    "accountid", "gap_prescientid"}));

            account["gap_prescientid"] = prescientId;

            serv.Update(account);
            res.CrmId = accountId;
            res.IsUpdate = true;

            return res;
        }

        public virtual CrmSaveResult SaveCasePresentingProblem(Referrals.CasePresentingProblemRow presentingProblemRow, Guid targetCaseId)
        {
            var res = new CrmSaveResult();

            var presentingProblemId = GetPresentingProblemCrmId(presentingProblemRow.Description);            
            if (presentingProblemId == null)
            {
                throw new ApplicationException(
                    String.Format("Presenting problem '{0}' not found in CRM", presentingProblemRow.Description));
            }

            var linkAlreadyExists = CaseHasPresentingProblemLink(targetCaseId, presentingProblemId.Value);

            if (linkAlreadyExists)
            {
                logger.DebugFormat(
                    "A link between Presenting Problem '{0}' and case {1} already exists - skipping it",
                    presentingProblemRow.Description,
                    targetCaseId);

                // call that an update
                res.IsUpdate = true;
                res.CrmId = presentingProblemId.Value;

                return res;
            }

            var createNewLinkRequest = new AssociateEntitiesRequest();

            // think this is the same as presentingproblemId that we already have?
            var presentingProblemRef = GetPresentingProblemRef(presentingProblemRow.Description);
            createNewLinkRequest.Moniker1 = new EntityReference("gap_presentingproblems", presentingProblemRef);
            createNewLinkRequest.Moniker2 = new EntityReference("incident", targetCaseId);
            createNewLinkRequest.RelationshipName = "gap_gap_presentingproblems_incident";

            var serv = _connectionPool.GetCrmConnectionCached();
            var result = serv.Execute(createNewLinkRequest);

            res.IsUpdate = false;
            res.CrmId = presentingProblemRef;
            return res;
        }

        /// <summary>
        /// Removes the specified PresentingProblem from the specified Case
        /// </summary>
        /// <param name="targetCaseId"></param>
        /// <param name="targetPresentingProblemId"></param>
        public virtual CrmSaveResult DeleteCasePresentingProblem(Guid targetCaseId, Guid targetPresentingProblemId)
        {
            var res = new CrmSaveResult();

            var linkAlreadyExists = CaseHasPresentingProblemLink(targetCaseId, targetPresentingProblemId);

            if (!linkAlreadyExists)
            {
                throw new ApplicationException(
                    String.Format(
                        "A link between Presenting Problem '{0}' and case {1} does not already exist - cannot delete it",
                            targetPresentingProblemId,
                            targetCaseId)
                );
            }

            var removeLinkRequest = new DisassociateEntitiesRequest();

            removeLinkRequest.Moniker1 = new EntityReference("gap_presentingproblems", targetPresentingProblemId);
            removeLinkRequest.Moniker2 = new EntityReference("incident", targetCaseId);
            removeLinkRequest.RelationshipName = "gap_gap_presentingproblems_incident";

            var serv = _connectionPool.GetCrmConnectionCached();
            var result = serv.Execute(removeLinkRequest);

            return res;
        }

        private bool CaseHasPresentingProblemLink(Guid targetCaseId, Guid presentingProblemId)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            ConditionExpression conditionName = new ConditionExpression
            {
                AttributeName = "incidentid",
                Operator = ConditionOperator.Equal
            };

            conditionName.Values.Add(targetCaseId);

            FilterExpression selectByName = new FilterExpression();
            selectByName.Conditions.Add(conditionName);

            //Create nested link entity and apply filter criteria
            LinkEntity nestedLinkEntity = new LinkEntity
            {
                LinkToEntityName = "incident",
                LinkFromAttributeName = "incidentid",
                LinkToAttributeName = "incidentid",
                LinkCriteria = selectByName
            };

            var nestedCondition = new ConditionExpression
            {
                AttributeName = "gap_presentingproblemsid",
                Operator = ConditionOperator.Equal
            };

            nestedCondition.Values.Add(presentingProblemId);

            var nestedFilter = new FilterExpression();
            nestedFilter.Conditions.Add(nestedCondition);

            //Create the nested link entities
            LinkEntity intersectEntity = new LinkEntity
            {
                LinkToEntityName = "gap_gap_presentingproblems_incident",
                LinkFromAttributeName = "incidentid",
                LinkToAttributeName = "incidentid",
                LinkCriteria = nestedFilter
            };

            intersectEntity.LinkEntities.Add(nestedLinkEntity);

            //Create Query expression and set the entity type to lead
            QueryExpression expression = new QueryExpression
            {
                EntityName = "incident"
            };

            expression.LinkEntities.Add(intersectEntity);

            var checkIfLinkExistsRequest = new RetrieveMultipleRequest
            {
                Query = expression
            };

            var response = (RetrieveMultipleResponse)serv.Execute(checkIfLinkExistsRequest);

            return response.EntityCollection.Entities.Count > 0;
        }

        public virtual CrmSaveResult SaveCase(Referrals.CaseRow caseRow, Guid targetContactId)
        {

            logger.DebugFormat("Save case called for sourceCaseId {0}", caseRow.SourceCaseID);
            var res = new CrmSaveResult();
            var serv = _connectionPool.GetCrmConnectionCached();

            Entity incident = new Entity("incident");


            if (!string.IsNullOrEmpty(caseRow.TargetCaseID))
            {
                res.IsUpdate = true;
                incident["incidentid"] = new Guid(caseRow.TargetCaseID);
                res.CrmId = (Guid)incident["incidentid"];
            }

            // only set contact if new record (otherwise various workflows get fired)
            if (res.IsInsert)
            {
                incident["customerid"] = new EntityReference("contact", targetContactId);
                incident["gap_clientcontactid"] = new EntityReference("contact", targetContactId);
            }


            incident["gap_prescientcaseid"] = SafeString(caseRow["SourceCaseID"]);
            incident["gap_openingdate"] = SafeStringToDate(caseRow["StartDate"]);
            incident["gap_closuredate"] = SafeStringToDate(caseRow["EndDate"]);

            //TODO description?
            // if too many fields are null, throw an error
            if ((caseRow.IsCaseTypeCodeNull() || string.IsNullOrEmpty(caseRow.CaseTypeCode))
                && (caseRow.IsCentreOrServiceCodeNull() || string.IsNullOrEmpty(caseRow.CentreOrServiceCode)))
                throw new ApplicationException(string.Format("Case SourceCaseID {0} has blank CaseType and blank CentreOrService - possibly sent wrongly?",
                    caseRow.SourceCaseID));

            if (!caseRow.IsCaseTypeCodeNull() && !string.IsNullOrEmpty(caseRow.CaseTypeCode))
            {
                /* old matching based on caseRow.CaseTypeCode

                    var matchCaseType = _picklistTasks.GetCaseTypes().FirstOrDefault(c => c.Code == caseRow.CaseTypeCode);
                    if (matchCaseType == null)
                        throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} SyncID {1} has CaseTypeCode {2} which could not be found in CRM Case Type list",
                            caseRow.SourceCaseID, caseRow.SyncID, caseRow.CaseTypeCode));
                    incident["gap_mattertype"] = new EntityReference("gap_mattertype", matchCaseType.CrmId);
                */
                if (caseRow.CaseTypeCode.Equals("47") || caseRow.CaseTypeCode.Equals("48"))
                {

                    // for Clarinet - we map to different CLA case types in CRM based on
                    // ClaMaxMinutes which has been calculated (in sql) based on Determination and eligibility codes
                    int claMaxMinutes = caseRow.ClaMaxMinutes;
                    CrmPicklistTasks.CrmCaseType matchCaseType = null;
                    String targetFixedFeeCode = GetFixedFeeCode(caseRow.CaseTypeCode, claMaxMinutes);
                    
                    if (targetFixedFeeCode != null)
                    {
                        matchCaseType = _picklistTasks.GetCaseTypes().FirstOrDefault(c => !String.IsNullOrEmpty(c.FixedFeeCodeSos) && c.FixedFeeCodeSos.Equals(targetFixedFeeCode));
                        if (matchCaseType == null)
                            throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has CaseTypeCode {1} and CLAMaxMinutes {2} but could not find a case type with SosFixedFeeCode {3}",
                                caseRow.SourceCaseID, caseRow.CaseTypeCode, caseRow.ClaMaxMinutes, targetFixedFeeCode));
                    }
                    else
                    {
                        logger.WarnFormat("SaveCase: Case sourceCaseID {0} has CaseTypeCode {1} and CLAMaxMinutes {2} so could not work out SOSFixedFeeCode. Matching to CaseType using CaseTypeCode",
                                caseRow.SourceCaseID, caseRow.CaseTypeCode, caseRow.ClaMaxMinutes);
                        matchCaseType = _picklistTasks.GetCaseTypes().FirstOrDefault(c => c.Code == caseRow.CaseTypeCode);
                        if (matchCaseType == null)
                            throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} SyncID {1} has CaseTypeCode {2} which could not be found in CRM Case Type list",
                                caseRow.SourceCaseID, caseRow.SyncID, caseRow.CaseTypeCode));
                    }
                    incident["gap_mattertype"] = new EntityReference("gap_mattertype", matchCaseType.CrmId);
                }
                else
                {
                    logger.WarnFormat("Case SourceCaseID {0} has non-CLA CaseTypeCode of {1} (should be 47 or 48) ",
                        caseRow.SourceCaseID, caseRow.CaseTypeCode);
                    var matchCaseType = _picklistTasks.GetCaseTypes().FirstOrDefault(c => c.Code == caseRow.CaseTypeCode);
                    if (matchCaseType == null)
                        throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} SyncID {1} has CaseTypeCode {2} which could not be found in CRM Case Type list",
                            caseRow.SourceCaseID, caseRow.SyncID, caseRow.CaseTypeCode));
                    incident["gap_mattertype"] = new EntityReference("gap_mattertype", matchCaseType.CrmId);
                }
            }
            else
                logger.WarnFormat("Case SourceCaseID {0} has blank CaseTypeCode",
                    caseRow.SourceCaseID);
                

            if (!caseRow.IsOwnerCodeNull())
            {
                var caseWorkerList = _picklistTasks.GetCaseworkers();
                var matchCaseWorker = caseWorkerList.FirstOrDefault(c => c.PrescientInitialsId == caseRow.OwnerCode);
                if (matchCaseWorker == null)
                {
                    // todo make this throw an error
                    // cant do that yet because data coming from presscient doesnt match
                    logger.WarnFormat(" SaveCase: Case sourceCaseID {0} SyncID {1} has OwnerCode {2} which could not be found in CRM CaseWorker list - using System England instead",
                        caseRow.SourceCaseID, caseRow.SyncID, caseRow.OwnerCode);

                    matchCaseWorker = GetSystemCaseworker();
                }
                
                incident["gap_caseworkerid"] = new EntityReference("gap_caseworker", matchCaseWorker.CaseWorkerId);
            }

            if (!caseRow.IsStatusCodeNull())
            {
                var caseStatusMap = _picklistTasks.GetCaseStatuses();
                var statusMatch = caseStatusMap.FirstOrDefault(a => a.PrescientCode == caseRow.StatusCode);
                if (statusMatch == null)
                    throw new ApplicationException(string.Format("SaveCase: Case SourceCaseID {0} SyncID {1} has StatusCode {2} but could not map that to a CRM status code",
                        caseRow.SourceCaseID, caseRow.SyncID, caseRow.StatusCode));

                incident["statuscode"] = new OptionSetValue(statusMatch.CrmOptionSetValue);

            }
            else
                logger.WarnFormat("SaveCase: Case sourceCaseID {0} SyncID {1} has blank status",
                    caseRow.SourceCaseID, caseRow.SyncID);

            
            if (!caseRow.IsContractNull() && !string.IsNullOrEmpty(caseRow.Contract))
            {


                var matchContract = _picklistTasks.GetContracts().FirstOrDefault(c => c.Code == caseRow.Contract);
                if (matchContract == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} SyncID {1} has Contract {2} which could not be found in CRM",
                        caseRow.SourceCaseID, caseRow.SyncID, caseRow.Contract));
                incident["gap_contract"] = new EntityReference("gap_sheltercontract", matchContract.CrmId);
            }
            else
                logger.WarnFormat("SaveCase: Case sourceCaseID {0} SyncID {1} has blank contract",
                    caseRow.SourceCaseID, caseRow.SyncID);

            // centre is hard coded for all CLA
            string centreToUse = "Civil Legal Advice (Shelter)";
            var matchCentre = _picklistTasks.GetCentres().FirstOrDefault(c => c.Name == centreToUse);
            if (matchCentre == null)
                    throw new ApplicationException(string.Format("SaveCase: Cannot find CLA Centre {0} in Org list",
                        centreToUse));
            incident["gap_centerorservice"] = new EntityReference("account", matchCentre.CrmId);
            
            // no longers a stub because clarinet
            incident["gap_cicmstubrecord"] = false; 
            incident["gap_formofcontact"] = null;

            // new mappings for Clarinet
            if (!caseRow.Isstage_reachedNull() && !string.IsNullOrWhiteSpace(caseRow.stage_reached))
            {
                var matchStage = _picklistTasks.GetLAAStageReached().FirstOrDefault(p => p.Code == caseRow.stage_reached);
                if (matchStage == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has stage_reached {1} which could not be found in CRM ",
                        caseRow.SourceCaseID, caseRow.stage_reached));

                incident["gap_laastagereached"] = new EntityReference("gap_laastagereached", matchStage.CrmId);
            }

            if (!caseRow.Isoutcome_for_clientNull() && !string.IsNullOrWhiteSpace(caseRow.outcome_for_client))
            {
                var matchOutcome = _picklistTasks.GetLAAOutcomes().FirstOrDefault(p => p.Code == caseRow.outcome_for_client);
                if (matchOutcome == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has outcome_for_client {1} which could not be found in CRM ",
                        caseRow.SourceCaseID, caseRow.outcome_for_client));

                incident["gap_laaoutcome"] = new EntityReference("gap_laaoutcome", matchOutcome.CrmId);
            }

            // matter type 1 and 2
            // Crm object is LAA Matter Type, codes dont match what is used in Prescient so needs a manual mapping
            if (!caseRow.Ismatter_type_1Null() && !string.IsNullOrWhiteSpace(caseRow.matter_type_1))
            {
                String prescientMatterType = caseRow.matter_type_1;
                String prescientFieldName = "matter_type_1";
                String crmFieldName = "gap_mattertype1";
                var level1Map = _picklistTasks.GetLaaMatterTypeMappingLevel1();
                ConvertLaaMatterType(caseRow, incident, prescientMatterType, prescientFieldName, crmFieldName, level1Map);
            }
            if (!caseRow.Ismatter_type_2Null() && !string.IsNullOrWhiteSpace(caseRow.matter_type_2))
            {
                String prescientMatterType = caseRow.matter_type_2;
                String prescientFieldName = "matter_type_2";
                String crmFieldName = "gap_mattertype2";
                var level2Map = _picklistTasks.GetLaaMatterTypeMappingLevel2();
                ConvertLaaMatterType(caseRow, incident, prescientMatterType, prescientFieldName, crmFieldName, level2Map);
            }

            // eligilbity codes
            // pick.GetEligibilityCodes(), match on Description
            if (!caseRow.Iseligible_client_indicatorNull() && !string.IsNullOrEmpty(caseRow.eligible_client_indicator))
            {
                var matchEligiblity = _picklistTasks.GetEligibilityCodes().FirstOrDefault(p => p.Description == caseRow.eligible_client_indicator);
                if (matchEligiblity == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has eligible_client_indicator {1} which could not be found in CRM (desc field of bst_eligibilitycode optionset)",
                        caseRow.SourceCaseID, caseRow.eligible_client_indicator));

                incident["bst_eligibilitycode"] = new OptionSetValue(matchEligiblity.CrmOptionSetValue);
            }

            // determination
            // pick.GetDeterminationCodes(), match on Description
            if (!caseRow.IsdeterminationNull() && !string.IsNullOrEmpty(caseRow.determination))
            {
                var matchDet = _picklistTasks.GetDeterminationCodes().FirstOrDefault(p => p.Description == caseRow.determination);
                if (matchDet == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has determination {1} which could not be found in CRM (desc field of bst_determination optionset)",
                        caseRow.SourceCaseID, caseRow.determination));

                incident["bst_determination"] = new OptionSetValue(matchDet.CrmOptionSetValue);
            }



            if (!caseRow.Ismedia_codeNull() && !string.IsNullOrEmpty(caseRow.media_code))
            {
                var medVal = caseRow.media_code.ToLower();
                // fix the obvious truncated ones
                if (medVal == "director")
                    medVal = "directory";
                if (medVal == "othersea")
                    medVal = "other search";
                if (medVal == "lawcent")
                    medVal = "law centre";
                

                var matchDet = _picklistTasks.GetMediaCodes().FirstOrDefault(p => p.Description == medVal);
                if (matchDet == null)
                {
                    logger.DebugFormat("SaveCase: Case sourceCaseID {0} has media code {1} which could not be found in CRM (desc field of bst_mediacode optionset) - will try Label match",
                        caseRow.SourceCaseID, caseRow.media_code);
                    if (!caseRow.IsMediaCodeDescriptionNull() && !string.IsNullOrEmpty(caseRow.MediaCodeDescription))
                    {
                        matchDet = _picklistTasks.GetMediaCodes().FirstOrDefault(p => p.Label.ToLower() == caseRow.MediaCodeDescription.ToLower());
                        if (matchDet == null)
                            logger.DebugFormat("SaveCase: Case sourceCaseID {0} has media code {1} with Description {2} which could not be matched to Labels in CRM (label field of bst_mediacode optionset)",
                                caseRow.SourceCaseID, caseRow.media_code, caseRow.MediaCodeDescription);
                        else
                            logger.DebugFormat("SaveCase: Case sourceCaseID {0} has media code {1} with Description {2} which matched to code {3} in CRM via label",
                                caseRow.SourceCaseID, caseRow.media_code, caseRow.MediaCodeDescription, matchDet.Description);

                    }
                    else
                        logger.DebugFormat("SaveCase: Case sourceCaseID {0} has media code {1} but blank media code description",
                        caseRow.SourceCaseID, caseRow.media_code);
                }
                if (matchDet != null)
                    incident["bst_mediacode"] = new OptionSetValue(matchDet.CrmOptionSetValue);
            }

            // cla ref
            if (!caseRow.Iscla_ref_numberNull() && !string.IsNullOrEmpty(caseRow.cla_ref_number))
                incident["gap_ecfreferencenumber"] = caseRow.cla_ref_number;
            // concluded date
            if (!caseRow.Iscase_concluded_dateNull())
                incident["gap_concludeddate"] = caseRow.case_concluded_date;

            // cicm ref - either original cicm ref or new generated one
            // might not need this
            /*
            if (!caseRow.IsFakeCicmIdNull() && caseRow.FakeCicmId > 0)
                incident["gap_cicmrecordid"] = caseRow.FakeCicmId;
            */

            // bst_referralcodes / signposting - map by label
            if (!caseRow.IsSignpostingDescriptionNull() && !string.IsNullOrEmpty(caseRow.SignpostingDescription))
            {
                var match = _picklistTasks.GetReferralCodes().FirstOrDefault(p => p.Label.ToLower() == caseRow.SignpostingDescription.ToLower());
                if (match == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has signposting/referral code {1} with desc {2}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.signposting, caseRow.SignpostingDescription));
                else
                    logger.DebugFormat("SaveCase: Case sourceCaseID {0} has signposting/referral code {1} with desc {2}, matched to CRM optionset {3}",
                        caseRow.SourceCaseID, caseRow.signposting, caseRow.SignpostingDescription, match.Label);

                incident["bst_referralcodes"] = new OptionSetValue(match.CrmOptionSetValue);
            }

            // bst_serviceadaptations - match to label
            if (!caseRow.IsServiceAdaptationDescriptionNull() && !string.IsNullOrEmpty(caseRow.ServiceAdaptationDescription))
            {
                var match = _picklistTasks.GetServiceAdaptations().FirstOrDefault(p => p.Label.ToLower() == caseRow.ServiceAdaptationDescription.ToLower());
                if (match == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has service adaptation {1} with desc {2}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.service_adaptation, caseRow.ServiceAdaptationDescription));

                incident["bst_serviceadaptations"] = new OptionSetValue(match.CrmOptionSetValue);
            }

            // chargeband is always 51
            var matchChargeBand = _picklistTasks.GetChargeBands().FirstOrDefault(p => p.Code == "51");
            if (matchChargeBand == null)
                throw new ApplicationException("Could not find CLA charge band (code 51) in CRM");
            incident["gap_chargeband"] = new EntityReference("gap_chargeband", matchChargeBand.CrmId);

            // supervisor
            if (!caseRow.IsSupervisorCodeNull() && !string.IsNullOrEmpty(caseRow.SupervisorCode))
            {
                var caseWorkerList = _picklistTasks.GetCaseworkers();
                var matchSupervisor = caseWorkerList.FirstOrDefault(c => c.PrescientInitialsId == caseRow.SupervisorCode);
                if (matchSupervisor == null)
                {
                    // warning
                    logger.WarnFormat(" SaveCase: Case sourceCaseID {0} SyncID {1} has SupervisorCode {2} which could not be found in CRM CaseWorker list",
                        caseRow.SourceCaseID, caseRow.SyncID, caseRow.SupervisorCode);
                }
                else
                    incident["gap_supervisor"] = new EntityReference("gap_caseworker", matchSupervisor.CaseWorkerId);
            }

            // gap_householdtype
            if (!caseRow.IsHouseholdTypeDescriptionNull() && !string.IsNullOrEmpty(caseRow.HouseholdTypeDescription))
            {
                var match = _picklistTasks.GetHouseholdTypes().FirstOrDefault(p => p.Label.ToLower() == caseRow.HouseholdTypeDescription.ToLower());
                if (match == null)
                    logger.WarnFormat("SaveCase: Case sourceCaseID {0} has household type desc {1}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.HouseholdTypeDescription);
                else
                    incident["gap_householdtype"] = new OptionSetValue(match.CrmOptionSetValue);
            }
            // gap_formofcontact
            if (!caseRow.IsFormOfContactDescriptionNull() && !string.IsNullOrEmpty(caseRow.FormOfContactDescription))
            {
                var match = _picklistTasks.GetFormOfContact().FirstOrDefault(p => p.Label.ToLower() == caseRow.FormOfContactDescription.ToLower());
                if (match == null)
                    logger.WarnFormat("SaveCase: Case sourceCaseID {0} has form of Contact type desc {1}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.FormOfContactDescription);
                else
                    incident["gap_formofcontact"] = new OptionSetValue(match.CrmOptionSetValue);
            }
            // gap_homelessnessstatus
            if (!caseRow.IsHomelessnessStatusDescriptionNull() && !string.IsNullOrEmpty(caseRow.HomelessnessStatusDescription))
            {
                var match = _picklistTasks.GetHomelessnessStatus().FirstOrDefault(p => p.Label.ToLower() == caseRow.HomelessnessStatusDescription.ToLower());
                if (match == null)
                    logger.WarnFormat("SaveCase: Case sourceCaseID {0} has Homelessness Status desc {1}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.HomelessnessStatusDescription);
                else
                    incident["gap_homelessnessstatus"] = new OptionSetValue(match.CrmOptionSetValue);
            }
            // gap_tenure
            if (!caseRow.IsTenureDescriptionNull() && !string.IsNullOrEmpty(caseRow.TenureDescription))
            {
                var match = _picklistTasks.GetTenure().FirstOrDefault(p => p.Label.ToLower() == caseRow.TenureDescription.ToLower());
                if (match == null)
                    logger.WarnFormat("SaveCase: Case sourceCaseID {0} has Tenure desc {1}, could not match to labels on crm optionset",
                        caseRow.SourceCaseID, caseRow.TenureDescription);
                else
                    incident["gap_tenure"] = new OptionSetValue(match.CrmOptionSetValue);
            }

            if (res.IsInsert)
            {
                res.CrmId = serv.Create(incident);
                logger.DebugFormat("Case save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(incident);
                logger.DebugFormat("Case save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;




        }

        private void ConvertLaaMatterType(Referrals.CaseRow caseRow, Entity incident, string prescientMatterType, string prescientFieldName, string crmFieldName, Dictionary<string,string> mapping)
        {
            if (mapping.ContainsKey(prescientMatterType))
            {
                string crmMatterTypeCode = mapping[prescientMatterType];
                var matchMatterType = _picklistTasks.GetLaaMatterTypes().FirstOrDefault(p => p.Code == crmMatterTypeCode);
                if (matchMatterType == null)
                    throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has {1} {2} which maps to {3} but {3} not be found in CRM list of LAA Matter Types",
                    caseRow.SourceCaseID, prescientFieldName, prescientMatterType, crmMatterTypeCode));

                incident[crmFieldName] = new EntityReference("gap_spancode1", matchMatterType.CrmId);
                logger.DebugFormat("SaveCase: Case sourceCaseID {0} has {1} {2} which maps to {3} which in CRM has guid {4}",
                    caseRow.SourceCaseID, prescientFieldName, prescientMatterType, crmMatterTypeCode, matchMatterType.CrmId);
            }
            else
            {
                throw new ApplicationException(string.Format("SaveCase: Case sourceCaseID {0} has {1} {2} which could not be found in the GetLaaMatterTypeMappingLevel1/2() map",
                        caseRow.SourceCaseID, prescientFieldName, caseRow.matter_type_1));
            }
        }

        // for CLA migration, returns the right SOS Fixed Fee Code based on case type and max minutes
        // this is then used to find right Case Type in Crm
        private string GetFixedFeeCode(string caseTypeCode, int claMaxMinutes)
        {
            if (caseTypeCode.Equals("47"))
            {
                if (claMaxMinutes == 18)
                    return "HOUCLA18";
                else if (claMaxMinutes == 132)
                    return "HOUCLA132";
                else if (claMaxMinutes == 156)
                    return "HOUCLA156";
                else
                    return null;
            }
            else if (caseTypeCode.Equals("48"))
            {
                if (claMaxMinutes == 18)
                    return "DEBCLA18";
                else if (claMaxMinutes == 132)
                    return "DEBCLA132";
                else if (claMaxMinutes == 156)
                    return "DEBCLA156";
                else
                    return null;
            }
            return null;
        }

        public virtual CrmSaveResult SaveTimeRecord(Referrals.TimeRecordRow trRow, Guid targetCaseId)
        {

            logger.DebugFormat("Save TimeReocrd called for sourceTimeRecordId {0}", trRow.SourceTimeRecordID);
            var res = new CrmSaveResult();
            var serv = _connectionPool.GetCrmConnectionCached();

            Entity timeRecord = new Entity("gap_timerecord");


            if (!string.IsNullOrEmpty(trRow.TargetTimeRecordID))
            {
                res.IsUpdate = true;
                timeRecord["gap_timerecordid"] = new Guid(trRow.TargetTimeRecordID);
                res.CrmId = (Guid)timeRecord["gap_timerecordid"];
            }

            // only set CASE if new record 
            if (res.IsInsert)
            {
                timeRecord["gap_case"] = new EntityReference("incident", targetCaseId);
            }

            timeRecord["gap_date"] = trRow.Date;
            // TODO looks like hours and units also need filling in
            timeRecord["gap_minutes"] = trRow.Minutes;
            timeRecord["gap_hours"] = (Decimal)(trRow.Minutes / 60.0m);
            timeRecord["gap_units6min"] = (int)Math.Round((float)trRow.Minutes / 6.0f);
            if (trRow["NoteText"] != DBNull.Value)
            {
                if (trRow.NoteText != null && trRow.NoteText.Length > 400)
                    trRow.NoteText = trRow.NoteText.Substring(0, 400);

                timeRecord["gap_description"] = trRow.NoteText;
            }

            if (!trRow.IsFeeEarnerCodeNull())
            {
                var caseWorkerList = _picklistTasks.GetCaseworkers();
                var matchCaseWorker = caseWorkerList.FirstOrDefault(c => c.PrescientInitialsId == trRow.FeeEarnerCode);
                if (matchCaseWorker == null)
                {
                    // use 7776 if caseworker not found

                    String safeDate = "null";
                    if (!trRow.IsDateNull())
                        safeDate = trRow.Date.ToString("dd/MMM/yyyy");
                    logger.WarnFormat(" SaveTimeRecord: TimeRec sourceTimeRecordID {0} dated {2} has FeeEarnerCode {1} which could not be found in CRM CaseWorker list - using System England instead",
                        trRow.SourceTimeRecordID, trRow.FeeEarnerCode, safeDate);

                    matchCaseWorker = GetSystemCaseworker();
                }
                timeRecord["gap_caseworker"] = new EntityReference("gap_caseworker", matchCaseWorker.CaseWorkerId);
            }
            if (!trRow.IsActivityCodeNull())
            {
                var activityMap = _picklistTasks.GetActivities();
                var activityMatch = activityMap.FirstOrDefault(a => a.Code== trRow.ActivityCode);
                if (activityMatch == null)
                    throw new ApplicationException(string.Format("SaveTimeRecord: TimeRec sourceTimeRecordID {0} has ActivityCode {1} which could not be found in CRM Activity List",
                        trRow.SourceTimeRecordID, trRow.ActivityCode));

                timeRecord["gap_activitytype"] = new EntityReference("gap_activitytype", activityMatch.CrmId);

            }
            else
                logger.WarnFormat("SaveTimeRecord: TimeRec sourceTimeRecordID {0} has blank Activitycode",
                        trRow.SourceTimeRecordID);


            // in prescient, time_status = 2 means billed
            int[] commonTimeStatusReasons = _picklistTasks.GetCommonTimeStatusReasons();
            if (!trRow.IsTimeStatusNull() && trRow.TimeStatus == 2)
            {
                timeRecord["statuscode"] = new OptionSetValue(commonTimeStatusReasons[1]);
            }
            else
            {
                timeRecord["statuscode"] = new OptionSetValue(commonTimeStatusReasons[0]);
            }



            if (res.IsInsert)
            {
                res.CrmId = serv.Create(timeRecord);
                logger.DebugFormat("Time Record save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(timeRecord);
                logger.DebugFormat("Time Record save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;




        }


        public CrmPicklistTasks.CrmCaseWorker GetSystemCaseworker()
        {
            var matchCw = _picklistTasks.GetCaseworkers().FirstOrDefault(c => c.PrescientInitialsId == "7776");
            if (matchCw == null)
                throw new ApplicationException("GetSystemCaseworker(): could not locate CRM 'System England' Caseworker (lookng for Prescient Initials Id = 7776)");
            return matchCw;
        }

        public virtual CrmSaveResult SaveCaseContactLink(Referrals.CaseContactLinkRow linkRow, Guid targetContactOrOrgId, Guid targetCaseId, bool targetIsContact, bool targetIsOrg)
        {
            logger.DebugFormat("Save CasecontactLink called for linkRow {0}", linkRow.SourceCaseContactLinkID);
            if (!targetIsContact && !targetIsOrg)
                throw new ApplicationException(string.Format("SaveCaseContactLink SyncID {0} sourceID {1} cannot set both targetIsContact and targetIsOrg to false",
                    linkRow.SyncID, linkRow.SourceCaseContactLinkID));

            var res = new CrmSaveResult();
            var serv = _connectionPool.GetCrmConnectionCached();

            Entity connection = new Entity("connection");


            if (!string.IsNullOrEmpty(linkRow.TargetCaseContactLinkID))
            {
                res.IsUpdate = true;
                connection["connectionid"] = new Guid(linkRow.TargetCaseContactLinkID);
                res.CrmId = (Guid)connection["connectionid"];
            }

            if (targetIsContact)
            {
                connection["record1id"] = new EntityReference("contact", targetContactOrOrgId);
                connection["record1objecttypecode"] = new OptionSetValue(2);
            }
            else if (targetIsOrg)
            {
                connection["record1id"] = new EntityReference("account", targetContactOrOrgId);
                connection["record1objecttypecode"] = new OptionSetValue(1);
            }

            connection["record2id"] = new EntityReference("incident", targetCaseId);
            connection["record2objecttypecode"] = new OptionSetValue(122);

            if (!linkRow.IsRoleCodeNull())
            {
                var connectionRolePair = _picklistTasks.GetCaseContactConnectionRolePair(linkRow.RoleCode);
                if (connectionRolePair.ContactConnectionRole.HasValue)
                    connection["record1roleid"] = new EntityReference("connectionrole", connectionRolePair.ContactConnectionRole.Value);
                else
                    logger.WarnFormat("ContactCaseLink SyncID {0} SourceID {1} has RoleCode {2} which could not be mapped to a CRM role code (on the Contact side)",
                        linkRow.SyncID, linkRow.SourceCaseContactLinkID, linkRow.RoleCode);

                if (connectionRolePair.CaseConnectionRole.HasValue)
                    connection["record2roleid"] = new EntityReference("connectionrole", connectionRolePair.CaseConnectionRole.Value);
                else
                    logger.WarnFormat("ContactCaseLink SyncID {0} SourceID {1} has RoleCode {2} which could not be mapped to a CRM role code (on the Case side)",
                        linkRow.SyncID, linkRow.SourceCaseContactLinkID, linkRow.RoleCode);

            }
            else
                throw new ApplicationException(string.Format("ContactCaseLink SyncID {0} SourceID {1} has blank RoleCode",
                        linkRow.SyncID, linkRow.SourceCaseContactLinkID));


            if (res.IsInsert)
            {
                res.CrmId = serv.Create(connection);
                logger.DebugFormat("Connection save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(connection);
                logger.DebugFormat("Connection save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;

        }


        

        public virtual CrmSaveResult SaveContactContactLink(Referrals.ContactContactLinkRow linkRow, Guid targetContactOrOrgId, bool targetIsContact, bool targetIsOrg)
        {
            logger.DebugFormat("Save ContactContactLink called for linkRow {0}", linkRow.SourceContactContactLinkID);
            if (!targetIsContact && !targetIsOrg)
                throw new ApplicationException(string.Format("SaveCaseContactLink SyncID {0} sourceID {1} cannot set both targetIsContact and targetIsOrg to false",
                    linkRow.SyncID, linkRow.SourceContactContactLinkID));

            var res = new CrmSaveResult();
            var serv = _connectionPool.GetCrmConnectionCached();

            Entity connection = new Entity("connection");


            if (!string.IsNullOrEmpty(linkRow.TargetContactContactLinkID))
            {
                res.IsUpdate = true;
                connection["connectionid"] = new Guid(linkRow.TargetContactContactLinkID);
                res.CrmId = (Guid)connection["connectionid"];
            }

            if (targetIsContact)
            {
                connection["record1id"] = new EntityReference("contact", targetContactOrOrgId);
                connection["record1objecttypecode"] = new OptionSetValue(2);
            }
            else if (targetIsOrg)
            {
                connection["record1id"] = new EntityReference("account", targetContactOrOrgId);
                connection["record1objecttypecode"] = new OptionSetValue(1);
            }

            if (!linkRow.IsRoleCodeNull())
            {
                var roleMatch = _picklistTasks.GetConnectionRoles().FirstOrDefault(r => r.Name == linkRow.RoleCode);
                if (roleMatch != null)
                {
                    //connection["record1roleid"] = new EntityReference("connectionrole", role.ConnectionRoleId);
                    connection["record2roleid"] = new EntityReference("connectionrole", roleMatch.ConnectionRoleId);
                }
                else
                {
                    logger.WarnFormat("ContactCaseLink SyncID {0} SourceID {1} has RoleCode {2} which could not be mapped to a CRM role code",
                        linkRow.SyncID, linkRow.SourceContactContactLinkID, linkRow.RoleCode);
                }
            }

            //TODO: check objecttype code
            connection["record2id"] = new EntityReference("contact", targetContactOrOrgId);
            connection["record2objecttypecode"] = new OptionSetValue(122);



            if (res.IsInsert)
            {
                res.CrmId = serv.Create(connection);
                logger.DebugFormat("Connection save (create) returned ID {0}", res.CrmId.ToString());
            }
            else
            {
                serv.Update(connection);
                logger.DebugFormat("Connection save (update) updated record {0}", res.CrmId.ToString());
            }


            return res;

        }


        public virtual void DeleteEntityBasedOnTable(string tableName, Guid crmId)
        {
            // translate tablename to crm entityname
            string entityName = null;
            switch (tableName)
            {
                case "Contact":
                    throw new ApplicationException("Cannot translate Contact to crm entity - please pass ContactOrganisation or ContactIndividual instead");
                    break;
                case "ContactIndividual":
                    entityName = "contact";
                    break;
                case "ContactOrganisation":
                    entityName = "account";
                    break;
                case "Address":
                    entityName = "gap_address";
                    break;
                case "ContactAddressLink":
                    throw new ApplicationException("This method cannot delete ContactAddressLink records - call DeleteContactAddressLink or DeleteOrganisationAddressLink");
                    break;
                case "Case":
                    entityName = "incident";
                    break;
                case "CaseContactLink":
                    entityName = "connection";
                    break;
                case "ContactAlert":
                    entityName = "gap_alert";
                    break;
                case "CasePresentingProblem":
                    entityName = null; //"special measures!";
                    break;
                case "ContactContactLink":
                    entityName = "connection";
                    break;
            }

            if (entityName == null)
                throw new ApplicationException(string.Format("Could not translate table name {0} to a CRM entity", tableName));


            DeleteEntity(entityName, crmId);


        }

        public virtual void DeleteEntity(string entityName, Guid crmId)
        {
            logger.DebugFormat("DeleteEntity called for {0} {1}", entityName, crmId);
            var serv = _connectionPool.GetCrmConnectionCached();

            serv.Delete(entityName, crmId);

        }

        public virtual void DeleteContactAddressLink(Guid targetContactId)
        {
            logger.DebugFormat("DeleteContactAddressLink called, TargetContactID {0}", targetContactId);
            
            // this means just clearing the FK from Contact to Address
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity contact = serv.Retrieve("contact", targetContactId, new ColumnSet(new string[] {
                    "contactid", "gap_currentaddress"}));

            contact["gap_currentaddress"] = null;

            // also need to clear the copied address fields
            contact["address1_line1"] = null;
            contact["address1_line2"] = null;
            contact["address1_line3"] = null;
            contact["address1_city"] = null;
            contact["address1_postalcode"] = null;
            contact["address1_county"] = null;
            contact["address1_country"] = null;
           
            serv.Update(contact);
            logger.DebugFormat("Contact save (address link update) updated record {0}", targetContactId);

        }

        public virtual void DeleteOrganisationAddressLink(Guid targetOrganisationId)
        {
            logger.DebugFormat("DeleteOrganisationAddressLink called, TargetOrganisationID {0}", targetOrganisationId);

            // this means just clearing the FK from Contact to Address
            var serv = _connectionPool.GetCrmConnectionCached();
            Entity account = serv.Retrieve("account", targetOrganisationId, new ColumnSet(new string[] {
                    "accountid", "gap_currentaddress"}));

            account["gap_currentaddress"] = null;

            // TODO might need to clear address into matching fields on org entity
            
            serv.Update(account);
            logger.DebugFormat("Account save (address link update) updated record {0}", targetOrganisationId);

        }

        public GetAlertsToProcessResult GetAlertsToProcess()
        {
            logger.DebugFormat("GetAlertsToProcess called");
            var res = new GetAlertsToProcessResult();

            string fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' count='60'>"
                        + "<entity name='gap_alert'>"
                        + "<attribute name='gap_alertreference' />"
                        + "<attribute name='createdon' />"
                        + "<attribute name='gap_person' />"
                        + "<attribute name='gap_datealertclosed' />"
                        + "<attribute name='gap_datealertadded' />"
                        + "<attribute name='gap_alerttype' />"
                        + "<attribute name='createdby' />"
                        + "<attribute name='gap_alertid' />"
                        + "<attribute name='gap_sourceofinformation' />"
                        + "<attribute name='gap_prescientalertid' />"
                        + "<attribute name='gap_cicmrecordid' />"
                        + "<attribute name='gap_centre' />"
                        + "<attribute name='gap_caseworker' />"
                        + "<attribute name='gap_alertdescription' />"
                        + "<order attribute='gap_alertreference' descending='false' />"
                        + "<filter type='and'>"
                        + "<condition attribute='gap_prescientalertid' operator='null' />"
                        + "</filter>"
                        + "<link-entity name='contact' alias='ab' to='gap_person' from='contactid'>"
                        + "<filter type='and'> <condition attribute='gap_prescientcontactid' operator='not-null'/> </filter>"
                        + "</link-entity>"
                        + "</entity>"
                        + "</fetch>";

            var serv = _connectionPool.GetCrmConnectionCached();
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetchXml));

            res.ContactAlertDataTable = new Referrals.ContactAlertDataTable();
            res.FailedInfo = new List<Tuple<string, string>>();
            res.Failed = 0;
            res.Succeeded = 0;
            res.TotalCount = 0;
            foreach (Entity entLoop in fetchresults.Entities)
            {
                res.TotalCount += 1;
                string alertIDBeingProcessed = entLoop["gap_alertid"].ToString();
                try
                {
                    var alertrow = MakeAlertRow(res.ContactAlertDataTable, entLoop);
                    res.ContactAlertDataTable.AddContactAlertRow(alertrow);
                    res.Succeeded += 1;
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Error while extracting gap_alertid {0} from CRM", alertIDBeingProcessed), ex);
                    res.FailedInfo.Add(new Tuple<string, string>(alertIDBeingProcessed, ex.Message));
                    res.Failed += 1;
                }
            }
            logger.DebugFormat("Found {0} alerts that need processing, {1} read successfully, {2} failed", res.TotalCount, res.Succeeded, res.Failed);
            return res;
        }

        private Referrals.ContactAlertRow MakeAlertRow(Referrals.ContactAlertDataTable ret, Entity entLoop)
        {
            var alertrow = ret.NewContactAlertRow();
            alertrow.SourceContactAlertID = entLoop["gap_alertid"].ToString();
            logger.DebugFormat("MakeAlertRow: reading gap_alertid {0} for transfer to Prescient", alertrow.SourceContactAlertID);
            alertrow.SourceContactID = ((EntityReference)entLoop["gap_person"]).Id.ToString();
            alertrow.SyncID = "";
            alertrow.TargetContactAlertID = "";

            if (entLoop.Attributes.Contains("gap_alerttype"))
            {
                var alertTypeCrmId = ((OptionSetValue)entLoop["gap_alerttype"]).Value;

                var alertMatch = _picklistTasks.GetAlertTypes().FirstOrDefault(a => a.CrmOptionSetValue == alertTypeCrmId);
                if (alertMatch == null)
                    logger.WarnFormat("GetAlertsToProcess: Crm Alert {0} has AlertTypeID {1} which cannot be mapped to a prescient alert type",
                        alertrow.SourceContactAlertID, alertTypeCrmId);
                else
                {
                    alertrow.Type = alertMatch.PrescientCode;
                }
            }
            else
            {
                logger.WarnFormat("GetAlertsToProcess: Crm Alert {0} has blank AlertType",
                alertrow.SourceContactAlertID);
                alertrow.Type = "";
            }

            if (entLoop.Attributes.Contains("gap_caseworker"))
            {
                Guid caseWorkerCrmId = ((EntityReference)entLoop["gap_caseworker"]).Id;
                var cwMatch = _picklistTasks.GetCaseworkers().FirstOrDefault(c => c.CaseWorkerId == caseWorkerCrmId);
                if (cwMatch == null || string.IsNullOrEmpty(cwMatch.PrescientInitialsId))
                {
                    logger.WarnFormat("GetAlertsToProcess: Crm Alert {0} has CaseWorkder ID {1} which cannot be mapped to a prescient case worker",
                     alertrow.SourceContactAlertID, caseWorkerCrmId);
                    alertrow.RaisedBy = "";
                }
                else
                    alertrow.RaisedBy = cwMatch.PrescientInitialsId;
            }
            else
            {
                logger.WarnFormat("GetAlertsToProcess: Crm Alert {0} has blank CaseWorker",
                alertrow.SourceContactAlertID);
                alertrow.RaisedBy = "";
            }

            if (entLoop.Attributes.Contains("gap_alertdescription"))
                alertrow.Notes = SafeEntityString(entLoop, "gap_alertdescription");
            else
                alertrow.Notes = "";

            alertrow.DeletionFlag = "N";
            alertrow.DateAlertAdded = ((DateTime)entLoop["gap_datealertadded"]).ToString("dd/MMM/yyyy");
            if (entLoop.Attributes.Contains("gap_datealertclosed"))
                alertrow.DateAlertClosed = ((DateTime)entLoop["gap_datealertclosed"]).ToString("dd/MMM/yyyy");
            else
                alertrow.DateAlertClosed = "";

            return alertrow;
        }

        public void Reconnect()
        {
            logger.DebugFormat("Reconnect (crmTasks) called, flushing connections caches ...");
            CrmConnectionCache.FlushCachedConnections();
            this._connectionPool = new CrmConnectionCache();
            this._picklistTasks.Reconnect();
        }

        

        public class CrmSaveResult
        {
            public bool IsUpdate { get; set; }
            public bool IsInsert { get { return !IsUpdate; } }
            public Guid CrmId { get; set; }

        }

        public class GetAlertsToProcessResult
        {
            public int TotalCount { get; set; }
            public int Succeeded { get; set; }
            public int Failed {get; set;}
            public List<Tuple<string, string>> FailedInfo { get; set; }
            public Referrals.ContactAlertDataTable ContactAlertDataTable { get; set; }

        }
    }
}
