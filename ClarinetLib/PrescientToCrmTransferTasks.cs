﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClarinetLib.LocalModel;


namespace ClarinetLib
{

    public class PrescientToCrmTransferTasks
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CrmTasks _crmTasks;
        private ControlDataTasks _controlTasks;
        private HistoryWriter _historyWriter;
        private IgnoreList _ignoreList;
        private PrescientDatabaseTasks _prescientTasks;
      

        // this is the constructor you'd normally use
        public PrescientToCrmTransferTasks() : this(new CrmTasks(), new PrescientDatabaseTasks(), new HistoryWriter(), new IgnoreList(), new ControlDataTasks()) { }

        // this is the constructor to use for unit testing etc
        public PrescientToCrmTransferTasks(CrmTasks crmTasks,  PrescientDatabaseTasks pTasks, HistoryWriter histWriter, IgnoreList ignoreList, ControlDataTasks cTasks)
        {
            _crmTasks = crmTasks;
            _historyWriter = histWriter;
            _prescientTasks = pTasks;
            _ignoreList = ignoreList;
            _controlTasks = cTasks;

           
        }

        // this is another constructor to use for unit testing etc
        public PrescientToCrmTransferTasks(CrmTasks crmTasks, PrescientDatabaseTasks pTasks, HistoryWriter histWriter) : this(crmTasks, pTasks, histWriter, new IgnoreList(), new ControlDataTasks()) { }

        /*
        /// <summary>
        /// Main method of the class -
        /// gets data from Prescient via GetDetails() web service call
        /// sends data to CRM and records the CrmIds generated
        /// Sends the CrmIds back to Prescient via web service call
        /// </summary>
        public List<TransferResult> TransferData()
        {
            return TransferData(0);
        }
        */

        public TransferResult Stage1_TransferContactBatch()
        {
            logger.DebugFormat("Stage1_TransferContactBatch starting");
            
            // work out contacts that need doing
            // take a batch ofcontacts:
            //    - not done already
            //    or
            //    - done but since updated in prescient
            // then apply updates to CRM
            // record updates in table so we know date last done

            // work out cases that need doimng
            // take a batch of cases
            //    - not done already
            //    or
            //    - done but since updated in prescient
            // apply case updates to CRM

            // transfer time records for cases
            // how to check if time has been done?

            // open cases - call Contact create and Case create in SOS

            // fetch some contacts to process
            List<int> contactIds = _controlTasks.FetchContactIdsToProcess();
            logger.DebugFormat("Fetched {0} contactids", contactIds.Count);
            var dscon = _prescientTasks.FetchDataSet(contactIds, null, null);
            logger.DebugFormat("Dataset has {0} Contacts", dscon.Contact.Count);
            // TODO modify this to write to clarinet table
            var res = Transfer_Contacts(dscon.Contact, dscon.ContactIndividual, dscon.ContactOrganisation);

        


            CheckTransferResults(res);
            logger.DebugFormat("Stage1_TransferContactBatch ending");
            return res;
        }

        public TransferResult Stage2_TransferCaseBatch()
        {
            logger.DebugFormat("Stage2_TransferCaseBatch starting");
            

            // fetch some cases to process
            List<String> caseRefs = _controlTasks.FetchCaseRefsToProcess();
            logger.DebugFormat("Fetched {0} caserefs", caseRefs.Count);
            var dscas = _prescientTasks.FetchDataSet(null, caseRefs, null);
            logger.DebugFormat("Dataset has {0} Cases", dscas.Case.Count);

            var res = Transfer_Case(dscas.Case);


            CheckTransferResults(res);
            logger.DebugFormat("Stage2_TransferCaseBatch ending");
            return res;
        }

        public TransferResult Stage3_TransferTimeBatch()
        {
            logger.DebugFormat("Stage3_TransferTimeBatch starting");
            
            List<String> caseRefsWithTime = _controlTasks.FetchCaseRefsWithTimeRecordsToProcess();
            logger.DebugFormat("Fetched {0} caserefs", caseRefsWithTime.Count);
            var dscas = _prescientTasks.FetchDataSet(null, null, caseRefsWithTime);
            logger.DebugFormat("Dataset has {0} Cases and {1} Time Records", dscas.Case.Count, dscas.TimeRecord.Count);
            _controlTasks.FetchTargetIDsForTimeRecords(dscas);
            var res = Transfer_TimeRecord(dscas.TimeRecord);

            CheckTransferResults(res);
            logger.DebugFormat("Stage3_TransferTimeBatch ending");
            return res;
        }

        public TransferResult Stage4_SyncToSosBatch()
        {
            logger.DebugFormat("Stage4_SyncToSosBatch starting");

            List<Guid> caseIds = _controlTasks.FetchCaseIdsForSosSync();
            logger.DebugFormat("Fetched {0} case ids",caseIds.Count);
            var res = Process_CaseToSosSync(caseIds);

            CheckTransferResults(res);
            logger.DebugFormat("Stage4_SyncToSosBatch ending");

            return res;
        }


        /* no longer used
        /// <summary>
        /// Main method of the class -
        /// gets data from Prescient via GetDetails() web service call
        /// sends data to CRM and records the CrmIds generated
        /// Sends the CrmIds back to Prescient via web service call
        /// </summary>
        public List<TransferResult> TransferData(int maxRecords)
        {
            logger.DebugFormat("TransferData starting, MaxRecords {0}", maxRecords);


            var res = new List<TransferResult>();


            _historyWriter.WriteMessage(string.Format("Transfer starting "), 0);

            //List<int> partyids = _prescientTasks.FetchContactIdsToProcess(10);
            List<int> partyids = new List<int>() {678252, 678992, 679240,
                            679390, 679496, 679872, 679873, 679878, 679919, 679921};
            List<String> caseRefs = new List<String>() {"A233503.1", "A234987.1", "A239959.1", "A252521.1", "A254005.1", "A256232.1",
                "A256761.1", "A257621.1", "A261193.1", "A264779.1" };
            List<String> caseRefs2 = new List<String>() { "L001950.1", "L001880.1", "L001852.1", "L001852.2" };

            partyids.Clear();
            var ds = _prescientTasks.FetchDataSet(partyids, caseRefs2, caseRefs2);

            //res.Add(Transfer_Contacts(ds.Contact, ds.ContactIndividual, ds.ContactOrganisation));
           
           
            //res.Add(Transfer_Case(ds.Case));
            res.Add(Transfer_TimeRecord(ds.TimeRecord));
           
            _historyWriter.WriteMessage(string.Format("Transfer finished"), 0);
            CheckTransferResults(res);
            return res;
            
        }
        */

        public void CheckTransferResults(List<TransferResult> transferResults)
        {
            foreach (var resLoop in transferResults)
            {
                CheckTransferResults(resLoop);   
            }

        }

        public void CheckTransferResults(TransferResult resLoop)
        {
            logger.DebugFormat("CheckTransferResults: Table {0} Total {1} Succeeded {2} Failed {3} NotFoundInDestination {4} Skipped {5}",
                    resLoop.TableName, resLoop.TotalRows, resLoop.Succeeded, resLoop.Failed,  resLoop.NotFoundInDestination, resLoop.Skipped);

            // write the interesting bits to history
            if (resLoop.Succeeded > 0)
                _historyWriter.WriteMessage(string.Format("Results: {0} Succeeded", resLoop.TableName), resLoop.Succeeded);
            if (resLoop.Failed > 0)
                _historyWriter.WriteMessage(string.Format("Results: {0} Failed", resLoop.TableName), resLoop.Failed);

        }


        public TransferResult Transfer_Contacts(Referrals.ContactDataTable contacts, Referrals.ContactIndividualDataTable indv, Referrals.ContactOrganisationDataTable orgs)
        {
            var res = new TransferResult("Contact");
            res.TotalRows = contacts.Count;
            res.Skipped = 0;
            logger.DebugFormat("Transfer_Contacts: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var contactRow in contacts)
            {
                HistoryWriter.RowHistory history = null;                
                try
                {
                    logger.DebugFormat("Transfer_Contacts: processing contact {0}", contactRow.SourceContactID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, contactRow.SourceContactID, int.Parse(contactRow.SyncID));

                    if (_ignoreList.ShouldIgnore(res.TableName, contactRow.SourceContactID))
                    {
                        IgnoreRowTasks_Generic(res, history, contactRow.SyncID, contactRow.SourceContactID, contactRow.TargetContactID, contactRow.TargetReference);
                        continue;
                    }


                    // find matching indv or org row
                    var matchIndv = indv.FirstOrDefault(r => r.SourceContactID == contactRow.SourceContactID);
                    var matchOrg = orgs.FirstOrDefault(r => r.SourceContactID == contactRow.SourceContactID);

                    if (matchIndv != null && matchOrg != null)
                        throw new ApplicationException(string.Format("Contact row {0} has both matching ContactIndividual or ContactOrganisation - shouldn't happen",
                            contactRow.SourceContactID));

                    if (matchIndv != null)
                    {

                        CheckForTargetContactID(contactRow);
                        if (String.IsNullOrEmpty(contactRow.TargetContactID))
                        {
                            _controlTasks.MarkContactNotFoundInCrm(int.Parse(contactRow.SourceContactID));
                            res.NotFoundInDestination += 1;
                            throw new ApplicationException(string.Format("Could not determine CRM ID for Contact row {0} type - indv",
                                contactRow.SourceContactID));
                        }

                        var crmResult = _crmTasks.SaveContact(contactRow, matchIndv);
                        contactRow.TargetContactID = crmResult.CrmId.ToString();
                        contactRow.TargetReference = _crmTasks.GetContactRef(crmResult.CrmId);

                        
                        res.Succeeded += 1;
                        history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, contactRow.TargetContactID);
                        

                    }
                    else if (matchOrg != null)
                    {
                        CheckForTargetOrganisationID(contactRow);
                        if (String.IsNullOrEmpty(contactRow.TargetContactID))
                        {
                            _controlTasks.MarkContactNotFoundInCrm(int.Parse(contactRow.SourceContactID));
                            throw new ApplicationException(string.Format("Could not determine CRM ID for Contact row {0} type - org",
                               contactRow.SourceContactID));
                        }


                        var crmResult = _crmTasks.SaveOrganisation(contactRow, matchOrg);
                        contactRow.TargetContactID = crmResult.CrmId.ToString();
                        contactRow.TargetReference = _crmTasks.GetOrganisationRef(crmResult.CrmId);
                        res.Succeeded += 1;
                        history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, contactRow.TargetContactID);

                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Contact row {0} did not have matching ContactIndividual or ContactOrganisation",
                        contactRow.SourceContactID));
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_Contacts: error processing {0}", contactRow.SourceContactID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                    ReconnectCheckOnError(ex);
                }
                _historyWriter.WriteRowHistory(history);
                if (history.Succeeded)
                {
                    _controlTasks.WriteContactDone(int.Parse(contactRow.SourceContactID));
                }

            }
            return res;
        }

        private void ReconnectCheckOnError(Exception ex)
        {
            if (ex.InnerException != null)
            {
                var innerex = ex.InnerException;
                if (innerex.Message.StartsWith("At least one security token in the message could not be validated."))
                {
                    logger.DebugFormat("ReconnectCheckOnError: error message suggests CRM connection broken ... reconnecting ....");
                    _crmTasks.Reconnect();
                }
            }
        }

        

        public void CheckForTargetContactID(Referrals.ContactRow contactRow)
        {
            // if we have SourceContactID but not TargetContactID then cross check with CRM
            // to see if we can get TargetContactID
            if (string.IsNullOrEmpty(contactRow.TargetContactID) && !string.IsNullOrEmpty(contactRow.SourceContactID))
            {

                var contactGuid = _crmTasks.GetContactCrmId(contactRow.SourceContactID);
                if (contactGuid.HasValue)
                {
                    contactRow.TargetContactID = contactGuid.Value.ToString();
                    logger.DebugFormat("CheckForTargetContactID: contactRow SyncID {0} has SourceContactID {1} but blank TargetContactID, got TargetContactID from CRM {2}",
                            contactRow.SyncID, contactRow.SourceContactID, contactRow.TargetContactID);
                }
                
            }

        }

        public void CheckForTargetOrganisationID(Referrals.ContactRow contactRow)
        {
            // if we have SourceContactID but not TargetContactID then cross check with CRM
            // to see if we can get TargetContactID
            if (string.IsNullOrEmpty(contactRow.TargetContactID) && !string.IsNullOrEmpty(contactRow.SourceContactID))
            {

                var orgGuid = _crmTasks.GetOrganisationCrmId(contactRow.SourceContactID);
                if (orgGuid.HasValue)
                {
                    contactRow.TargetContactID = orgGuid.Value.ToString();
                    logger.DebugFormat("CheckForTargetOrganisationID: contactRow SyncID {0} has SourcecontactID {1} but blank TargetContactID, got TargetContactID from CRM {2} (Organisation)",
                            contactRow.SyncID, contactRow.SourceContactID, contactRow.TargetContactID);
                }


            }

        }

        public TransferResult Transfer_Addresses(Referrals.AddressDataTable addresses)
        {
            var res = new TransferResult("Address");
            res.TotalRows = addresses.Count;
            res.Skipped = addresses.Where(r => r.DeletionFlag == "Y").Count();
            logger.DebugFormat("Transfer_Addresses: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var addressRow in addresses.Where(r => r.DeletionFlag != "Y"))
            {
                HistoryWriter.RowHistory history = null;     
                try
                {
                    logger.DebugFormat("Transfer_Addresses: processing address {0}", addressRow.SourceAddressID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, addressRow.SourceAddressID, int.Parse(addressRow.SyncID));

                    CheckForTargetAddressID(addressRow);

                    var crmResult = _crmTasks.SaveAddress(addressRow);
                    addressRow.TargetAddressID = crmResult.CrmId.ToString();
                    //TellPrescientDone_Generic("Address", addressRow.SyncID, addressRow.TargetAddressID, null);
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, addressRow.TargetAddressID);

                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_Addresses: error processing {0}", addressRow.SourceAddressID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public void CheckForTargetAddressID(Referrals.AddressRow addressRow)
        {
            // if we have SourceAddressID but not TargetAddressID then cross check with CRM
            // to see if we can get TargetAddressID
            if (string.IsNullOrEmpty(addressRow.TargetAddressID) && !string.IsNullOrEmpty(addressRow.SourceAddressID))
            {

                var addressGuid = _crmTasks.GetAddressCrmId(addressRow.SourceAddressID);
                if (addressGuid.HasValue)
                {
                    addressRow.TargetAddressID = addressGuid.Value.ToString();
                    logger.DebugFormat("CheckForTargetAddressID: addressRow SyncID {0} has SourceAddressID {1} but blank TargetAddressID, got TargetAddressID from CRM {2}",
                            addressRow.SyncID, addressRow.SourceAddressID, addressRow.TargetAddressID);
                }

            }

        }

        public TransferResult Transfer_ContactAddressLinks(Referrals.ContactAddressLinkDataTable links, Referrals.AddressDataTable addresses)
        {
            var res = new TransferResult("ContactAddressLink");
            res.TotalRows = links.Count;
            res.Skipped = links.Where(r => r.DeletionFlag == "Y").Count();
            logger.DebugFormat("Transfer_ContactAddressLinks: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var linkRow in links.Where( r => r.DeletionFlag != "Y"))
            {
                HistoryWriter.RowHistory history = null;     
                try
                {
                    logger.DebugFormat("Transfer_ContactAddressLinks: processing link {0}", linkRow.SourceContactAddressLinkID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, linkRow.SourceContactAddressLinkID, int.Parse(linkRow.SyncID));
                    

                    if (string.IsNullOrEmpty(linkRow.SourceContactID) && string.IsNullOrEmpty(linkRow.SourceAddressID))
                    {
                        throw new ApplicationException(string.Format("Transfer_ContactAddressLinks: sync id {0} has empty SourceAddressID, SourceContactID - cannot link",
                            linkRow.SyncID));
                    }

                    var contactOrOrganisation = new ContactOrOrganisationResult();
                    if (linkRow.ContactRow != null && !string.IsNullOrEmpty(linkRow.ContactRow.TargetContactID))
                    {
                        contactOrOrganisation.CrmId = new Guid(linkRow.ContactRow.TargetContactID);
                        logger.DebugFormat("Getting related ContactID {0} from DataSet", contactOrOrganisation.CrmId);
                        if (linkRow.ContactRow.GetContactIndividualRows().Count() > 0)
                            contactOrOrganisation.IsContact = true;
                        else if (linkRow.ContactRow.GetContactOrganisationRows().Count() > 0)
                            contactOrOrganisation.IsOrganisation = true;
                        logger.DebugFormat("Related ContactID {0}  IsIndv: {1}  IsOrg: {2}", contactOrOrganisation.CrmId,
                            contactOrOrganisation.IsContact, contactOrOrganisation.IsOrganisation);
                    }
                    else
                    {
                        contactOrOrganisation = DoesIdRepresentContactOrOrganisationInCrm(linkRow.SourceContactID);
                        if (!contactOrOrganisation.CrmId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Contact {0} SyncId {1}",
                                linkRow.SourceContactID, linkRow.SyncID));
                        
                    }
                    Guid? targetAddressId = null;
                    // dataset does not link address to contactaddresslink directly so we search like this
                    var matchingAddressRow = addresses.FirstOrDefault(r => r.SourceAddressID == linkRow.SourceAddressID);
                    if (matchingAddressRow != null && !string.IsNullOrEmpty(matchingAddressRow.TargetAddressID))
                    {
                        targetAddressId = new Guid(matchingAddressRow.TargetAddressID);
                        logger.DebugFormat("Getting related addressID {0} from DataSet", targetAddressId);
                    }
                    else
                    {
                        targetAddressId = _crmTasks.GetAddressCrmId(linkRow.SourceAddressID);
                        if (!targetAddressId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Address {0} SyncId {1}",
                                linkRow.SourceAddressID, linkRow.SyncID));
                                
                        
                    }

                    if (contactOrOrganisation.IsContact)
                    {
                        var crmResult = _crmTasks.SaveContactAddressLink(linkRow, contactOrOrganisation.CrmId.Value, targetAddressId.Value);
                        linkRow.TargetContactAddressLinkID = crmResult.CrmId.ToString();
                        //TellPrescientDone_Generic("ContactAddressLink", linkRow.SyncID, linkRow.TargetContactAddressLinkID, null);
                        res.Succeeded += 1;
                        history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, linkRow.TargetContactAddressLinkID);

                    }
                    else if (contactOrOrganisation.IsOrganisation)
                    {
                        var crmResult = _crmTasks.SaveOrganisationAddressLink(linkRow, contactOrOrganisation.CrmId.Value, targetAddressId.Value);
                        linkRow.TargetContactAddressLinkID = crmResult.CrmId.ToString();
                        //TellPrescientDone_Generic("ContactAddressLink", linkRow.SyncID, linkRow.TargetContactAddressLinkID, null);
                        res.Succeeded += 1;
                        history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, linkRow.TargetContactAddressLinkID);
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Transfer_ContactAddressLinks could not tell whether SourcecontactaddresslinkID {0} related to a Contact or an Organisation",
                            linkRow.SourceContactAddressLinkID));
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_ContactAddressLinks: error processing {0}", linkRow.SourceContactAddressLinkID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public TransferResult Transfer_Case(Referrals.CaseDataTable cases)
        {
            var res = new TransferResult("Case");
            res.TotalRows = cases.Count;
            res.Skipped = 0;
            logger.DebugFormat("Transfer_Case: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var caseRow in cases)
            {
                HistoryWriter.RowHistory history = null;     
                try
                {
                    logger.DebugFormat("Transfer_Case: processing case {0}", caseRow.SourceCaseID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, caseRow.SourceCaseID, int.Parse(caseRow.SyncID));

                    if (_ignoreList.ShouldIgnore(res.TableName, caseRow.SourceCaseID))
                    {
                        // mark ignore cases as missing so they dont get processed
                        _controlTasks.MarkCaseNotFoundInCrm(caseRow.SourceCaseID);
                        IgnoreRowTasks_Generic(res, history, caseRow.SyncID, caseRow.SourceCaseID, caseRow.TargetCaseID, null);
                        continue;
                    }

                    if (string.IsNullOrEmpty(caseRow.SourceContactID))
                    {
                        throw new ApplicationException(string.Format("Transfer_Case: sync id {0} Case {1} has empty SourceContactID - cannot link",
                            caseRow.SyncID, caseRow.SourceCaseID));
                    }

                    CheckForTargetCaseID(caseRow);
                    if (String.IsNullOrEmpty(caseRow.TargetCaseID))
                    {
                        _controlTasks.MarkCaseNotFoundInCrm(caseRow.SourceCaseID);
                        res.NotFoundInDestination += 1;
                        throw new ApplicationException(string.Format("Could not determine CRM ID for Case row {0} ",
                            caseRow.SourceCaseID));
                    }

                    Guid? targetContactId = null;
                    if (caseRow.ContactRow != null && !string.IsNullOrEmpty(caseRow.ContactRow.TargetContactID))
                    {
                        targetContactId = new Guid(caseRow.ContactRow.TargetContactID);
                        logger.DebugFormat("Getting related ContactID {0} from DataSet", targetContactId);
                    }
                    else
                    {
                        targetContactId = _crmTasks.GetContactCrmId(caseRow.SourceContactID);
                        if (!targetContactId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Contact {0} for Case {1} SyncId {2}",
                                caseRow.SourceContactID, caseRow.SourceCaseID, caseRow.SyncID));

                    }

                    


                    var crmResult = _crmTasks.SaveCase(caseRow, targetContactId.Value);
                    caseRow.TargetCaseID = crmResult.CrmId.ToString();
                    caseRow.TargetReference = _crmTasks.GetCaseRef(crmResult.CrmId);
                    //TellPrescientDone_Generic("Case", caseRow.SyncID, caseRow.TargetCaseID, caseRow.TargetReference);
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, caseRow.TargetCaseID);
                    _controlTasks.WriteCaseDone(caseRow.SourceCaseID, crmResult.CrmId);
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_Case: error processing {0}", caseRow.SourceCaseID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                    ReconnectCheckOnError(ex);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public void CheckForTargetCaseID(Referrals.CaseRow caseRow)
        {

            if (string.IsNullOrEmpty(caseRow.TargetCaseID) && !string.IsNullOrEmpty(caseRow.SourceCaseID))
            {

                var caseGuid = _crmTasks.GetCaseCrmId(caseRow.SourceCaseID);
                if (caseGuid.HasValue)
                {
                    caseRow.TargetCaseID = caseGuid.Value.ToString();
                    logger.DebugFormat("CheckForTargetCaseID: caseRow SyncID {0} has SourceCaseID {1} but blank TargetCaseID, got TargetCaseID from CRM {2}",
                            caseRow.SyncID, caseRow.SourceCaseID, caseRow.TargetCaseID);
                }

            }

        }

       

        public TransferResult Transfer_TimeRecord(Referrals.TimeRecordDataTable trs)
        {
            var res = new TransferResult("TimeRecord");
            res.TotalRows = trs.Count;
            res.Skipped = trs.Where(t => !String.IsNullOrEmpty(t.TargetTimeRecordID)).Count();
            logger.DebugFormat("Transfer_TimeRecord: {0} rows in table, skipping {1} because they alredy have target ids",res.TotalRows, res.Skipped);
            foreach (var trRow in trs.Where(t => String.IsNullOrEmpty(t.TargetTimeRecordID)))
            {
                HistoryWriter.RowHistory history = null;
                try
                {
                    logger.DebugFormat("Transfer_TimeRecord: processing time record {0} for case {1}", trRow.SourceTimeRecordID, trRow.SourceCaseID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, trRow.SourceTimeRecordID, 0);

                    if (_ignoreList.ShouldIgnore(res.TableName, trRow.SourceTimeRecordID))
                    {
                        IgnoreRowTasks_Generic(res, history, "", trRow.SourceTimeRecordID, null, null);
                        continue;
                    }

                    if (string.IsNullOrEmpty(trRow.SourceCaseID))
                    {
                        throw new ApplicationException(string.Format("Transfer_TimeRecord: Time rec {0} has empty SourceCaseID - cannot link",
                            trRow.SourceTimeRecordID));
                    }

                    Guid? targetCaseId = null;
                    if (trRow.CaseRow != null && !string.IsNullOrEmpty(trRow.CaseRow.TargetCaseID))
                    {
                        targetCaseId = new Guid(trRow.CaseRow.TargetCaseID);
                        logger.DebugFormat("Getting related CaseID {0} from DataSet", targetCaseId);
                    }
                    else
                    {
                        targetCaseId = _crmTasks.GetCaseCrmId(trRow.SourceCaseID);
                        if (!targetCaseId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Case {0} for Time Record {1} ",
                                trRow.SourceCaseID, trRow.SourceTimeRecordID));

                    }

                    // if row already exists in CRM, TargetID will be filled in
                    // via Clarinet control tables
                    // see ControlDataTasks.FetchTargetIDsForTimeRecords(Referrals ds)

                    var crmResult = _crmTasks.SaveTimeRecord(trRow, targetCaseId.Value);
                    trRow.TargetTimeRecordID = crmResult.CrmId.ToString();
                    
                    
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, trRow.TargetTimeRecordID);
                    _controlTasks.WriteTimeRecordDone(trRow.SourceCaseID, int.Parse(trRow.SourceTimeRecordID), crmResult.CrmId);
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_TimeRecord: error processing {0}", trRow.SourceCaseID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                    ReconnectCheckOnError(ex);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public TransferResult Process_CaseToSosSync(List<Guid> caseIds)
        {
            var res = new TransferResult("SosSync");
            res.TotalRows = caseIds.Count;
            logger.DebugFormat("Process_CaseToSosSync: {0} case Ids",
                res.TotalRows);
            SosInterface.SosTasks sosTasks = new SosInterface.SosTasks();
            foreach(Guid cId in caseIds)
            {
                HistoryWriter.RowHistory history = null;
                try
                {
                    logger.DebugFormat("Process_CaseToSosSync: processing case id {0}", cId);
                    history = new HistoryWriter.RowHistory(1, "SosSync", cId.ToString(), 0);

                    var sosResponse = sosTasks.TransferCrmCaseToSos(cId);
                    if (sosResponse.Success)
                    {
                        res.Succeeded += 1;
                        history.SetSucceededInfo("SosSync", sosResponse.SosCaseID);
                        _controlTasks.WriteSosSyncDone(sosResponse.CrmCaseRef, cId);
                    }
                    else
                    {
                        history.SetFailedInfo(sosResponse.ErrorMessage);
                        res.Failed += 1;
                    }
                
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Process_CaseToSosSync: error processing {0}", cId), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public TransferResult Transfer_CaseContactLinks(Referrals.CaseContactLinkDataTable links)
        {
            var res = new TransferResult("CaseContactLink");
            res.TotalRows = links.Count;
            res.Skipped = links.Where(r => r.DeletionFlag == "Y").Count();
            logger.DebugFormat("Transfer_CaseContactLinks: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var linkRow in links.Where( r=> r.DeletionFlag != "Y"))
            {
                HistoryWriter.RowHistory history = null;     
                try
                {
                    logger.DebugFormat("Transfer_CaseContactLink: processing link {0}", linkRow.SourceCaseContactLinkID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, linkRow.SourceCaseContactLinkID, int.Parse(linkRow.SyncID));
                    

                    Guid? targetContactId = null;
                    bool contactIsIndividual = false;
                    bool contactIsOrganisation = false;
                    if (linkRow.ContactRow != null && !string.IsNullOrEmpty(linkRow.ContactRow.TargetContactID))
                    {
                        targetContactId = new Guid(linkRow.ContactRow.TargetContactID);
                        logger.DebugFormat("Getting related ContactID {0} from DataSet", targetContactId);
                        if (linkRow.ContactRow.GetContactIndividualRows().Count() > 0)
                            contactIsIndividual = true;
                        else if (linkRow.ContactRow.GetContactOrganisationRows().Count() > 0)
                            contactIsOrganisation = true;
                        logger.DebugFormat("Related ContactID {0}  IsIndv: {1}  IsOrg: {2}", targetContactId,
                            contactIsIndividual, contactIsOrganisation);
                    }
                    else
                    {
                        targetContactId = _crmTasks.GetContactCrmId(linkRow.SourceContactID);
                        if (targetContactId.HasValue)
                            contactIsIndividual = true;
                        else
                        {
                            targetContactId = _crmTasks.GetOrganisationCrmId(linkRow.SourceContactID);
                            if (targetContactId.HasValue)
                                contactIsOrganisation = true;

                        }
                        if (!targetContactId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Contact {0} SyncId {1}",
                                linkRow.SourceContactID, linkRow.SyncID));

                    }
                    Guid? targetCaseId = null;
                    if (linkRow.CaseRow != null && !string.IsNullOrEmpty(linkRow.CaseRow.TargetCaseID))
                    {
                        targetCaseId = new Guid(linkRow.CaseRow.TargetCaseID);
                        logger.DebugFormat("Getting related caseID {0} from DataSet", targetCaseId);
                    }
                    else
                    {
                        targetCaseId = _crmTasks.GetCaseCrmId(linkRow.SourceCaseID);
                        if (!targetCaseId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Case {0} SyncId {1}",
                                linkRow.SourceCaseID, linkRow.SyncID));

                    }


                    var crmResult = _crmTasks.SaveCaseContactLink(linkRow, targetContactId.Value, targetCaseId.Value, contactIsIndividual, contactIsOrganisation);
                    linkRow.TargetCaseContactLinkID = crmResult.CrmId.ToString();
                    //TellPrescientDone_Generic("CaseContactLink", linkRow.SyncID, linkRow.TargetCaseContactLinkID, null);
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, linkRow.TargetCaseContactLinkID);

                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_CaseContactLinks: error processing {0}", linkRow.SourceCaseContactLinkID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public TransferResult Transfer_ContactContactLinks(Referrals.ContactContactLinkDataTable links)
        {
            var res = new TransferResult("ContactContactLink");
            res.TotalRows = links.Count;
            foreach (var linkRow in links)
            {
                HistoryWriter.RowHistory history = null; 
                try
                {
                    logger.DebugFormat("Transfer_ContactContactLinks: processing link {0}", linkRow.SourceContactContactLinkID);
                    history = new HistoryWriter.RowHistory(1, res.TableName,  linkRow.SourceContactContactLinkID, int.Parse(linkRow.SyncID));
                   
                    Guid? targetContactId = null;
                    bool contactIsIndividual = false;
                    bool contactIsOrganisation = false;
                    if (linkRow.ContactRowByContact_ContactContactLink != null && !string.IsNullOrEmpty(linkRow.ContactRowByContact_ContactContactLink.TargetContactID))
                    {
                        targetContactId = new Guid(linkRow.ContactRowByContact_ContactContactLink.TargetContactID);
                        logger.DebugFormat("Getting related ContactID {0} from DataSet", targetContactId);
                        if (linkRow.ContactRowByContact_ContactContactLink.GetContactIndividualRows().Count() > 0)
                            contactIsIndividual = true;
                        else if (linkRow.ContactRowByContact_ContactContactLink.GetContactOrganisationRows().Count() > 0)
                            contactIsOrganisation = true;
                        logger.DebugFormat("Related ContactID {0}  IsIndv: {1}  IsOrg: {2}", targetContactId,
                            contactIsIndividual, contactIsOrganisation);
                    }
                    else
                    {
                        targetContactId = _crmTasks.GetContactCrmId(linkRow.SourceContactID);
                        if (targetContactId.HasValue)
                            contactIsIndividual = true;
                        else
                        {
                            targetContactId = _crmTasks.GetOrganisationCrmId(linkRow.SourceContactID);
                            if (targetContactId.HasValue)
                                contactIsOrganisation = true;

                        }
                        if (!targetContactId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Contact {0} SyncId {1}",
                                linkRow.SourceContactID, linkRow.SyncID));

                    }

                    var crmResult = _crmTasks.SaveContactContactLink(linkRow, targetContactId.Value, contactIsIndividual, contactIsOrganisation);
                    linkRow.TargetContactContactLinkID = crmResult.CrmId.ToString();
                    // todo: contactcontactlink....
                    //TellPrescientDone_CONTACTContactLink(linkRow.SyncID, linkRow.TargetContactContactLinkID, linkRow.SourceContactContactLinkID, linkRow.SourceContactID, linkRow.SourceContactID);
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, linkRow.TargetContactContactLinkID);

                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_ContactContactLinks: error processing {0}", linkRow.SourceContactContactLinkID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public TransferResult Transfer_ContactAlerts(Referrals.ContactAlertDataTable contactAlerts)
        {
            var res = new TransferResult("ContactAlert");
            res.TotalRows = contactAlerts.Count;
            res.Skipped = contactAlerts.Where(r => r.DeletionFlag == "Y").Count();
            logger.DebugFormat("Transfer_ContactAlerts: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var contactAlertRow in contactAlerts.Where( r => r.DeletionFlag != "Y"))
            {
                HistoryWriter.RowHistory history = null;     
                try
                {
                    logger.DebugFormat("Transfer_ContactAlerts: processing contact alert {0}", contactAlertRow.SourceContactAlertID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, contactAlertRow.SourceContactAlertID, int.Parse(contactAlertRow.SyncID));
                    
                    if (string.IsNullOrEmpty(contactAlertRow.SourceContactID))
                    {
                        throw new ApplicationException(string.Format("Transfer_ContactAlerts: sync id {0} Case {1} has empty SourceContactID - cannot link",
                            contactAlertRow.SyncID, contactAlertRow.SourceContactID));
                    }

                    Guid? targetContactId = null;
                    if (contactAlertRow.ContactRow != null && !string.IsNullOrEmpty(contactAlertRow.ContactRow.TargetContactID))
                    {
                        targetContactId = new Guid(contactAlertRow.ContactRow.TargetContactID);
                        logger.DebugFormat("Getting related ContactID {0} from DataSet", targetContactId);
                    }
                    else
                    {
                        targetContactId = _crmTasks.GetContactCrmId(contactAlertRow.SourceContactID);
                        if (!targetContactId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Contact {0} for Contact Alert {1} SyncId {2}",
                                contactAlertRow.SourceContactID, contactAlertRow.SourceContactAlertID, contactAlertRow.SyncID));

                    }

                    CheckForTargetContactAlertID(contactAlertRow);

                    var crmResult = _crmTasks.SaveContactAlert(contactAlertRow, targetContactId.Value);

                    
                    contactAlertRow.TargetContactAlertID = crmResult.CrmId.ToString();
                    
                    //TellPrescientDone_Generic("ContactAlert",contactAlertRow.SyncID, contactAlertRow.TargetContactAlertID, null);
                    res.Succeeded += 1;
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, contactAlertRow.TargetContactAlertID);

                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_Case: error processing {0}", contactAlertRow.SourceContactAlertID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }
            return res;
        }

        public void CheckForTargetContactAlertID(Referrals.ContactAlertRow alertRow)
        {

            if (string.IsNullOrEmpty(alertRow.TargetContactAlertID) && !string.IsNullOrEmpty(alertRow.SourceContactAlertID))
            {

                var alertGuid = _crmTasks.GetContactAlertCrmId(alertRow.SourceContactAlertID);
                if (alertGuid.HasValue)
                {
                    alertRow.TargetContactAlertID = alertGuid.Value.ToString();
                    logger.DebugFormat("CheckForTargetContactAlertID: alertRow SyncID {0} has SourceContactalertID {1} but blank TargetContactAlertID, got TargetContactAlertID from CRM {2}",
                            alertRow.SyncID, alertRow.SourceContactAlertID, alertRow.TargetContactAlertID);
                }

            }

        }

        public TransferResult Transfer_CasePresentingProblems(Referrals.CasePresentingProblemDataTable presentingProblems)
        {
            var res = new TransferResult("CasePresentingProblem");
            res.TotalRows = presentingProblems.Count;
            res.Skipped = presentingProblems.Where(r => r.DeletionFlag == "Y").Count();
            logger.DebugFormat("Transfer_CasePresentingProblems: {0} rows in table, skipping {1} because they are deletions",
                res.TotalRows, res.Skipped);
            foreach (var presentingProblem in presentingProblems.Where( r => r.DeletionFlag != "Y"))
            {
                HistoryWriter.RowHistory history = null;
                try
                {
                    logger.DebugFormat("Transfer_CasePresentingProblems: processing presenting problem {0}", presentingProblem.SourceCasePresentingProblemID);
                    history = new HistoryWriter.RowHistory(1, res.TableName, presentingProblem.SourceCasePresentingProblemID, int.Parse(presentingProblem.SyncID));
                    

                    if (string.IsNullOrEmpty(presentingProblem.SourceCaseID))
                    {
                        throw new ApplicationException(string.Format("Transfer_CasePresentingProblems: sync id {0} Presenting Problem {1} has empty SourceCaseID - cannot link",
                            presentingProblem.SyncID, presentingProblem.SourceCaseID));
                    }

                    Guid? targetCaseId = null;
                    if (presentingProblem.CaseRow != null && !string.IsNullOrEmpty(presentingProblem.CaseRow.TargetCaseID))
                    {
                        targetCaseId = new Guid(presentingProblem.CaseRow.TargetCaseID);
                        logger.DebugFormat("Getting related CaseID {0} from DataSet", targetCaseId);
                    }
                    else
                    {
                        targetCaseId = _crmTasks.GetCaseCrmId(presentingProblem.SourceCaseID);
                        if (!targetCaseId.HasValue)
                            throw new ApplicationException(string.Format("Cannot determine CRMId for Prescient Case {0} for Presenting Problem {1} SyncId {2}",
                                presentingProblem.SourceCaseID, presentingProblem.SourceCasePresentingProblemID, presentingProblem.SyncID));

                    }

                    var crmResult = _crmTasks.SaveCasePresentingProblem(presentingProblem, targetCaseId.Value);

                   
                    presentingProblem.TargetCasePresentingProblemID = crmResult.CrmId.ToString();

                    //TellPrescientDone_Generic("CasePresentingProblem", presentingProblem.SyncID, presentingProblem.TargetCasePresentingProblemID, null);
                    history.SetSucceededInfo(crmResult.IsInsert, crmResult.IsUpdate, presentingProblem.TargetCasePresentingProblemID);

                    

                    res.Succeeded += 1;
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Transfer_CasePresentingProblems: error processing {0}", presentingProblem.SourceCasePresentingProblemID), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }

            return res;
        }


        public ContactOrOrganisationResult DoesIdRepresentContactOrOrganisationInCrm(string prescientIdToCheck)
        {
            var res = new ContactOrOrganisationResult();
            res.CrmId = _crmTasks.GetContactCrmId(prescientIdToCheck);
            if (res.CrmId.HasValue)
                res.IsContact = true;
            else
            {
                res.CrmId = _crmTasks.GetOrganisationCrmId(prescientIdToCheck);
                if (res.CrmId.HasValue)
                    res.IsOrganisation = true;

            }
            return res;
        }

        public ContactOrOrganisationResult DoesIdRepresentContactOrOrganisationInCrm(Guid crmIdToCheck)
        {
            var res = new ContactOrOrganisationResult();
            if (_crmTasks.DoesContactExist(crmIdToCheck))
            {
                res.IsContact = true;
                res.CrmId = crmIdToCheck;
            }
            else if (_crmTasks.DoesOrganisationExist(crmIdToCheck))
            {
                res.IsOrganisation = true;
                res.CrmId = crmIdToCheck;
            }
            return res;
        }

        public TransferResult ProcessDeletions(DataTable tableWithDeletions)
        {
            var res = new TransferResult(tableWithDeletions.TableName + " Deletion", true);
            res.TotalRows = tableWithDeletions.Rows.Count;
            res.Skipped = tableWithDeletions.AsEnumerable().Where( r=> r["DeletionFlag"].ToString() != "Y").Count();
            
            logger.DebugFormat("ProcessDeletions: {0} rows in table {1}, skipping {2} because they are not deletions",
                res.TotalRows, tableWithDeletions.TableName, res.Skipped);
            string targetIdFieldName = GetTargetIdFieldName(tableWithDeletions.TableName);
            foreach (var dataRow in tableWithDeletions.AsEnumerable().Where(r => r["DeletionFlag"].ToString() == "Y"))
            {
                HistoryWriter.RowHistory history = null;   
                try
                {
                    string syncId = dataRow["SyncId"].ToString();
                    logger.DebugFormat("ProcessDeletions: processing deletion for {0} row with syncid {1}", tableWithDeletions.TableName, syncId);
                    history = new HistoryWriter.RowHistory(1, res.TableName, null, int.Parse(syncId));
                    
                    if (dataRow[targetIdFieldName] == DBNull.Value)
                        throw new ApplicationException(string.Format("Deletion of a {0} row (SyncID {1}) not possible because {2} is null",
                            tableWithDeletions.TableName, syncId, targetIdFieldName));
                    if (string.IsNullOrEmpty(dataRow[targetIdFieldName].ToString()))
                        throw new ApplicationException(string.Format("Deletion of a {0} row (SyncID {1}) not possible because {2} is an empty string",
                            tableWithDeletions.TableName, syncId, targetIdFieldName));
                    Guid idToDelete = new Guid(dataRow[targetIdFieldName].ToString());

                    ContactOrOrganisationResult conorg = null;
                    if (tableWithDeletions.TableName == "Contact" || tableWithDeletions.TableName == "ContactAddressLink")
                    {
                        conorg = DoesIdRepresentContactOrOrganisationInCrm(idToDelete);
                        if (!(conorg.IsContact || conorg.IsOrganisation))
                            throw new ApplicationException(string.Format("Can't determine whether Contact {0} (SyncId {1}) is a Crm Contact or Organisation",
                                idToDelete, syncId));
                    }

                    switch (tableWithDeletions.TableName)
                    {
                        case "Contact":
                            if (conorg.IsContact)
                                _crmTasks.DeleteEntityBasedOnTable("ContactIndividual", idToDelete);
                            else
                                _crmTasks.DeleteEntityBasedOnTable("ContactOrganisation", idToDelete);
                            break;

                        case "ContactAddressLink":
                            if (conorg.IsContact)
                                _crmTasks.DeleteContactAddressLink(idToDelete);
                            else
                                _crmTasks.DeleteOrganisationAddressLink(idToDelete);
                            break;
                        case "CasePresentingProblem":
                            ProcessDeletion_PresentingProblem(syncId, dataRow, idToDelete);
                            break;


                        default:
                            // this will work for most tablenames
                            _crmTasks.DeleteEntityBasedOnTable(tableWithDeletions.TableName, idToDelete);
                            break;
                    }
                    

                    //TellPrescientDone_Generic(tableWithDeletions.TableName, syncId, idToDelete.ToString(), null);
                    res.Succeeded += 1;
                    history.SetSucceededInfo("Delete", idToDelete.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("ProcessDeletions: error processing deletion" ), ex);
                    res.Failed += 1;
                    history.SetFailedInfo(ex.Message);
                }
                _historyWriter.WriteRowHistory(history);
            }

            return res;

        }

        /// <summary>
        /// Presenting problems need to be handled differently during deletion because they dont have a unique ID
        /// </summary>
        public virtual void ProcessDeletion_PresentingProblem(string syncId, DataRow dataRow, Guid idToDelete)
        {
            string sourcePpId = dataRow["SourceCasePresentingProblemID"].ToString();
            logger.DebugFormat("ProcessDeletion_PresentingProblem called with SyncId {0}, pres prob id {1} and SourceCasePresentingProblemID {2}",
                syncId, idToDelete, sourcePpId);
            
            string prescientCase = null;
            if (sourcePpId.IndexOf(",") > -1)
            {
                prescientCase = sourcePpId.Substring(0, sourcePpId.LastIndexOf(","));
                prescientCase = prescientCase.Replace(",",".");
            }
            else
            {
                prescientCase = sourcePpId.Substring(0, sourcePpId.LastIndexOf("."));
            }
            
            
            logger.DebugFormat("Delete presprob: Prescient case ref is {0}", prescientCase);
            Guid? caseId = _crmTasks.GetCaseCrmId(prescientCase);
            if (!caseId.HasValue)
                throw new ApplicationException(string.Format("cannot remove presenting problem because prescient case ref {0} not found in CRM",
                    prescientCase));
            _crmTasks.DeleteCasePresentingProblem(caseId.Value, idToDelete);
        }

        /// <summary>
        /// Most tables in the Referrals dataset have a field that contains the target field name
        /// this returns the targetid field name for each table
        /// </summary>
        public string GetTargetIdFieldName(string tableName)
        {
            var ds = new Referrals();

            string targetIdFieldName = null;
            switch (tableName)
            {
                case "Contact":
                    targetIdFieldName = "TargetContactID";
                    break;
                case "Address":
                    targetIdFieldName = "TargetAddressID";
                    break;
                case "ContactAddressLink":
                    targetIdFieldName = "TargetContactAddressLinkID";
                    break;
                case "Case":
                    targetIdFieldName = "TargetCaseID";
                    break;
                case "CaseContactLink":
                    targetIdFieldName = "TargetCaseContactLinkID";
                    break;
                case "ContactAlert":
                    targetIdFieldName = "TargetContactAlertID";
                    break;
                case "CasePresentingProblem":
                    targetIdFieldName = "TargetCasePresentingProblemID";
                    break;
                case "ContactContactLink":
                    targetIdFieldName = "TargetContactContactLinkID";
                    break;
            }
            if (targetIdFieldName == null)
                throw new ApplicationException(string.Format("Could not determine target id field name for table {0}", tableName));

            return targetIdFieldName;
        }


          
        

        public void CallSetLinkID(Referrals ds, SyncStatus useSync, string contextForErrorLog)
        {
            // override usesync for testing
           //TODO
           // some sort of mark-done here

            

        }

        /// <summary>
        /// Called when a row is ignored, implements various tasks that overall treat it as a 'success'
        /// Its gets logged in History with OpType Ignored
        /// </summary>
        /// <param name="res">the TransferResult object from the Transfer_ method</param>
        /// <param name="history">the RowHistory object for the current row</param>
        /// <param name="syncID">the syncID of the ignored row</param>
        /// <param name="sourceEntityID">the sourceID of the ignored row</param>
        /// <param name="targetEntityID">the targetID of the row (just pass in what the dataset already has)</param>
        /// <param name="targetReference">the targetRefence of the row, if applicable. pass in what the dataset already has, or null</param>
        public void IgnoreRowTasks_Generic(TransferResult res, HistoryWriter.RowHistory history,
            string syncID, string sourceEntityID, string targetEntityID, string targetReference)
        {
            logger.DebugFormat("IgnoreRowTasks_Generic: row is being ignored, tablename {0} syncID {1} sourceEntityID {2} targetEntityID {3}",
                res.TableName, syncID, sourceEntityID, targetEntityID);
            //TellPrescientDone_Generic(res.TableName, syncID, targetEntityID, targetReference);
            res.Succeeded += 1;
            history.SetSucceededInfo("Ignored", targetEntityID);
            _historyWriter.WriteRowHistory(history);
        }

        public class TransferResult
        {
            public TransferResult(string name):this(name, false)
            { }

            public TransferResult(string name, bool processingDeletions)
            {
                TableName = name;
                TotalRows = 0;
                Succeeded = 0;
                Failed = 0;
                Skipped = 0;
                NotFoundInDestination = 0;
                ProcessingDeletions = processingDeletions;
                
            }

            public string TableName { get; set; }
            public bool ProcessingDeletions { get; set; }
            public int TotalRows { get; set; }
            public int Succeeded { get; set; }
            public int Failed { get; set; }
            public int Skipped { get; set; }
            public int NotFoundInDestination { get; set; }
            


        }

        public class ContactOrOrganisationResult
        {
            public Guid? CrmId { get; set; }
            public bool IsContact { get; set; }
            public bool IsOrganisation { get; set; }

        }


    }
}
