﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using ClarinetLib.CrmConnection;
using Microsoft.Crm.Sdk.Messages;

namespace ClarinetLib.SosInterface
{
    // crm tasks that relate to the SOS Interface
    public class CrmTasksForSos
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        private CrmConnectionCache _connectionPool;



        public CrmTasksForSos() : this(new CrmConnectionCache()) { }

        // constrcutor for unit testing
        public CrmTasksForSos(CrmConnectionCache connPool)
        {
            _connectionPool = connPool;


        }

        public Entity FetchIntegrationLog(Guid crmId, String logReference, String entityType)
        {
            EntityCollection fetchresults = InnerFetchIntegrationLogs(ref crmId, logReference, entityType);

            Entity ret = null;
            int count = 0;
            foreach (Entity logLoop in fetchresults.Entities)
            {
                ret = logLoop;
                count += 1;
            }
            // nb: sometimes there is more than 1, and the second one is a rollback message
            if (count > 1)
                throw new ApplicationException(string.Format("FetchIntegrationLog: found {0} IntegrationLog records for object {1} Id {2} LogRef {3}",
                   count, entityType, crmId, logReference));

            if (ret == null)
                throw new ApplicationException(string.Format("FetchIntegrationLog: found {0} IntegrationLog records for object {1} Id {2} LogRef {3}",
                   count, entityType, crmId, logReference));

            return ret;
        }

        private EntityCollection InnerFetchIntegrationLogs(ref Guid crmId, string logReference, string entityType)
        {
            var serv = _connectionPool.GetCrmConnectionCached();

            String fetchXmlPattern = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
        <entity name='gap_integrationlog'>
        <attribute name='gap_integrationlogid' />
        <attribute name='gap_name' />
        <attribute name='createdon' />
        <attribute name='gap_sourcetype' />
        <attribute name='gap_messagetype' />
        <attribute name='gap_message' />
        <order attribute='gap_name' descending='false' />
        <filter type='and'>
        <condition attribute='gap_reference' operator='eq' value='{0}' />
        <condition attribute='gap_sourcetype' operator='eq' value='810340000' />
        //<condition attribute='gap_name' operator='eq' value='{1}' />
        <condition attribute='{2}' operator='eq' value='{3}' />
        </filter>
        </entity>
        </fetch>";
            String entName = "gap_case";
            String logName = "SOS Create Case";
            if (entityType.Equals("Contact"))
            {
                entName = "gap_contact";
                logName = "SOS Create Contact";
            }
            String fetch = String.Format(fetchXmlPattern,
                logReference, logName, entName, "{" + crmId.ToString() + "}");

            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));
            return fetchresults;
        }

        public String FetchIntegrationLogMessages(Guid crmId, String logReference, String entityType)
        {
            EntityCollection fetchresults = InnerFetchIntegrationLogs(ref crmId, logReference, entityType);
            StringBuilder sb = new StringBuilder();

            int count = 0;
            foreach (Entity logLoop in fetchresults.Entities)
            {
                if (logLoop.Contains("gap_message") && logLoop["gap_message"] != null)
                {
                    String intMessage = logLoop["gap_message"].ToString();
                    sb.Append(intMessage + "\n");
                }
                count += 1;
            }
            logger.DebugFormat("FetchIntegrationLogMessages: found {0} IntegrationRLog records for id {0} ref {1}",
                crmId, logReference);

            if (sb.Length > 0)
                return sb.ToString();
            
            return null;
        }

        // Gap's javascript version fetches case info using 4 different fetchXml queries,
        // so doing the same here
        public Entity FetchCasePart1ForSos(Guid crmCaseID)
        {
            String fetchXmlPattern = @"
<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
<entity name='incident'>
<attribute name='incidentid' />
<attribute name='title' />
<attribute name='gap_sosid' />
<attribute name='gap_transferdate' />
<order attribute='gap_transferdate' descending='false' />
<filter type='and'>
<condition attribute='incidentid' operator='eq' value='{0}' />
</filter>
<link-entity name='gap_sosmaritalstatus' from='gap_sosmaritalstatusid' to='gap_sosmaritalstatusid' visible='false' link-type='outer' alias='sosmaritalstatus'>
<attribute name='gap_sosid' />
</link-entity>
<link-entity name='gap_sheltercontract' from='gap_sheltercontractid' to='gap_contract' visible='false' link-type='outer' alias='sheltercontract'>
<attribute name='gap_sosbranchcode' />
<attribute name='gap_sosid' />
<attribute name='gap_contactstart' />
<attribute name='gap_laaschedulenumber' />
<attribute name='gap_contractname' />
</link-entity>
<link-entity name='gap_caseworker' from='gap_caseworkerid' to='gap_caseworkerid' visible='false' link-type='outer' alias='caseworker'>
<attribute name='gap_sosid' />
<attribute name='gap_caseworkername' />
</link-entity>
<link-entity name='gap_caseworker' from='gap_caseworkerid' to='gap_supervisor' visible='false' link-type='outer' alias='supervisor'>
<attribute name='gap_sosid' />
<attribute name='gap_caseworkername' />
</link-entity>
<link-entity name='gap_chargeband' from='gap_chargebandid' to='gap_chargeband' visible='false' link-type='outer' alias='chargeband'>
<attribute name='gap_sosid' />
<attribute name='gap_chargebandname' />
</link-entity>
<link-entity name='gap_mattertype' from='gap_mattertypeid' to='gap_mattertype' visible='false' link-type='outer' alias='mattertype'>
<attribute name='gap_laacode' />
<attribute name='gap_controlled' />
<attribute name='gap_mattertypename' />
</link-entity>
<link-entity name='gap_laadate' from='gap_laadateid' to='gap_laadate' visible='false' link-type='outer' alias='laadate'>
<attribute name='gap_dateactive' />
</link-entity>
<link-entity name='gap_spancode1' from='gap_spancode1id' to='gap_mattertype1' visible='false' link-type='outer' alias='mattertype1'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_spancode1' from='gap_spancode1id' to='gap_mattertype2' visible='false' link-type='outer' alias='mattertype2'>
<attribute name='gap_code' />
</link-entity>
</entity>
</fetch>";
            // <attribute name='gap_schedulenumber' />  // crm didnt like this


            String fetch = String.Format(fetchXmlPattern,
                "{" + crmCaseID.ToString() + "}");


            String whatThisIs = String.Format("FetchCasePart1ForSos for CaseID {0}", crmCaseID);

            Entity ret = CrmFetchSingle(fetch, whatThisIs);

            return ret;
        }

        private Entity CrmFetchSingle(string fetch, string whatThisIs)
        {
            var serv = _connectionPool.GetCrmConnectionCached();
            var fetchresults = serv.RetrieveMultiple(new FetchExpression(fetch));


            Entity ret = null;
            int count = 0;
            foreach (Entity logLoop in fetchresults.Entities)
            {
                ret = logLoop;
                count += 1;
            }
            if (count > 1)
                throw new ApplicationException(string.Format("{0} found {1} rows",
                   whatThisIs, count));

            if (ret == null)
                throw new ApplicationException(string.Format("{0} found {1} rows",
                   whatThisIs, count));
            return ret;
        }

        // Gap's javascript version fetches case info using 4 different fetchXml queries,
        // so doing the same here
        public Entity FetchCasePart2ForSos(Guid crmCaseID)
        {
            String fetchXmlPattern = @"
<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
<entity name='incident'>
<attribute name='incidentid' />
<filter type='and'>
<condition attribute='incidentid' operator='eq' value='{0}' />
</filter>
<link-entity name='gap_accesspoint' from='gap_accesspointid' to='gap_accesspoint' visible='false' link-type='outer' alias='accesspoint'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_laaoutcome' from='gap_laaoutcomeid' to='gap_laaoutcome' visible='false' link-type='outer' alias='laaoutcome1'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_laaoutcome' from='gap_laaoutcomeid' to='gap_laaoutcomebox2' visible='false' link-type='outer' alias='laaoutcome2'>
<attribute name='gap_code' />
</link-entity>
</entity>
</fetch>";

            String fetch = String.Format(fetchXmlPattern,
                "{" + crmCaseID.ToString() + "}");
            String whatThisIs = String.Format("FetchCasePart2ForSos for CaseID {0}", crmCaseID);

            Entity ret = CrmFetchSingle(fetch, whatThisIs);

            return ret;
        }

        // Gap's javascript version fetches case info using 4 different fetchXml queries,
        // so doing the same here
        public Entity FetchCasePart3ForSos(Guid crmCaseID)
        {
            String fetchXmlPattern = @"
<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
<entity name='gap_mattertype'>
<attribute name='gap_mattertypeid' />
<link-entity name='incident' from='gap_mattertype' to='gap_mattertypeid' alias='aa'>
<filter type='and'>
<condition attribute='incidentid' operator='eq' value='{0}' />
</filter>
</link-entity>
<link-entity name='gap_laacategory' from='gap_laacategoryid' to='gap_laacategory' visible='false' link-type='outer' alias='laacategory'>
<attribute name='gap_sosid' />
</link-entity>
</entity>
</fetch>";

            String fetch = String.Format(fetchXmlPattern,
                "{" + crmCaseID.ToString() + "}");
            String whatThisIs = String.Format("FetchCasePart3ForSos for CaseID {0}", crmCaseID);

            Entity ret = CrmFetchSingle(fetch, whatThisIs);

            return ret;
        }

        // Gap's javascript version fetches case info using 4 different fetchXml queries,
        // so doing the same here
        public Entity FetchCasePart4ForSos(Guid crmCaseID)
        {
            String fetchXmlPattern = @"
<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
<entity name='incident'>
<attribute name='incidentid' />
<attribute name='gap_sosid' />
<attribute name='gap_openingdate' />
<order attribute='gap_openingdate' descending='false' />
<filter type='and'>
<condition attribute='incidentid' operator='eq' value='{0}' />
</filter>
<link-entity name='gap_laaprocurementarea' from='gap_laaprocurementareaid' to='gap_procurementarea' visible='false' link-type='outer' alias='laaprocurementarea'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_laastagereached' from='gap_laastagereachedid' to='gap_laastagereached' visible='false' link-type='outer' alias='laastagereached'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_chargeband' from='gap_chargebandid' to='gap_chargeband' visible='false' link-type='outer' alias='chargeband'>
<attribute name='gap_sosid' />
</link-entity>
<link-entity name='account' from='accountid' to='gap_centerorservice' visible='false' link-type='outer' alias='account'>
<attribute name='gap_sosid' />
</link-entity>
</entity>
</fetch>";

            String fetch = String.Format(fetchXmlPattern,
                "{" + crmCaseID.ToString() + "}");
            String whatThisIs = String.Format("FetchCasePart4ForSos for CaseID {0}", crmCaseID);

            Entity ret = CrmFetchSingle(fetch, whatThisIs);

            return ret;
        }


        public Entity FetchContactForSos(Guid crmCaseID)
        {
            String fetchXmlPattern = @"
<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
<entity name='contact'>
<attribute name='gap_title' />
<attribute name='gap_sosid' />
<attribute name='gap_contactref' />
<attribute name='lastname' />
<attribute name='firstname' />
<attribute name='birthdate' />
<attribute name='contactid' />
<attribute name='address1_city' />
<attribute name='address1_line3' />
<attribute name='address1_line2' />
<attribute name='address1_line1' />
<attribute name='address1_postalcode' />
<attribute name='address1_country' />
<order attribute='gap_title' descending='false' />
<link-entity name='incident' from='gap_clientcontactid' to='contactid' alias='ab'>
<filter type='and'>
<condition attribute='incidentid' operator='eq' value='{0}' />
</filter>
</link-entity>
<link-entity name='gap_laagender' from='gap_laagenderid' to='gap_laagender' visible='false' link-type='outer' alias='gendercode'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_laaethnicity' from='gap_laaethnicityid' to='gap_laaethnicity' visible='false' link-type='outer' alias='ethnicitycode'>
<attribute name='gap_code' />
</link-entity>
<link-entity name='gap_disability' from='gap_disabilityid' to='gap_laadisability' visible='false' link-type='outer' alias='laadisability'>
<attribute name='gap_code' />
</link-entity>
</entity>
</fetch>";

           

            String fetch = String.Format(fetchXmlPattern,
                "{" + crmCaseID.ToString() + "}");

            String whatThisIs = String.Format("FetchContactForSos for CaseID {0}", crmCaseID);

            Entity ret = CrmFetchSingle(fetch, whatThisIs);
            return ret;
        }

        public void UpdateContactSosID(Guid crmContactID, String sosID)
        {
            Entity contact = new Entity("contact");
            contact["contactid"] = crmContactID;
            contact["gap_sosid"] = sosID;

            var serv = _connectionPool.GetCrmConnectionCached();
            serv.Update(contact);
            logger.DebugFormat("UpdateContactSosID updated record {0} with SosID {1}", crmContactID.ToString(), sosID);
        }

        public void UpdateCaseSosIDAndUfn(Guid crmCaseID, String sosID, String ufn)
        {
            Entity incident = new Entity("incident");
            incident["incidentid"] = crmCaseID;
            incident["gap_sosid"] = sosID;
            incident["gap_uniquefilenumber"] = ufn;
            
            var serv = _connectionPool.GetCrmConnectionCached();
            serv.Update(incident);
            logger.DebugFormat("UpdateCaseSosIDAndUfn updated record {0} with SosID {1} and Ufn {2}", crmCaseID.ToString(), sosID, ufn);
        }

    }
}
