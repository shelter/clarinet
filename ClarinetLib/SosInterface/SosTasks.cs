﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClarinetLib.GapSosService;
using Microsoft.Xrm.Sdk;

namespace ClarinetLib.SosInterface
{
    public class SosTasks
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private CrmTasksForSos _crmTasks;
        private SOSServiceClient _sosClient;
        private CrmPicklistTasks _crmPicklistTasks;

        public SosTasks() : this(new CrmTasksForSos(), new SOSServiceClient(), new CrmPicklistTasks())
        {
        }

        public SosTasks(CrmTasksForSos pcrm, SOSServiceClient psos, CrmPicklistTasks ppick)
        {
            _crmTasks = pcrm;
            _sosClient = psos;
            _crmPicklistTasks = ppick;
        }

        public SosTaskResponse TransferCrmCaseToSos(Guid crmCaseID)
        {
            logger.DebugFormat("TransferCrmCaseToSos called with CaseID {0}", crmCaseID.ToString());
            var resp = new SosTaskResponse();

            var contact = _crmTasks.FetchContactForSos(crmCaseID);
            var casePart1 = _crmTasks.FetchCasePart1ForSos(crmCaseID);

            var sosContactWrap = ConvertCrmResultToSosContact(contact);
            MergeCaseFieldsIntoSosContact(sosContactWrap, casePart1);

            // if contact doesn't already have an SOS ID ....
            if (String.IsNullOrEmpty(sosContactWrap.SosContactID))
            {
                logger.DebugFormat("TransferCrmCaseToSos: contact {0} doesn't have SOS ID so creating Contact in SOS",
                    sosContactWrap.Contact.ContactReference);
                // create it in SOS
                var createReponse = CreateSosContact(sosContactWrap.Contact);
                if (!createReponse.Success)
                {
                    resp.Success = false;
                    String errorNotes = ExplainContactErrorMessages(sosContactWrap, createReponse.ErrorMessage);
                    resp.ErrorMessage = "Create Contact Failed \n" + createReponse.ErrorMessage + errorNotes;
                    return resp;
                }
                logger.DebugFormat("TransferCrmCaseToSos: updating crm contact {0} with SOS ID {1}",
                    sosContactWrap.Contact.ContactReference, createReponse.SosID);
                
                _crmTasks.UpdateContactSosID(sosContactWrap.Contact.ContactId, createReponse.SosID);
                // copy the id into our wrap object
                sosContactWrap.SosContactID = createReponse.SosID;

            }
            else
            {
                logger.DebugFormat("TransferCrmCaseToSos: Crm Contact {0} already exists in SOS with ID {1}",
                    sosContactWrap.Contact.ContactReference, sosContactWrap.SosContactID);
                resp.ContactAlreadyExistsInSos = true;
            }
            resp.SosContactID = sosContactWrap.SosContactID;

            // Gap's original javascript version uses 4 different fetxhXml queries to
            // get case info, same queries used here (part 1 is above)
            var casePart2 = _crmTasks.FetchCasePart2ForSos(crmCaseID);
            var casePart3 = _crmTasks.FetchCasePart3ForSos(crmCaseID);
            var casePart4 = _crmTasks.FetchCasePart4ForSos(crmCaseID);

            var sosCaseWrap = ConvertCrmResultsToSosCase(casePart1, casePart2, casePart3, casePart4, sosContactWrap.SosContactID);
            resp.CrmCaseRef = sosCaseWrap.CrmCaseRef;
            
            if (!String.IsNullOrEmpty(sosCaseWrap.SosCaseID))
            {
                logger.DebugFormat("TransferCrmCaseToSos: Crm Case {0} already exists in SOS with ID {1}",
                    sosCaseWrap.CrmCaseRef, sosCaseWrap.SosCaseID);
                resp.CaseAlreadyExistsInSos = true;
                resp.SosCaseID = sosCaseWrap.SosCaseID;
                // I guess this is success?
                resp.Success = true;
                return resp;
            }

            // so now create the case in SOS
            
            var createReponse2 = CreateSosCase(sosCaseWrap.Case);
            if (!createReponse2.Success)
            {
                resp.Success = false;
                String errorNotes = ExplainCaseErrorMessages(sosCaseWrap, sosContactWrap, createReponse2.ErrorMessage);

                resp.ErrorMessage = "Create Case Failed \n" + createReponse2.ErrorMessage + errorNotes;
                return resp;
            }

            _crmTasks.UpdateCaseSosIDAndUfn(crmCaseID, createReponse2.SosID, createReponse2.SosUfn);
            resp.SosCaseID = createReponse2.SosID;
            resp.Success = true;
            
            return resp;
        }

        

        public SosContactWrap ConvertCrmResultToSosContact(Entity crmContact)
        {
            var ret = new SosContactWrap();

            if (EntityFieldExists(crmContact, "gap_title"))
            {
               
                ret.Contact.Title = ConvertTitleOptionset( crmContact["gap_title"]);
            }
            if (EntityFieldExists(crmContact,"lastname"))
                ret.Contact.Surname = crmContact["lastname"].ToString();
            if (EntityFieldExists(crmContact,"gap_sosid"))
                ret.SosContactID = crmContact["gap_sosid"].ToString();
            if (EntityFieldExists(crmContact,"gap_contactref"))
                ret.Contact.ContactReference = crmContact["gap_contactref"].ToString();
            if (EntityFieldExists(crmContact,"firstname"))
                ret.Contact.Forename = crmContact["firstname"].ToString();
            if (EntityFieldExists(crmContact,"address1_line1"))
                ret.Contact.Address1 = crmContact["address1_line1"].ToString();
            if (EntityFieldExists(crmContact,"address1_line2"))
                ret.Contact.Address2 = crmContact["address1_line2"].ToString();
            if (EntityFieldExists(crmContact,"address1_line3"))
                ret.Contact.Address3 = crmContact["address1_line3"].ToString();
            if (EntityFieldExists(crmContact,"contactid"))
                ret.Contact.ContactId = (Guid)crmContact["contactid"];
            if (EntityFieldExists(crmContact,"address1_city"))
                ret.Contact.Address4 = crmContact["address1_city"].ToString();
            if (EntityFieldExists(crmContact,"address1_postalcode"))
                ret.Contact.PostCode = crmContact["address1_postalcode"].ToString();
            if (EntityFieldExists(crmContact,"address1_country"))
                ret.Contact.Country = crmContact["address1_country"].ToString();
            if (EntityFieldExists(crmContact,"birthdate"))
                ret.Contact.DOB = crmContact["birthdate"].ToString();
            if (EntityFieldExists(crmContact,"gendercode.gap_code"))
                ret.Contact.Gender = ConvertAliasedValue( crmContact["gendercode.gap_code"]);
            if (EntityFieldExists(crmContact, "ethnicitycode.gap_code"))
            {
                var al = ConvertAliasedValue(crmContact["ethnicitycode.gap_code"]);
                
                logger.DebugFormat("ethnicitycode.gap_code {0}", al);
                ret.Contact.LAAEthnicity = int.Parse(al);
            }
            if (EntityFieldExists(crmContact,"laadisability.gap_code"))
                ret.Contact.LAADisability = ConvertAliasedValue( crmContact["laadisability.gap_code"]);

            // time based reference to use for integration log
            ret.Contact.ContactLogReference = DateTime.Now.ToString("yyyyMMddHHmmss");

           

            return ret;
        
        }

        private String ConvertAliasedValue(object crmField)
        {
            var alObj = (Microsoft.Xrm.Sdk.AliasedValue)crmField;
            return alObj.Value.ToString();
        }

        private String ConvertAliasedValueToSosDate(object crmField)
        {
            var alObj = (Microsoft.Xrm.Sdk.AliasedValue)crmField;
            
            DateTime theDate = (DateTime)alObj.Value;
            return theDate.ToString("yyyy-MM-dd");
        }

        private String ConvertDateTimeToSosDate(object theDate)
        {
            DateTime d = (DateTime)theDate;
            return d.ToString("yyyy-MM-dd");
        }

        private String ConvertAliasedValueToSosAppliesFromDate(object crmField)
        {
            var alObj = (Microsoft.Xrm.Sdk.AliasedValue)crmField;

            DateTime theDate = (DateTime)alObj.Value;
            String ret = theDate.ToString("dd/MM/yyyy");
            // for testing
            if (ret.Equals("31/03/2013"))
                ret = "01/04/2013";
            return ret;
        }

        private String ConvertTitleOptionset(object crmField)
        {
            int titleID = ((OptionSetValue)crmField).Value;
            var titleOptionSet = _crmPicklistTasks.GetTitleOptionSet();
            var match = titleOptionSet.FirstOrDefault(t => t.Key == titleID);
            if (match.Key != null)
                return match.Value;

            return null;
        }

        public SosCaseWrap ConvertCrmResultsToSosCase(Entity crmCasePart1, Entity crmCasePart2, 
            Entity crmCasePart3, Entity crmCasePart4, String sosContactId)
        {
            var ret = new SosCaseWrap();


            // ***** fields from crmCasePart1
            if (EntityFieldExists(crmCasePart1, "incidentid"))
                ret.Case.CaseId = new Guid( crmCasePart1["incidentid"].ToString());

            if (EntityFieldExists(crmCasePart1,"gap_transferdate"))
                ret.Case.TransferredDate = ConvertDateTimeToSosDate(crmCasePart1["gap_transferdate"]);
            if (EntityFieldExists(crmCasePart1,"gap_sosid"))
                ret.SosCaseID = crmCasePart1["gap_sosid"].ToString();
            if (EntityFieldExists(crmCasePart1,"sheltercontract.gap_laaschedulenumber"))
                ret.Case.ScheduleNumber = ConvertAliasedValue(crmCasePart1["sheltercontract.gap_laaschedulenumber"]);
            if (EntityFieldExists(crmCasePart1,"sheltercontract.gap_sosbranchcode"))
                ret.Case.SOSBranchCode=  ConvertAliasedValue( crmCasePart1["sheltercontract.gap_sosbranchcode"]);
            if (EntityFieldExists(crmCasePart1,"sheltercontract.gap_sosid"))
                ret.Case.Contract = ConvertAliasedValue(crmCasePart1["sheltercontract.gap_sosid"]);
            if (EntityFieldExists(crmCasePart1,"sheltercontract.gap_contactstart"))
                ret.Case.ContractStartDate= ConvertAliasedValueToSosDate(crmCasePart1["sheltercontract.gap_contactstart"]);
            
            if (EntityFieldExists(crmCasePart1,"caseworker.gap_sosid"))
                ret.Case.CaseWorker = ConvertAliasedValue( crmCasePart1["caseworker.gap_sosid"]);
            if (EntityFieldExists(crmCasePart1,"supervisor.gap_sosid"))
                ret.Case.Supervisor= ConvertAliasedValue( crmCasePart1["supervisor.gap_sosid"]);
            // for validation/error reporting
            if (EntityFieldExists(crmCasePart1, "supervisor.gap_caseworkername"))
                ret.SupervisorName = ConvertAliasedValue(crmCasePart1["supervisor.gap_caseworkername"]);

            if (EntityFieldExists(crmCasePart1,"chargeband.gap_sosid"))
                ret.Case.ChargeBand= ConvertAliasedValue(crmCasePart1["chargeband.gap_sosid"]);
            // for validation/error reporting
            if (EntityFieldExists(crmCasePart1, "chargeband.gap_chargebandname"))
                ret.ChargeBandName = ConvertAliasedValue(crmCasePart1["chargeband.gap_chargebandname"]);

            if (EntityFieldExists(crmCasePart1,"mattertype.gap_laacode"))
                ret.Case.CaseType = ConvertAliasedValue( crmCasePart1["mattertype.gap_laacode"]);
            if (EntityFieldExists(crmCasePart1,"mattertype.gap_controlled"))
            {
                var al = (Microsoft.Xrm.Sdk.AliasedValue)crmCasePart1["mattertype.gap_controlled"];

                ret.Case.IsControlled = bool.Parse(al.Value.ToString());
                

            }
               
            // Set matter description as matter type in SOS.
            if (EntityFieldExists(crmCasePart1,"mattertype.gap_mattertypename"))
                ret.Case.CaseTypeName = ConvertAliasedValue( crmCasePart1["mattertype.gap_mattertypename"]);

            // Description is set to "Title" which is CRM Case Ref
            if (EntityFieldExists(crmCasePart1,"title"))
                ret.Case.Description = crmCasePart1["title"].ToString();
            if (EntityFieldExists(crmCasePart1, "laadate.gap_dateactive"))
                ret.Case.LAADateActive = ConvertAliasedValueToSosAppliesFromDate( crmCasePart1["laadate.gap_dateactive"]);
            if (EntityFieldExists(crmCasePart1,"mattertype1.gap_code"))
                ret.Case.MatterType1 = ConvertAliasedValue(crmCasePart1["mattertype1.gap_code"]);
            if (EntityFieldExists(crmCasePart1,"mattertype2.gap_code"))
                ret.Case.MatterType2  = ConvertAliasedValue(crmCasePart1["mattertype2.gap_code"]);

            // ************ fields from crmCasePart2
            if (EntityFieldExists(crmCasePart2, "accesspoint.gap_code"))
                ret.Case.AccessPoint = ConvertAliasedValue(crmCasePart2["accesspoint.gap_code"]);
            if (EntityFieldExists(crmCasePart2, "laaoutcome1.gap_code"))
                ret.Case.LAAOutcome = ConvertAliasedValue(crmCasePart2["laaoutcome1.gap_code"]);
            // this wasnt used in original
            //if (EntityFieldExists(crmCasePart2, "laaoutcome2.gap_code"))
            //    String laaoutcome2 = crmCasePart2["laaoutcome2.gap_code"].ToString();

            // ***** fields from crmCasePart3
            if (EntityFieldExists(crmCasePart3, "laacategory.gap_sosid"))
                ret.Case.LAACategory = ConvertAliasedValue(crmCasePart3["laacategory.gap_sosid"]);

            // ***** fields from crmCasePart4
            if (EntityFieldExists(crmCasePart4, "gap_openingdate"))
                ret.Case.LAAUFNDate = ConvertDateTimeToSosDate(crmCasePart4["gap_openingdate"]);
            if (EntityFieldExists(crmCasePart4, "laaprocurementarea.gap_code"))
                ret.Case.LAAProcurementArea = ConvertAliasedValue(crmCasePart4["laaprocurementarea.gap_code"]);
            if (EntityFieldExists(crmCasePart4, "laastagereached.gap_code"))
                ret.Case.LAAStageReached = ConvertAliasedValue( crmCasePart4["laastagereached.gap_code"]);
            if (EntityFieldExists(crmCasePart4, "chargeband.gap_sosid"))
                ret.Case.LegalAidStatus = ConvertAliasedValue( crmCasePart4["chargeband.gap_sosid"]);

            // put the crm ref somewhere more obvious too
            ret.CrmCaseRef = ret.Case.Description;

            // time based reference to use for integration log
            ret.Case.CaseLogReference = DateTime.Now.ToString("yyyyMMddHHmmss");

            // link to the contact
            ret.Case.ContactSoSId = sosContactId;
            
             

            return ret;
        }

        public void MergeCaseFieldsIntoSosContact(SosContactWrap sosContactWrap, Entity crmCase)
        {
            if (EntityFieldExists(crmCase, "sosmaritalstatus.gap_sosid"))
                sosContactWrap.Contact.Marital = ConvertAliasedValue( crmCase["sosmaritalstatus.gap_sosid"]);

            if (EntityFieldExists(crmCase, "sheltercontract.gap_sosbranchcode"))
                sosContactWrap.Contact.SOSBranchCode = ConvertAliasedValue( crmCase["sheltercontract.gap_sosbranchcode"]);

            if (EntityFieldExists(crmCase, "caseworker.gap_sosid"))
                sosContactWrap.Contact.Partner = ConvertAliasedValue( crmCase["caseworker.gap_sosid"]);

            // for log/help with validation 
            if (EntityFieldExists(crmCase, "caseworker.gap_caseworkername"))
                sosContactWrap.CaseWorkerName =  ConvertAliasedValue( crmCase["caseworker.gap_caseworkername"]);
            if (EntityFieldExists(crmCase, "sheltercontract.gap_contractname"))
                sosContactWrap.ContractName = ConvertAliasedValue( crmCase["sheltercontract.gap_contractname"]);

        }

        private bool EntityFieldExists(Entity e, String fieldName)
        {
            if (!e.Attributes.Contains(fieldName))
                return false;
            if (e[fieldName] == null)
                return false;

            return true;
        }

        private SosCreateResponse CreateSosContact(GapSosService.Contact sosContact)
        {
            var ret = new SosCreateResponse();

            var request = new CreateContactRequest(sosContact);

            var response = _sosClient.CreateContact(request);

            // if response is null, something went wrong
            if (response.CreateContactResult == null)
            {
                ret.Success = false;
                // service will have created an integration log in CRM with error message
                // fetch it using the LogReference that was defined in the contact obj
                Entity intLog = _crmTasks.FetchIntegrationLog(sosContact.ContactId, sosContact.ContactLogReference, "Contact");
                if (intLog["gap_message"] != null)
                    ret.ErrorMessage = intLog["gap_message"].ToString();
                else
                    ret.ErrorMessage = "Create Contact failed, no error message, check logs on SosCrm server"; 
            }
            else
            {
                ret.Success = true;
                ret.SosID = response.CreateContactResult;
            }
            return ret;
        }

        private SosCreateResponse CreateSosCase(GapSosService.Case sosCase)
        {
            var ret = new SosCreateResponse();

            var request = new CreateCaseRequest(sosCase);

            var response = _sosClient.CreateCase(request);

            // if response is null, something went wrong
            if (response.CreateCaseResult == null)
            {
                ret.Success = false;
                // service will have created an integration log in CRM with error message
                // fetch it using the LogReference that was defined in the case obj
                String intLogMessages = _crmTasks.FetchIntegrationLogMessages(sosCase.CaseId, sosCase.CaseLogReference, "Case");

                if (!String.IsNullOrEmpty(intLogMessages))
                    ret.ErrorMessage = intLogMessages;
                else
                    ret.ErrorMessage = "Create Case failed, no error message, check logs on SosCrm server";
            }
            else
            {
                ret.Success = true;
                // unpack json
                // will be of form { "CaseSOSId":"651824.1","UFN":"010417/001"}
                var unpackedJson = ParseCaseResult(response.CreateCaseResult);

                ret.SosID = unpackedJson["CaseSOSId"];
                ret.SosUfn = unpackedJson["UFN"];

            }
            return ret;
        }

        private Dictionary<String,String> ParseCaseResult(string createCaseResult)
        {
            // will be of form { "CaseSOSId":"651824.1","UFN":"010417/001"}
            // dont need a full json processor, just split and extract

            // split by "
            var splitBits = createCaseResult.Split("\"".ToCharArray());

            // should give:
            // 0  1         2 3        4 5   6 7          8
            // { "CaseSOSId":"651824.1","UFN":"010417/001"}

            if (splitBits.Length < 8)
                throw new ApplicationException(string.Format("ParseCaseResult: Could not parse {0}", createCaseResult));

            if (!splitBits[1].Equals("CaseSOSId"))
                throw new ApplicationException(string.Format("ParseCaseResult: Could not find CaseSOSId in  {0}", createCaseResult));

            if (!splitBits[5].Equals("UFN"))
                throw new ApplicationException(string.Format("ParseCaseResult: Could not find UFN in  {0}", createCaseResult));


            logger.DebugFormat("ParseCaseResult: {0} = {1}",
                splitBits[1], splitBits[3]);
            logger.DebugFormat("ParseCaseResult: {0} = {1}",
                splitBits[5], splitBits[7]);

            var ret = new Dictionary<String, String>();
            ret.Add(splitBits[1], splitBits[3]);
            ret.Add(splitBits[5], splitBits[7]);

            return ret;
        }

        public String ExplainContactErrorMessages(SosContactWrap sosContactWrap, String sosMessage)
        {
            StringBuilder sb = new StringBuilder();

            if (sosMessage.Contains("GENDER: Invalid Gender"))
                sb.AppendFormat("Gender values passed was {0} (taken from LAA Gender in CRM)\n", NullCheck(sosContactWrap.Contact.Gender));

            if (sosMessage.Contains("DISABILITY: Invalid Disability Type"))
                sb.AppendFormat("LAADisability value passed was {0}\n", NullCheck(sosContactWrap.Contact.LAADisability));

            if (sosMessage.Contains("PARTNER: Invalid Fee Earner"))
                sb.AppendFormat("Partner value passed was {0} (Crm Caseworker was {1})\n", NullCheck(sosContactWrap.Contact.Partner),
                    NullCheck(sosContactWrap.CaseWorkerName));

            if (sosMessage.Contains("Invalid Branch:"))
                sb.AppendFormat("SosBranchCode value passed was {0} (Crm Contract was {1})\n",
                    NullCheck(sosContactWrap.Contact.SOSBranchCode), NullCheck(sosContactWrap.ContractName));

            if (sosMessage.Contains("Client Code already in use"))
                sb.AppendFormat("Crm ContactReference value passed was {0}\n",
                    NullCheck(sosContactWrap.Contact.ContactReference));


            if (sb.Length > 0)
                sb.Insert(0, "\nNotes: \n");

            return sb.ToString();
        }

        public String ExplainCaseErrorMessages(SosCaseWrap  sosCaseWrap, SosContactWrap sosContactWrap, String sosMessage)
        {
            StringBuilder sb = new StringBuilder();

            if (sosMessage.Contains("Unable to find Client for new Matter"))
                sb.AppendFormat("Crm Contact {0} specified gap_sosid of {1} but could not find that Contact in SOS",
                    NullCheck(sosContactWrap.Contact.ContactReference), NullCheck(sosContactWrap.SosContactID));

            if (sosMessage.Contains("Invalid Matter Type: "))
                sb.AppendFormat("Matter Type (CaseType) value passed was {0} (CaseTypeName was {1} - check 'Case Type - LAA Code' in CRM)\n",
                    NullCheck(sosCaseWrap.Case.CaseType), NullCheck(sosCaseWrap.Case.CaseTypeName));

            if (sosMessage.Contains("SUPERVISOR: Invalid Fee Earner"))
                sb.AppendFormat("Supervisor value passed was {0} (Crm Caseworker name {1})\n",
                    NullCheck(sosCaseWrap.Case.Supervisor), NullCheck(sosCaseWrap.SupervisorName));

            if (sosMessage.Contains("LA-STAT: Legal Aid Status must be supplied")
                || sosMessage.Contains("LA-STAT: Invalid Legal Aid Status"))
                sb.AppendFormat("LegalAidStatus value passed was {0} (Chargebandname {1} - see 'ChargeBand - sosid' in CRM)\n",
                    NullCheck(sosCaseWrap.Case.LegalAidStatus), NullCheck(sosCaseWrap.ChargeBandName));


            if (sosMessage.Contains("APPLIES-FROM: Invalid \"Applies From\" date"))
                sb.AppendFormat("AppliesFrom aka LAADateActive (from LaaDate object linked to Crm Case) value passed was {0}.\nNote that it needs to exactly match one of the Applies From Matrix entries in SOS\n", 
                    NullCheck(sosCaseWrap.Case.LAADateActive));

            if (sosMessage.Contains("Invalid Stage Reached Code"))
                sb.AppendFormat("LAAStageReached value passed was {0} (must also match LAADateActive {1} and LAACategory {2})\n",
                    NullCheck(sosCaseWrap.Case.LAAStageReached), NullCheck(sosCaseWrap.Case.LAADateActive), NullCheck(sosCaseWrap.Case.LAACategory));

            if (sosMessage.Contains("Invalid Outcome Code"))
                sb.AppendFormat("LAAOutcome value passed was {0} (must also match LAADateActive {1} and LAACategory {2})\n",
                    NullCheck(sosCaseWrap.Case.LAAOutcome), NullCheck(sosCaseWrap.Case.LAADateActive), NullCheck(sosCaseWrap.Case.LAACategory));

            if (sosMessage.Contains("Invalid Branch"))
                sb.AppendFormat("SOSBranchCode value passed was {0} (taken from SOSBranchCode field of Crm Contract)\n",
                    NullCheck(sosCaseWrap.Case.SOSBranchCode));

            if (sosMessage.Contains("CATEGORY: Invalid Legal Aid Category"))
                sb.AppendFormat("LAACategory value passed was {0}\n",
                    NullCheck(sosCaseWrap.Case.LAACategory));

            if (sosMessage.Contains("Matter Code already in use"))
                sb.AppendFormat("Case already exists in SOS with Matter Code {0} however CRM has gap_sosid as {1}\n",
                    NullCheck(sosCaseWrap.CrmCaseRef), NullCheck(sosCaseWrap.SosCaseID));

            if (sosMessage.Contains("TRANSFERRED: \"Transfer Date\" must be later than the matter open date!"))
                sb.AppendFormat("TransferredDate value is {0}, LAAUFNDate is {1}, Matter Open Date maybe defaults to today?\n",
                    NullCheck(sosCaseWrap.Case.TransferredDate), NullCheck(sosCaseWrap.Case.LAAUFNDate));

            if (sosMessage.Contains("No matching span codes (CRM Matter types) found in SOS "))
                sb.AppendFormat("Matter Type 1 {0} and Matter Type 2 {1} must also match LAADateActive {2} and LaaCategory {3} in the SOS Span Code list\n",
                    NullCheck(sosCaseWrap.Case.MatterType1), NullCheck(sosCaseWrap.Case.MatterType2), NullCheck(sosCaseWrap.Case.LAADateActive), NullCheck(sosCaseWrap.Case.LAACategory));

            if (sosMessage.Contains("APPLIES-FROM: Invalid Procurement code, invalid Access Point or invalid combination of \"Applies From\" date"))
                sb.AppendFormat("LAAProcurementArea is {0}, Access Point is {1}, LAADateActive (Applies From) is {2}\nNote that SOS needs Access Point specified if Procurement Area is specified, try AP00000 = 'No Access Point'\n",
                    NullCheck(sosCaseWrap.Case.LAAProcurementArea), NullCheck(sosCaseWrap.Case.AccessPoint), NullCheck(sosCaseWrap.Case.LAADateActive));


            if (sb.Length > 0)
                sb.Insert(0, "\nNotes: \n");

            return sb.ToString();
        }

        // for error reporting on sosfields, replaces null with NULL etc
        private String NullCheck(String sosField)
        {
            if (sosField == null)
                return "(NULL)";
            if (sosField.Equals(""))
                return "(EMPTY)";
            return sosField;
        }

        // this is for testing
        // in CRM, clears the SOS Id for the specified contact
        public void ClearContactSosIdInCrm(Guid crmContactId)
        {
            _crmTasks.UpdateContactSosID(crmContactId, null);
        }

        // this is for testing
        // in CRM, clears the SOS Id for the specified case
        public void ClearCaseSosIdInCrm(Guid crmCaseId)
        {
            _crmTasks.UpdateCaseSosIDAndUfn(crmCaseId, null, null);
            
        }

     

      

        public class SosTaskResponse
        {
            public bool Success;
            public bool ContactAlreadyExistsInSos;
            public bool CaseAlreadyExistsInSos;
            public String CrmCaseRef;
            public String SosContactID;
            public String SosCaseID;
            public String ErrorMessage;
        }

        public class SosContactWrap
        {

            public SosContactWrap()
            {
                Contact = new GapSosService.Contact();
            }

            public String SosContactID;
            public GapSosService.Contact Contact;
            // useful for error/validation logging
            public String CaseWorkerName;
            public String ContractName;
        }

        public class SosCaseWrap
        {
            public SosCaseWrap()
            {
                Case = new GapSosService.Case();
            }

            public String SosCaseID;
            public String CrmCaseRef;
            public GapSosService.Case Case;

            // useful for error/validation logging
            public String MatterTypeName;
            public String SupervisorName;
            public String ChargeBandName;
        }

        private class SosCreateResponse
        {
            public bool Success;
            public String SosID;
            // only for case creation
            public String SosUfn;
            public String ErrorMessage;
        }

        

    }
}
