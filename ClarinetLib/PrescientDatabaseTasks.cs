﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClarinetLib.LocalModel;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace ClarinetLib
{
    public class PrescientDatabaseTasks
    {

        public Referrals FetchExampleData()
        {
            return new Referrals();
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(GetConnectionString());
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["Prescient"].ConnectionString;


        }

        public List<int> FetchContactIdsToProcess(int max)
        {
            String sqlPattern = @"
select top {0} party.party_ref
from party
INNER JOIN udf_partgen
ON udf_partgen.party_ref = party.party_ref
WHERE udf_partgen.crm_ref is not null
order by party_ref desc";

            string sql = String.Format(sqlPattern, max);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            DataTable tab = new DataTable();
            adapt.Fill(tab);

            return tab.AsEnumerable().Select(s => (int)s["party_ref"]).ToList<int>();

        }

        public Referrals FetchDataSet(List<int> partyids, List<string> caseRefs, List<string> caseRefsForTime)
        {
            var ds = new Referrals();
            if (partyids != null && partyids.Count > 0)
            {
                FillContactDataTable(ds, partyids);
                FillContactIndividualDataTable(ds, partyids);
                FillContactOrganisationDataTable(ds, partyids);
            }
            if (caseRefs != null && caseRefs.Count > 0)
            { 
                FillCaseDataTable(ds, caseRefs);
            }
            if (caseRefsForTime != null &&  caseRefsForTime.Count > 0)
            {
                FillTimeRecordDataTable(ds, caseRefsForTime);
            }
            return ds;
        }

        private String ListToIn(List<int> ids)
        {
            StringBuilder sb = new StringBuilder();
    
            foreach(int i in ids)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(i.ToString());
            }
         
            return sb.ToString();
        }

        private String ListToIn(List<string> stringRefs)
        {
            StringBuilder sb = new StringBuilder();

            foreach (String s in stringRefs)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append("'");
                sb.Append(s);
                sb.Append("'");
            }

            return sb.ToString();
        }

        public void FillContactDataTable(Referrals ds, List<int> partyids)
        {
            string sqlPattern = @"select
Cast(party.party_ref as varchar(50)) AS SourceContactID,
Cast('' as varchar(50))  AS TargetContactID,
IsNull((CASE WHEN party.client_ref = '' THEN RTRIM(CONVERT(char(20), party.party_ref)) ELSE party.client_ref END),'') AS SourceReference,
udf_partgen.crm_ref AS TargetReference,
(CASE WHEN party.client_ref = '' THEN 'N' ELSE 'Y' END) AS [Primary],
party_details.salutation AS Salutation,
Telephone1Communications.communication_value AS Telephone1,
Telephone2Communications.communication_value AS Telephone2,
MobileCommunications.communication_value AS MobilePhone,
Email1Communications.communication_value AS EmailAddress1,
Email2Communications.communication_value AS EmailAddress2,
Email3Communications.communication_value AS EmailAddress3,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM 
 party
LEFT JOIN udf_partgen
ON udf_partgen.party_ref = party.party_ref
LEFT JOIN party_details
ON party_details.party_ref = party.party_ref
LEFT JOIN communications AS MobileCommunications
ON MobileCommunications.party_ref = party.party_ref
AND MobileCommunications.seq_no = 1
AND MobileCommunications.communication_type_code = 'MOB'
AND MobileCommunications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 1 AND communications.communication_type_code = 'MOB')
LEFT JOIN communications AS Email1Communications
ON Email1Communications.party_ref = party.party_ref
AND Email1Communications.seq_no = 1
AND Email1Communications.communication_type_code = 'WEM'
AND Email1Communications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 1 AND communications.communication_type_code = 'WEM')
LEFT JOIN communications AS Email2Communications
ON Email2Communications.party_ref = party.party_ref
AND Email2Communications.seq_no = 1
AND Email2Communications.communication_type_code = 'PEM'
AND Email2Communications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 1 AND communications.communication_type_code = 'PEM')
LEFT JOIN communications AS Email3Communications
ON Email3Communications.party_ref = party.party_ref
AND Email3Communications.seq_no = 2
AND Email3Communications.communication_type_code = 'PEM'
AND Email3Communications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 2 AND communications.communication_type_code = 'PEM')
LEFT JOIN communications AS Telephone1Communications
ON Telephone1Communications.party_ref = party.party_ref
AND Telephone1Communications.seq_no = 1
AND Telephone1Communications.communication_type_code = 'BUS'
AND Telephone1Communications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 1 AND communications.communication_type_code = 'BUS')
LEFT JOIN communications AS Telephone2Communications
ON Telephone2Communications.party_ref = party.party_ref
AND Telephone2Communications.seq_no = 1
AND Telephone2Communications.communication_type_code = 'HOM'
AND Telephone2Communications.address_ref = (SELECT TOP 1 communications.address_ref FROM communications WHERE communications.party_ref = party.party_ref AND communications.seq_no = 1 AND communications.communication_type_code = 'HOM')
WHERE
party.party_ref In ({0})";

            string sql = String.Format(sqlPattern, ListToIn(partyids));
            var adapt = new SqlDataAdapter(sql, GetConnection());
            
            adapt.Fill(ds, "Contact");


        }

        public void FillContactIndividualDataTable(Referrals ds, List<int> partyids)
        {
            string sqlPattern = @"SELECT
cast(party.party_ref as varchar(50)) AS SourceContactID,
party_dets_extra.party_title AS Title,
party_dets_extra.forename_1 AS Forename,
RTRIM(party_dets_extra.forename_2 + ' ' + party_dets_extra.forename_3 + ' ' + party_dets_extra.forename_4) AS Middlenames,
party_dets_extra.surname AS Surname,
party_details.d_o_b AS Birthdate,
RTRIM(udf_partgen.ninumber) AS NINumber,
(Select top 1 RTRIM(contact_details.gender)
from contact_details where contact_details.party_ref = party.party_ref
and IsNull(gender,'') <> '') as Gender,
RTRIM(udf_partgen.ethnic) as Ethnicity, 
RTRIM(udf_partgen.disability) as Disability, 
party_details.mailing_name as MailingName,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM party
INNER JOIN party_dets_extra
ON party_dets_extra.party_ref = party.party_ref
INNER JOIN party_types
ON party_types.party_type_code = party.party_type_code
LEFT JOIN udf_partgen
ON udf_partgen.party_ref = party.party_ref
LEFT JOIN party_details
ON party_details.party_ref = party.party_ref
WHERE party_types.indiv_flag = 'Y'
AND party.party_ref in ({0})";

            string sql = String.Format(sqlPattern, ListToIn(partyids));
            var adapt = new SqlDataAdapter(sql, GetConnection());
            
            adapt.Fill(ds, "ContactIndividual");

            
        }

        public void FillContactOrganisationDataTable(Referrals ds, List<int> partyids)
        {
            string sqlPattern = @"SELECT
Cast(party.party_ref as varchar(50)) AS SourceContactID,
party.party_name AS Name,
udf_partgen.webpage AS WebsiteURL,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM party
INNER JOIN party_types
ON party_types.party_type_code = party.party_type_code
LEFT JOIN udf_partgen
ON udf_partgen.party_ref = party.party_ref
WHERE party_types.indiv_flag = 'N'
AND party.party_ref in ({0})";

            string sql = String.Format(sqlPattern, ListToIn(partyids));
            var adapt = new SqlDataAdapter(sql, GetConnection());
            
            adapt.Fill(ds, "ContactOrganisation");

           
        }

        public Referrals FillContactAddressLinkDataTable(int partyid, int addressid)
        {
            string sqlPattern = @"SELECT
cast(addr_xref.party_ref as varchar(50)) AS SourceContactID,
cast(addr_xref.address_ref  as varchar(50))AS SourceAddressID,
cast(addr_xref.party_ref as varchar(50) ) + '.' + cast(addr_xref.address_ref as varchar(50)) AS SourceContactAddressLinkID, -- might be wrong
Cast(null as varchar(50)) AS TargetContactAddressLinkID,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM addr_xref
WHERE addr_xref.party_ref = {0}
and addr_xref.address_ref = {1}
";
            string sql = String.Format(sqlPattern, partyid, addressid);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            Referrals ds = new Referrals();
            adapt.Fill(ds, "ContactAddressLink");

            return ds;
        }

        public Referrals FillAddressDataTable(int addressid)
        {
            string sqlPattern = @"SELECT
cast(address.address_ref as varchar(50)) AS SourceAddressID,
Cast(null as varchar(50)) AS TargetAddressID,
address.address_1 AS AddressLine1,
address.address_2 AS AddressLine2,
address.address_3 AS AddressLine3,
address.town AS Town,
address.county AS County,
address.postcode AS Postcode,
countries.country AS Country,
address.telephone AS Telephone1,
address.telephone_2 AS Telephone2,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM address
LEFT JOIN countries
ON countries.country_code = address.country_code
WHERE address.address_ref = {0}";

            string sql = String.Format(sqlPattern, addressid);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            Referrals ds = new Referrals();
            adapt.Fill(ds, "Address");

            return ds;
        }

        // case refs are form clientRef + "." + mattersuffix
        public void FillCaseDataTable(Referrals ds, List<String> caseRefs )
        {
            string sqlPattern = @"SELECT
cast(Rtrim(archive.client_ref) as varchar(40)) + '.' + cast(archive.matter_suffix as varchar(10)) AS SourceCaseID,
Cast('' as varchar(50)) AS TargetCaseID,
party.party_ref AS SourceContactID,
cast(RTrim(archive.client_ref) as varchar(40)) + '.' + cast(archive.matter_suffix as varchar(10)) AS SourceReference,
matter_extra.crm_ref AS TargetReference,
archive.matter_description AS Description,
RTrim(user_details.user_init) AS OwnerCode,
archive.date_opened AS StartDate,
case when archive.date_closed is not null then archive.date_closed 
when lastbill.last_bill_date is not null then lastbill.last_bill_date 
else null end AS EndDate,
/* file_status is either O,C,R
   if O check for existence of bill record */ 
Case when RTrim(archive.file_status) in ('C','R') then RTrim(archive.file_status) 
when lastbill.last_bill_date is not null then 'C' 
else RTrim(archive.file_status) end AS StatusCode,
RTrim(archive.wtype_code) AS CaseTypeCode,
RTrim(departments.dept_code_alpha) AS CentreOrServiceCode,
RTrim(matter_extra.contract) AS Contract,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID,
nud_legal_aid_civil.case_concluded_date,
nud_legal_aid_civil.matter_type_1,
nud_legal_aid_civil.matter_type_2,
nud_legal_aid_civil.stage_reached,
nud_legal_aid_civil.outcome_for_client,
nud_legal_aid_civil.eligible_client_indicator,
nud_legal_aid_civil.cla_ref_number,
nud_legal_aid_civil.determination,
nud_legal_aid_civil.service_adaptation,
nud_legal_aid_civil.signposting,
nud_legal_aid_civil.media_code,
nud_legal_aid_civil.media_code_top,
nud_legal_aid_civil.telephone_advice,
case when isnull(nud_legal_aid_civil.determination,'') <> '' then 18
when nud_legal_aid_civil.eligible_client_indicator in ('T','V') then 156
when nud_legal_aid_civil.eligible_client_indicator in ('S','W','X','Z') then 132
else 0 end as ClaMaxMinutes,
case when RTRIM(matter_extra.old_file_ref) = 'na' then 0
when IsNull(matter_extra.old_file_ref,'') <> ''
then matter_extra.old_file_ref
else
1000000000 +
Cast(SUBSTRING(matter_extra.client_ref,2,6) as int) * 1000 +
matter_extra.matter_suffix
 end as FakeCicmId,
RTrim(supervisor.user_init) AS SupervisorCode,
RTrim(medialook.look_desc) as MediaCodeDescription,
RTrim(serviceadapt.look_desc) as ServiceAdaptationDescription,
RTrim(signpost.look_desc) as SignpostingDescription,
RTrim(hhold.look_desc) as HouseholdTypeDescription,
RTrim(foc.look_desc) as FormOfContactDescription,
RTrim(hmless.look_desc) as HomelessnessStatusDescription,
RTrim(tenure.look_desc) as TenureDescription
FROM archive
LEFT JOIN matter_extra
ON matter_extra.client_ref = archive.client_ref
AND matter_extra.matter_suffix = archive.matter_suffix
LEFT JOIN party
ON party.client_ref = archive.client_ref
LEFT JOIN user_details
ON user_details.user_code = archive.asst_code
LEFT JOIN departments
ON departments.dept_code = archive.dept_code
/* get latest bill date */ 
left outer join (select client_ref, matter_suffix, max(bill_date) as last_bill_date
				from bill group by client_ref, matter_suffix) lastbill 
on lastbill.client_ref = archive.client_ref and lastbill.matter_suffix = archive.matter_suffix
left outer join nud_legal_aid_civil
on nud_legal_aid_civil.client_ref = archive.client_ref
and nud_legal_aid_civil.matter_suffix = archive.matter_suffix
LEFT outer JOIN user_details supervisor
ON supervisor.user_code = archive.ptnr_code
LEFT OUTER JOIN udf_look medialook
ON medialook.look_alp = nud_legal_aid_civil.media_code and medialook.look_name = 'media'
and rtrim(medialook.look_alp) <> ''
LEFT OUTER JOIN udf_look serviceadapt
ON serviceadapt.look_alp = nud_legal_aid_civil.service_adaptation and serviceadapt.look_name = 'service_adapt'
and rtrim(serviceadapt.look_alp) <> ''
LEFT OUTER JOIN udf_look signpost
on signpost.look_alp = nud_legal_aid_civil.signposting and signpost.look_name = 'signposting' 
 /* a few identical sublookups, we just want one */ 
and signpost.sub_alp = 'HU'
and rtrim(signpost.look_alp) <> ''
LEFT OUTER JOIN udf_look hhold
on hhold.look_alp = matter_extra.household_type and hhold.look_name = 'household_type'
and rtrim(hhold.look_alp) <> ''
LEFT OUTER JOIN udf_look foc
on foc.look_alp = matter_extra.form_of_contact and foc.look_name = 'form_of_contact'
and rtrim(foc.look_alp) <> ''
LEFT OUTER JOIN udf_look hmless
on hmless.look_alp = matter_extra.homelessness_status and hmless.look_name = 'homelessness_st'
and rtrim(hmless.look_alp) <> ''
LEFT OUTER JOIN udf_look tenure
on tenure.look_alp = matter_extra.tenure and tenure.look_name = 'tenure'
and rtrim(tenure.look_alp) <> ''
WHERE  archive.volume_number = 0
and cast(Rtrim(archive.client_ref) as varchar(40)) + '.' + cast(archive.matter_suffix as varchar(10)) in ({0})";
            string sql = String.Format(sqlPattern, ListToIn(caseRefs));
            var adapt = new SqlDataAdapter(sql, GetConnection());
           
            adapt.Fill(ds, "Case");

            
        }


        // case refs are form clientRef + "." + mattersuffix
        public void FillTimeRecordDataTable(Referrals ds, List<String> caseRefs)
        {
            string sqlPattern = @"select 
t.tstran_key as SourceTimeRecordID,
cast(Rtrim(t.client_ref) as varchar(40)) + '.' + cast(t.matter_suffix as varchar(10)) as SourceCaseID,
t.ts_date as [Date],
RTrim(fe.fe_init) as [FeeEarnerCode],
RTrim(t.activity_code) as [ActivityCode],
t.time_mins as [Minutes],
tlkp.task_desc as [TaskDesc],
nt.note_text as [NoteText],
t.time_status as TimeStatus, 
t.bill_number, 
t.bill_date, 
t.bill_period,
cast('' as varchar(50)) as TargetTimeRecordID
from time_trn_hist t
left outer join fe_lookup fe on fe.f_e_code = t.f_e_code
left outer join activities a on a.activity_code = t.activity_code
left outer join time_tasks tt on tt.tstran_key = t.tstran_key
left outer join tasks tlkp on tlkp.task_code = tt.task_code
left outer join time_notes nt on nt.tstran_key = t.tstran_key 
where 
cast(Rtrim(t.client_ref) as varchar(40)) + '.' + cast(t.matter_suffix as varchar(10)) in ({0})
order by t.tstran_key";
            string sql = String.Format(sqlPattern, ListToIn(caseRefs));
            var adapt = new SqlDataAdapter(sql, GetConnection());

            adapt.Fill(ds, "TimeRecord");


        }


        public Referrals FillCaseContactLinkDataTable(string clientRef, int matterSuffix, int identKey)
        {

            String sqlPattern = @"SELECT
RTRIM(matter_addressbook.client_ref) + '.' + RTRIM(CONVERT(char(20), matter_addressbook.matter_suffix)) AS SourceCaseID,
matter_addressbook.party_ref AS SourceContactID,
matter_addressbook.ident_key AS SourceCaseContactLinkID,
Cast(null as varchar(50))  AS TargetCaseContactLinkID,
matter_addressbook.role_code AS RoleCode,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM matter_addressbook
where matter_addressbook.client_ref =  '{0}'
and matter_addressbook.matter_suffix = {1}
and matter_addressbook.ident_key = {2}";
            string sql = String.Format(sqlPattern, clientRef, matterSuffix, identKey);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            Referrals ds = new Referrals();
            adapt.Fill(ds, "CaseContactLink");

            return ds;
        }

        public Referrals FillCasePresentingProblemDataTable(string clientRef, int matterSuffix, int seq)
        {
            String sqlPattern = @"SELECT
RTRIM(nud_presenting_problem.client_ref) + '.' + RTRIM(CONVERT(char(20), nud_presenting_problem.matter_suffix)) AS SourceCaseID,
RTRIM(nud_presenting_problem.client_ref) + '.' + RTRIM(CONVERT(char(20), nud_presenting_problem.matter_suffix)) + '.' + Cast(nud_presenting_problem.seq as varchar(20))  AS SourceCasePresentingProblemID,
Cast(null as varchar(50))  AS TargetCasePresentingProblemID,
nud_presenting_problem.presenting_problem AS Description,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM  nud_presenting_problem
WHERE nud_presenting_problem.client_ref =  '{0}'
and nud_presenting_problem.matter_suffix = {1}
and nud_presenting_problem.seq = {2}";

            string sql = String.Format(sqlPattern, clientRef, matterSuffix, seq);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            Referrals ds = new Referrals();
            adapt.Fill(ds, "CasePresentingProblem");

            return ds;

        }

        public Referrals FillContactAlertDataTable(int partyId,  int seq)
        {
            String sqlPattern = @"SELECT
nud_alerts.party_ref AS SourceContactID,
Cast(nud_alerts.party_ref as varchar(50)) + '.' + Cast(nud_alerts.seq as varchar(10))  AS SourceContactAlertID,
Cast(null as varchar(50)) AS TargetContactAlertID,
nud_alerts.alert_type AS Type,
nud_alerts.alert_start_date AS DateAlertAdded,
nud_alerts.alert_end_date AS DateAlertClosed,
nud_alerts.alert_created_by AS RaisedBy,
nud_alerts.alert_notes AS Notes,
cast(null as char(1)) AS DeletionFlag,
cast(0 as int) AS SyncID
FROM nud_alerts
WHERE nud_alerts.party_ref = {0}
and nud_alerts.seq = {1}";
            string sql = String.Format(sqlPattern, partyId, seq);
            var adapt = new SqlDataAdapter(sql, GetConnection());
            Referrals ds = new Referrals();
            adapt.Fill(ds, "ContactAlert");

            return ds;

        }
    }
}
