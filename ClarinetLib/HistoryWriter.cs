﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using System.Data;
using System.Data.SqlClient;

namespace ClarinetLib
{
    public class HistoryWriter
    {

        public virtual void WriteMessage(string message, int countOfSomething)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "insert into ClarinetHistory([Timestamp], [Message], MessageCount) values (@Timestamp,@Message,@MessageCount)";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@Timestamp", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
                comm.Parameters.Add("@Message", System.Data.SqlDbType.VarChar).Value = message;
                comm.Parameters.Add("@MessageCount", System.Data.SqlDbType.Int).Value = countOfSomething;

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetHistory insert with message {0} failed", message));



            }

        }

        public virtual void WriteRowHistory(RowHistory rowhist)
        {
            WriteRowHistory(rowhist.Direction, rowhist.EntityType, rowhist.OpType, rowhist.SourceId, rowhist.SyncId,
                rowhist.TargetId, rowhist.Succeeded, rowhist.Message);
        }

        public virtual void WriteRowHistory(int direction, string entityType, string opType, string sourceId, int syncId, string targetId, bool succeeded, string message)
        {
            string directionName = "";
            if (direction == 1)
                directionName = "ToCrm";
            if (direction == 2)
                directionName = "ToNorwel";

            using (SqlConnection conn = new SqlConnection(GetConnectionString()))
            {
                string sql = "insert into ClarinetHistory([Timestamp], Direction, EntityType, OpType, SourceId, SyncId, TargetId, Succeeded, [Message])"
                + "values (@Timestamp,@Direction, @EntityType, @OpType, @SourceId, @SyncId, @TargetId, @Succeeded, @Message)";
                var comm = new SqlCommand(sql, conn);
                comm.Parameters.Add("@Timestamp", System.Data.SqlDbType.DateTime).Value = DateTime.Now;
                comm.Parameters.Add("@Direction", System.Data.SqlDbType.VarChar).Value =  StringOrDbNull(directionName);
                comm.Parameters.Add("@EntityType", System.Data.SqlDbType.VarChar).Value =  StringOrDbNull(entityType);
                comm.Parameters.Add("@OpType", System.Data.SqlDbType.VarChar).Value = StringOrDbNull(opType);
                comm.Parameters.Add("@SourceId", System.Data.SqlDbType.VarChar).Value =  StringOrDbNull(sourceId);
                comm.Parameters.Add("@SyncId", System.Data.SqlDbType.Int).Value = syncId;
                comm.Parameters.Add("@TargetId", System.Data.SqlDbType.VarChar).Value =  StringOrDbNull(targetId);
                comm.Parameters.Add("@Succeeded", System.Data.SqlDbType.Int).Value = (succeeded) ? 1:0;
                comm.Parameters.Add("@Message", System.Data.SqlDbType.VarChar).Value =  StringOrDbNull(message);
                

                conn.Open();
                int rows = comm.ExecuteNonQuery();

                if (rows == 0)
                    throw new ApplicationException(string.Format("ClarinetHistory insert with sourceid {0} failed", sourceId));

            }

        }

        private object StringOrDbNull(string input)
        {
            if (!string.IsNullOrEmpty(input))
                return input;
            else
                return DBNull.Value;

        }

        public class RowHistory
        {

            public RowHistory() { }

            public RowHistory(int direction, string entityType, string sourceId, int syncId)
            {
                Direction = direction;
                EntityType = entityType;
                SourceId = sourceId;
                SyncId = syncId;
            }

            public void SetSucceededInfo(string opType, string targetId)
            {
                OpType = opType;
                TargetId = targetId;
                Succeeded = true;
            }

            public void SetSucceededInfo(bool wasInsert, bool wasUpdate, string targetId)
            {
                if (wasInsert)
                    OpType = "Insert";
                if (wasUpdate)
                    OpType = "Update";
                TargetId = targetId;
                Succeeded = true;
            }

            public void SetFailedInfo(string errorMessage)
            {
                
                Succeeded = false;
                Message = errorMessage;
            }

            public int Direction {get; set;}
            public string EntityType {get; set;}
            public string OpType {get;set;}
            public string SourceId {get;set;}
            public int SyncId { get;set;}
            public string TargetId {get;set;}
            public bool Succeeded {get;set;}
            public string Message { get; set; }


        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ClarinetDb"].ConnectionString;


        }

    }
}
